FROM node:14.17.1

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

WORKDIR /usr/src/app

COPY package*.json ./
COPY index.js ./

RUN npm install

COPY ./browserScripts ./browserScripts
COPY ./Script ./Script
COPY ./HTML ./HTML
COPY ./nodeScripts ./nodeScripts
COPY ./initialData ./initialData
COPY ./initializationMethods ./initializationMethods
COPY ./testdata ./testdata

RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git mercurial subversion

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda2-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc



RUN mkdir /usr/src/sw_to_analyze
RUN conda update -n base -c defaults conda
RUN conda env create --file ./initializationMethods/envname.yml
RUN mkdir /usr/src/app/simulationout && mkdir /usr/src/app/docs && mkdir /usr/src/app/uidoc

EXPOSE 3000
EXPOSE 5432

ENTRYPOINT ["./initializationMethods/entrypoint.sh"]