//============================================================================================
//
//             index.js : the node.js server main file. 
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===---------------------------------------------------------------------------------------===//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ONLY FOR DOCUMENTATION ! - DEFINE ALL GLOBAL TYPES AND VARIABLES NEEDED FOR JSDOC DOCUMENTATION
// Start of declaration block
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/**
  * Selection of used properties of a HTML node
  * @typedef {object} nodetype
  * @property {string} innerHTML inner/child HTML structure inside a HTML node
  * 
  */ 

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name d3
 * @class
 */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name d3#select
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */
  
/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name d3#selectAll
  * @param {nodetype} nodes - An integer.     
  * @param {string} nodes - A callback to run whose signature is (sum), where
  * @param {string} table - A callback to run whose signature is (sum), where
  *  sum is an integer.
  */

/**
 * @classdesc Class representing a socket connection.
 * @name selection
 * @class
 * @augments d3
 */

  /**  
  * Removes the current selection object from the HTML DOM
  * @method 
  * @name selection#remove
  * @param {object} this - uses the current selection object.     
  *  sum is an integer.
  */

/**  
  * Removes the current selection object from the HTML DOM
  * @method 
  * @name selection#node
  * @param {object} this - uses the current selection object.     
  *  sum is an integer.
  */

/**  
  * Removes the current selection object from the HTML DOM
  * @method 
  * @name selection#nodes
  * @param {object} this - uses the current selection object.     
  *  sum is an integer.
  */

/**
 * @classdesc Socket.io class
 * @name socket
 * @class
 */

/**  
  * Fires a callback on incoming message
  * @method 
  * @name socket#on
  * @param {string} messageID - string identifier input signal.     
  *  
  */

/**  
  * Fires a callback on incoming message
  * @method 
  * @name socket#emit
  * @param {string} messageID - string identifier out signal.     
  *  
  */

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name express
 * @class
 */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name express#use
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name express#set
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name express#static
  * @param {string} path - path.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name express#router
  * @description constructor of a router object
  */

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name router
 * @class
 * @augments express
 */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name router#get
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name router#post
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name response
 * @class
 */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name response#redirect
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name response#sendFile
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name server
 * @class
 */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name server#listen
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/** 
 * @classdesc Not a real class but an interface. Etc...
 * @name pool
 * @class
 */

 
/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#user
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */


/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#host
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#database
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#password
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#port
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */

/**  
  * Add two numbers together, then pass the results to a callback function.  
  * @method 
  * @name pool#connect
  * @param {nodetype} node - An integer.    
  *  sum is an integer.
  */
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// End of declaration block
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

/**
 * <h2>Main module</h2>
 * <p> This is the module description of the index.js main module in node.js. The project goal is to provide models, simulations
 * and controls methods for vehicle fleets. It starts out with a simulation environment of a static engine model. To understand the
 * details about the static engine model see {@tutorial staticEngineModel}</p>
 * <h3>Webservers and clients of this application</h3>
 * <table>
     * <thead><tr><th>Servers</th><th>Clients</th></tr></thead>
     * <tr><td>Postgres</td> <td>node.js, jupyter</td></tr>
     * <tr><td>Node.js</td> <td>Web Browser : calibration views + stimulation & simulation view + static code view</td></tr>
     * <tr><td>Jupyter</td> <td>Jupyter Notebooks</td></tr>     
 * </table>
 * <p> The project currently uses 3 docker containers: </p>
 * <li>node.js container : calibration data and simulation control interface</li>
 * <li>jupyter server and jupyter notebook container : static and dynamic engine models</li>
 * <li>postgres container : database server for all needed by node and jupyter</li>
 * <p>Continue reading this page to understand the details of the node.js container</p>
 * <h3>Web Clients</h3>
 * <p>To understand the concept of this main server module it is necessary to understand the web clients.
 * Find the overview in the web clients tutorial.{@tutorial webclients}</p>
 * <h3>Parent module</h3>
 * <p> This is the top level module. There is no parent module.</p>
 * <h3>Child modules</h3>
 * <p>A socketIO module is attached to the http server. Follow the attached link to find the details for the socket module : </p>
 * <p>See {@link module:c_socketCom_server}</p>
 * <h3>Clients and servers of this module</h3>
  * <table>
     * <thead><tr><th>Client</th><th>Table in Postgres Server</th><th>Description</th></tr></thead>
     * <tr><td>/</td> <td>ThermoBaseDataStationary</td><td>All engine parameters are described by JSONB information in table columns</td></tr>
     * <tr><td>/</td> <td>ThermoCalDataCurves</td><td>Data table for constants : Editors for constants are opened up in main view once a constant is selected</td></tr>
     * <tr><td>/signals</td> <td>not yet defined</td><td>simulation view is currently under development</td></tr>
     * <tr><td>/Tree</td> <td>not yet defined</td><td>static code analysis is under development</td></tr>
     * <tr><td>/editsurface</td> <td>ThermoCalDataMaps</td><td>data table for 3d edtor</td></tr>
     * <tr><td>/editcurve</td> <td>ThermoCalDataCurves</td><td>load 2d graphical curve editor</td></tr>
     * <tr><td>/docs</td> <td>no data tables used</td><td>currently no documentation data is stored in postgres</td></tr>     
     * </table>   
 * <p>The table shows the postgres data which is read and changed by the client calls according to the path shown in the first column</p>
 * <p>Index.js is the main node.js module. It controls the following views : 
 * <li>select and activate the below views (exception : editing calibration constants happens in the same view)</li>
 * <li>3d map editor</li>
 * <li>2d curve editors specialized in engine characteristics</li>
 * <li>simulation</li>
 * <li>static code analysis + placement of bypass and measurement hooks</li>
 * <br>
 * "npm run start" activates the module.
 * It starts an http server on the given host at port 3000 or a port which is defined by 
 * the environment variable PORT.env. A socket connection is attached to the http server.
 * Major goal of the socket is the communication from an to a postgres database 
 * which stores all project related data. </p>
 * <p> The router of a node express application is coordinating requests received from the 
 *  client </p>
 * 
 * 
 * <p>The index.js main module provides the following services : </p> 
 * <li>provide a set of public folders which may be accessed by client http requests</li>
 * <li>routing for http get and post requests</li>
 * <li>continuous client/server communication via socket.IO</li>
 * <p> Members :  </p>
 * <li> app_use : defines the public folders,the view engine, the router path </li>
 * <li> socketCom : controls the socketIO communication between client and server </li>
 * <li> path : controls the socketIO communication between client and server </li>
 * <li> server : controls the socketIO communication between client and server </li>
 * <li> router : controls the socketIO communication between client and server </li>
 * <li> express : controls the socketIO communication between client and server </li>
 * 
 * <p> Methods :  </p>
 * <li> app_set() : defines pug as the view engine and the views folder as the location of views </li>
 * <li> app_use() : defines the public folders and their path names </li>
 * <li> bodyParser() : parses the http request bodies </li>
 * <li> router_get() : http get service function </li>
 * <li> router_post() : http post service function </li>
 * <li> socket_com() : http post service function </li>
 * <li> server_listen() : activates the listener of the server</li>
 * 
 * The main module creates and activates the http server. It coordinates the http get and post 
 * routes. 
 * 
 * @module a_index_server
 */

/**
 * @type {class} 
 * @description Load framework class
 */
const express = require('express');
/**
 * @type {express} 
 * @description Create the application object
 */
const app = express();
/**
 * @type {httpserver}
 * @description create http server object out of the application object
 */
const server = require('http').createServer(app);
/**
 * @type {object} 
 * @description get path object from path module
 */
const path = require('path');
/**
 * @type {object} 
 * @description get socket object with socket communication method from socketCom module
 */
const socketCom = require("./nodeScripts/socketCom.js");
const loadInitialDirs = require("./nodeScripts/loadInitialDirs.js");

/**
 * @type {router} 
 * @description get router module
 */
const router = express.Router();
/**
     * Define public folder and route names : 
     * <table>
     * <thead><tr><th>access from client</th><th>path on host</th><th>description</th></tr></thead>
     * <tr><td>/HTML</td> <td>__dirname + /HTML</td><td>contains all HTML client pages</td></tr>
     * <tr><td>/View</td> <td>__dirname + /View</td><td>contains the pug view engine source code</td></tr>
     * <tr><td>/Script</td> <td>__dirname + /Script</td><td>contains all javascript sources for the HTML pages</td></tr>
     * <tr><td>/testdata</td> <td>__dirname + /testdata</td><td>contains the model stimulation data</td></tr>
     * <tr><td>/images</td> <td>__dirname + /images</td><td>contains pictures in png format</td></tr>
     * <tr><td>/docs</td> <td>__dirname + /docs</td><td>contains the documentation pages in HTML</td></tr>     
     * </table>
     * @function app_use
     * 
     */
app.use('/node_modules',express.static(__dirname + '/node_modules'));

app.use('/HTML',express.static(__dirname + '/HTML'));

//Store all view engine files in view folder.
app.use('/View',express.static(__dirname + '/View'));

//Store all JS and CSS in Scripts folder.
app.use('/browserScripts',express.static(__dirname + '/browserScripts'));

app.use('/Script',express.static(__dirname + '/Script'));

app.use('/testdata',express.static(__dirname + '/testdata'));

app.use('/simulationout',express.static(__dirname + '/simulationout'));

app.use('/images',express.static(__dirname + '/images'));

app.use('/docs',express.static(__dirname + '/docs'));

app.use('/uidocs',express.static(__dirname + '/uidoc/docs'));

/**
     * Set folder any type of view engine : 
     * <p> folder : ./views
     * <p> view engine : pug     
     * @function app_set
     * 
     */
app.set('views',path.join(__dirname+'/View'));
app.set('view engine','pug');

//add the router
app.use('/', router);

var table_data=[],curve_data=[];
/**
  * Launch socket communication 
  * @function socketCom
  * @description see {@link module:c_socketCom_server}
  */
socketCom(server,table_data,curve_data);


/**
  * Parses HTML structure sent by a http post request     
  * @function bodyParser
  */
const bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: true });
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
  * HTTP get service root
   * @function router_get_d
   * @param {string} / root path
   * @description By inserting the url path of this server together with the root path ("/") into the browser
   * address bar and pressing return, the browser sends a get request for the path "/" to the server. The server
   * replies by loading the '/HTML/dispatcher.html' file into the web client. On the reply the browser starts
   * to render the html file and executes the included javascript. Find more details about the client by following
   * the link. <br><br>
   * [client : load and start function selection view]{@link b_create_caldata_buttons_client.b_1_dataviz}
  */

router.get('/',function(req,res){
    console.log('Select a function')  
    res.sendFile(path.join(__dirname + '/HTML/landing.html'))
      //__dirname : It will resolve to your project folder.
    });


  router.get('/selectSW',function(req,res){
      var DirsReader = new loadInitialDirs('/../initialData/initialDirs.json');
      
      DirsReader.then(function(result)
          {
            var iniPaths = result.intialPaths;
            var iniPth=[];
            for(var i=0;i<iniPaths.length;i++)
            {          
              iniPth.push(iniPaths[i].SWDir);
            }
    
      
            
            res.render('index',{ iniPaths: iniPth });
          });
      //__dirname : It will resolve to your project folder.
    });
    


/**
  * HTTP get service signals
  * @function router_get_s
  * @param {string} /signals signals
  */
  router.get('/signals',function(req,res){
    console.log('Stimulation')  
    res.sendFile(path.join(__dirname + '/HTML/signalsNOsci.html'))
      //__dirname : It will resolve to your project folder.
    });

  /**
  * HTTP get service tree
  * @function router_get_t
  * @param {string} /tree d3treeAnimated.html does not use any
  * external javascript codes yet. This view uses json documents
  * and javascript code which is directly embedded into the html
  * for demonstration purposes. 
  * @description get request service on url path /tree. Replies by 
  * sending d3treeAnimated.html, which creates the static 
  * is sent to 
  */
  router.get('/Tree',function(req,res){
      console.log('Next step : loading HTML')
      res.sendFile(path.join(__dirname + '/HTML/d3treeAnimated.html'))
      //__dirname : It will resolve to your project folder.
    });
  
  /**
  * HTTP get service editsurface
  * @function router_get_es
  * @param {string} /editsurface url path
  * @description replies on the get request by sending /HTML/editableSurface.html
  * to the client. The html creates the 3d map visualization. 
  * <br>[3d visualization]{@link module:l_3d_map_visualization_client}
  */    
  router.get('/editsurface',function(req,res){    
      res.sendFile(path.join(__dirname + '/HTML/editableSurface.html'))
      //__dirname : It will resolve to your project folder.
    });
  
  /**
  * HTTP get service editcurve
  * @function router_get_ec
  * @param {string} /editcurve url path
  * @description replies on the get request by sending /HTML/editableSurface.html
  * to the client. The html creates the 3d map visualization. 
  * <br>[2d visualization]{@link module:m_curveGraphGenerator_client}
  */    
  router.get('/editcurve',function(req,res){    
      res.sendFile(path.join(__dirname + '/HTML/editableCurve.html'))
      //__dirname : It will resolve to your project folder.
    });
  
  /**
  * HTTP get service docs
  * @function router_get_dc
  * @param {string} /docs url path
  * @description load /docs/index.html which creates the
  * documentation view 
  */            
    router.get('/docs',function(req,res){    
      res.sendFile(path.join(__dirname + '/docs/index.html'))
      //__dirname : It will resolve to your project folder.
    });

    router.get('/uidocs',function(req,res){    
      res.sendFile(path.join(__dirname + '/uidoc/docs/index.html'))
      //__dirname : It will resolve to your project folder.
    });
  
/**
  * HTTP post service
  * <p> - client HTML information sent by the post request is parsed
  * and the HTML page is redirected to a destination route according
  * to a request information which is contained in the HTML 
  * The post requests of this application are sent by submit buttons 
  * in client HTML forms</p>
  * 
  * Routes : 
  * <table>
  *   <tr><th>path req. client</th><th>ActiveView == 'map'</th><th>ActiveView == 'curve'</th><th>Description</th></tr>
  *   <tr><td>/submit</td> <td>/editsurface</td><td>/editcurve</td><td>Redirect request to corresponding graphical editor</td></tr>
  *   <tr><td>/signals</td> <td>/signals</td><td>/signals</td><td>Redirect to stimulation view</td></tr>
  *   <tr><td>/staticcode</td> <td>/tree</td><td>/tree</td><td>Redirect tree view for static code analysis</td></tr>
  * </table>
  * 
  * @function router_post
  */

 /**
  * HTTP post on path 'submit' is triggered by pressing the submit button in an input form of
  * the function selection view. The http post service is checking the status of an active 
  * view flag before it activates either a 3d map editor or a 2d map editor. <br><br>
  * Find details about the decision logic in <br> [get active view from postgres]{@link module:a_index_server~pgGetFnc}
  * @function router_post_sbm
  * @param {string} /submit
  * @param {object} urlencodedParser
  * @param {function} function() [get active view from postgres]{@link module:a_index_server~pgGetFnc}
  */            
  router.post('/submit',urlencodedParser,function(req,res){
    
    /**
      * HTTP get service docs
      * @function pgGetFnc
      * @param {string} 'ActiveView' 'constant','map','curve'
      * @description The active view postgres interface is used by the 'submit' post request. 
      * Depending on the status of the 'ActiveView' information in postgres the post request
      * is redirected in the following way : <br>
      * <li>'ActiveView' equals 'map' : &nbsp;&nbsp;&nbsp;&nbsp;[redirect 3d map editor]{@link module:a_index_server~response_redirect_m}</li>
      * <li>ActiveView' equals 'curve' :&nbsp;&nbsp;&nbsp;&nbsp;[redirect 2d curve editor]{@link module:a_index_server~response_redirect_c}</li>
      */            
      var pgGetFnc =require("./nodeScripts/getPostgresWOsocket.js")
      pgGetFnc('ActiveView').then(
        function(data)
        {
          console.log(data);
          if(data.rows[0].View.view == 'map')
          {
                /**
                  * HTTP get service docs
                  * @function response_redirect_m
                  * @param {string} '/editsurface' redirects node express router to the
                  * /editsurfe url path. 
                  * @description 'ActiveView' status is equal to 'map'
                  * the 3d editing view is activated by redirecting the post
                  * request. 
                  * <br><br>[see get request for edit surface]{@link module:a_index_server~router_get_es}
                  */
            res.redirect('/editsurface');
          }
          if(data.rows[0].View.view == 'curve')
          {
                /**
                  * HTTP get service docs
                  * @function response_redirect_c
                  * @param {string} '/editcurve' redirects node express router to the
                  * /editsurfe url path. 
                  * @description 'ActiveView' status is equal to 'curve'
                  * the 2d editing view is activated by redirecting the post
                  * request. 
                  * <br><br>[see get request for edit curve]{@link module:a_index_server~router_get_ec}
                  */
            res.redirect('/editcurve');
          }
        }); 
  });

  
  /**
  * HTTP get service signals
  * @function router_post_sig
  * @param {string} /signals
  * @param {object} urlencodedParser
  * @param {function} redirect_s Redirects the 'signals' post request to the 'signals'
  * get request.
  * <br>[see get request for signals]{@link module:a_index_server~router_get_s}
  */   
  router.post('/signals',urlencodedParser,function(req,res){
   
      console.log(req);
      res.redirect('/signals');    
      
  });
  
  /**
  * HTTP get service static code
  * @function router_post_stc
  * @param {string} /staticcode
  * @param {object} urlencodedParser
  * @param {function} function()
  */   
  router.post('/staticcode',urlencodedParser,function(req,res){
    
    console.log(req);
    res.redirect('/Tree');
    
  });

let port = process.env.PORT || 3000

/**
  * The server starts listening to a defined port and host 
  * @function server_listen
  */

server.listen(port,'0.0.0.0');

console.log('Running at Port',port);

module.exports = app;

