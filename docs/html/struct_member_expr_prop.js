var struct_member_expr_prop =
[
    [ "getMemberDecl", "struct_member_expr_prop.html#aa2dd3965d742282b08b7cf8a1b63f07d", null ],
    [ "getParent", "struct_member_expr_prop.html#aa53da5c129dac567118486ab02f439d5", null ],
    [ "isMemberExpr", "struct_member_expr_prop.html#ab43cd2d9250c32d4779c7c800cc72b11", null ],
    [ "MemberPosition", "struct_member_expr_prop.html#ac812b1fa20af33d6811767d865fdd39e", null ],
    [ "name", "struct_member_expr_prop.html#ab522fae571f022f9f45b16e9ebb3cc49", null ],
    [ "parentname", "struct_member_expr_prop.html#a8a240f11e65d3dddc1054852526bc330", null ],
    [ "parenttypedef", "struct_member_expr_prop.html#ad85af5f7d1ac9470b9a6280676ec6f72", null ],
    [ "parentTypeDefname", "struct_member_expr_prop.html#a65c64578070e879178099a181ee15494", null ]
];