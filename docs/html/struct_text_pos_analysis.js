var struct_text_pos_analysis =
[
    [ "ASTNodeUnderneathMacro", "struct_text_pos_analysis.html#abcc762d206dbfd1e6280398a0d6cc76c", null ],
    [ "FuncMacroExpAtASTNodeBegin", "struct_text_pos_analysis.html#a3dc8fe113f144454dc85fd38b5729326", null ],
    [ "FuncMacroExpAtASTNodeEnd", "struct_text_pos_analysis.html#a35f0707e72d92420dae13d002943849f", null ],
    [ "SpellFuncArgExpASTNodeBeg", "struct_text_pos_analysis.html#a83414bf0eb6e78da5d0a0822b008c697", null ],
    [ "SpellFuncArgExpASTNodeEnd", "struct_text_pos_analysis.html#a888cc8658d73a76a8f4eb82fac7cb759", null ],
    [ "SpellSingleTokASTNodeBeg", "struct_text_pos_analysis.html#a13f8a849b34454697b911a20f4dc137a", null ],
    [ "SpellSingleTokASTNodeEnd", "struct_text_pos_analysis.html#af2f91a3e7da2256c88bdedf54469b737", null ],
    [ "TopLevelText", "struct_text_pos_analysis.html#ad89fc011b259989af80a6072aa6bf61f", null ]
];