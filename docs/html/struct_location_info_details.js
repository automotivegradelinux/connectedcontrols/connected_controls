var struct_location_info_details =
[
    [ "CapturedTextPerLocation", "struct_location_info_details.html#a279ab3e8db333f36e921278d1b1f7fdd", null ],
    [ "ColumnOfMatchEnd", "struct_location_info_details.html#a1094200ef3c3bee0db917af7d217dc79", null ],
    [ "ColumnOfMatchStart", "struct_location_info_details.html#a75bfdf90f6790fb25972148856f04841", null ],
    [ "End", "struct_location_info_details.html#aba22606e445f119b01414b50679d47bd", null ],
    [ "EndDecomposed", "struct_location_info_details.html#a811bb8dadea4c80b9f6bd3cb703680a5", null ],
    [ "EndEndDecomposed", "struct_location_info_details.html#ae5b575a122641a433ff2e7e8b85212c2", null ],
    [ "isFromScratchArea", "struct_location_info_details.html#aea6c00e9d09a11fbb968b18c5a8b9a9e", null ],
    [ "LineOfMatchEnd", "struct_location_info_details.html#a6a22faa6816b7aa9540d23bcda7f4146", null ],
    [ "LineOfMatchStart", "struct_location_info_details.html#ae6f76995baf63e3777ef10b95965863e", null ],
    [ "RealPathName", "struct_location_info_details.html#a107f28887d5fb927ecf8dc187f8bb069", null ],
    [ "Start", "struct_location_info_details.html#a917230fcb9153b86ab5ad59e930545d1", null ],
    [ "StartDecomposed", "struct_location_info_details.html#ad386cbea334a1cedbc3d430d4a5e9ec8", null ],
    [ "StartEndDecomposed", "struct_location_info_details.html#a31589de8c339de1eeda8c56f0a74763d", null ]
];