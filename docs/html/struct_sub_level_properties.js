var struct_sub_level_properties =
[
    [ "BinOpProperties_scalar", "struct_sub_level_properties.html#a930c6a2242ea559f7037c9e74951ac67", null ],
    [ "CallExpr_scalar", "struct_sub_level_properties.html#a628bfd7eaebe38364b6c11627647c9fa", null ],
    [ "current_level", "struct_sub_level_properties.html#a64262bfa983cfbaa7c00ae8d549a6a83", null ],
    [ "DeclRefExpr_scalar", "struct_sub_level_properties.html#ac3940c27ebcead90b719e3617332ff82", null ],
    [ "FuncDeclProperties_scalar", "struct_sub_level_properties.html#ac8f09161a1d5e23d9cb2a308bca7274b", null ],
    [ "InterLiteral_scalar", "struct_sub_level_properties.html#a8291fcd199f52cc4b1c5a9fabc020271", null ],
    [ "isLHS", "struct_sub_level_properties.html#acc00ec5a61d8638c15424e71f70e6b25", null ],
    [ "MemberExprPrp_scalar", "struct_sub_level_properties.html#ac574933724aa35a9dde0755864c3eaef", null ],
    [ "ParenExpr_scalar", "struct_sub_level_properties.html#a8303dd1282e57188e9028b01e6c339ab", null ],
    [ "tree", "struct_sub_level_properties.html#a0cdb22aa96f864d2b4fc2706cd3007dc", null ],
    [ "UnaryOperator_scalar", "struct_sub_level_properties.html#ad8853c7583912269de48283bff1bf11b", null ],
    [ "VarDeclProperties_scalar", "struct_sub_level_properties.html#a821f9744c2fe9965fb3cf9ca69794b79", null ]
];