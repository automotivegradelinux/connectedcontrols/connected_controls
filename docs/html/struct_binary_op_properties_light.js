var struct_binary_op_properties_light =
[
    [ "hasGlobalLHS", "struct_binary_op_properties_light.html#ab656c923dd5ef79a507a431dcbdfd7e8", null ],
    [ "isAdditiveOp", "struct_binary_op_properties_light.html#ac428b347b87013e52c90a7b1f521523d", null ],
    [ "isAssignmentOp", "struct_binary_op_properties_light.html#a65761439cc21f9ad0e5469eb99da4163", null ],
    [ "isBitwiseOp", "struct_binary_op_properties_light.html#ac549dc09a6842e386462e40cfd52de98", null ],
    [ "isComparisonOp", "struct_binary_op_properties_light.html#ae822e5b9b76b7290a7e1449834bd24d7", null ],
    [ "isCompoundAssignmentOp", "struct_binary_op_properties_light.html#a36385bff7f15390bd5c82fc4e352c30d", null ],
    [ "isEqualityOp", "struct_binary_op_properties_light.html#a507f4b5803cbb92138c5d95f1ba81ea4", null ],
    [ "isLogicalOp", "struct_binary_op_properties_light.html#a422e47b47e9a13c26fbaca6cd10700bd", null ],
    [ "isMultiplicativeOp", "struct_binary_op_properties_light.html#ab2183b5d8256702b252e57648706d4d5", null ],
    [ "isPtrMemOp", "struct_binary_op_properties_light.html#a50ffad8a494777b34dc6ef0008dbf913", null ],
    [ "isRelationalOp", "struct_binary_op_properties_light.html#a979c782cf48372975093454f15e2cbd0", null ],
    [ "isShiftAssignOp", "struct_binary_op_properties_light.html#a93e703b37f9889509bab1b1a2d5ba813", null ],
    [ "isShiftOp", "struct_binary_op_properties_light.html#a59e7ea1e0bb4b7445d259f06e3d421fa", null ],
    [ "LHSPosition", "struct_binary_op_properties_light.html#a79e9ad2e4acb648da16d19f39c337cb0", null ],
    [ "noOfGlobalRHS", "struct_binary_op_properties_light.html#a20497c8d158e74c2520f0b30d0bb681c", null ],
    [ "noOfLocalRHS", "struct_binary_op_properties_light.html#acba4d519d706e67cbe644f7b8e4cae6b", null ],
    [ "opcode", "struct_binary_op_properties_light.html#ad719d6ac3b0cc0a91c27ea42b451f5e3", null ],
    [ "RHSPosition", "struct_binary_op_properties_light.html#ada711d8752932835cc2ac049b556ca86", null ]
];