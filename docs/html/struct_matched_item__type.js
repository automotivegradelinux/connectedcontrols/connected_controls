var struct_matched_item__type =
[
    [ "ItemToMatch", "struct_matched_item__type.html#a03973154de401029aabc9823be8a34ad", null ],
    [ "ItemToReturn", "struct_matched_item__type.html#ab2aa02783b5a8cd1cdef4d3f14a64b67", null ],
    [ "MatchPattern", "struct_matched_item__type.html#a159d555434cac820c0ff166dce2560a7", null ],
    [ "ReturnCondition", "struct_matched_item__type.html#a194058831fdeda1c6070321c51d7d71e", null ],
    [ "ReturnConditionItem", "struct_matched_item__type.html#a6d07e5a3f8ecb6079308dc57ffc36368", null ]
];