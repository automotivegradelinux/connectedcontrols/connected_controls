var struct_variable__type =
[
    [ "Arraysize", "struct_variable__type.html#afc36cc87be3bd26dfedc3d640036af42", null ],
    [ "BuiltInType", "struct_variable__type.html#a7d37778a750acca8472a0044b7e08c92", null ],
    [ "CompuRatFuncDenominator", "struct_variable__type.html#a1d1fe08d1400e27b3be0477fdc1c2483", null ],
    [ "CompuRatFuncNumerator", "struct_variable__type.html#a37bd621d091ce55b5144aa09cd35cb8e", null ],
    [ "ConstVolatile", "struct_variable__type.html#a8be7260e81db396e7299c0af539f6717", null ],
    [ "isLHS", "struct_variable__type.html#a58a52481fa764d59ceaa1939453a8102", null ],
    [ "isPointer", "struct_variable__type.html#acf61adf3d8168e11f8cbe1c37247cc65", null ],
    [ "name", "struct_variable__type.html#a4db3c4fb283c01c3bcff6233edd26c41", null ],
    [ "PointeeIsConst", "struct_variable__type.html#a60255db01d0877046da3167ced303ecb", null ],
    [ "PointerIsConst", "struct_variable__type.html#a76acba30d36d5229fed7965e37ae6626", null ],
    [ "SWUnit", "struct_variable__type.html#a96f43c027abbb73f0072b02b863d7671", null ],
    [ "Type", "struct_variable__type.html#ae9939a09770ca983cc7d05934c00db57", null ]
];