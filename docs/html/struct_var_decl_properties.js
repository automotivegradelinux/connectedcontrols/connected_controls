var struct_var_decl_properties =
[
    [ "DeclPosition", "struct_var_decl_properties.html#a134231e9d759048b7d10d0dba907febc", null ],
    [ "DefPosition", "struct_var_decl_properties.html#a2639f33dc190d7d116d70e39b921ea15", null ],
    [ "hasGlobalStorage", "struct_var_decl_properties.html#a5552839197deff5bda4a449a40cb07a1", null ],
    [ "hasLocalStorage", "struct_var_decl_properties.html#acd5449c75601118681291dc9f0ed104b", null ],
    [ "isExtern", "struct_var_decl_properties.html#ae1da2e38828113fe0d3c03d38c83d138", null ],
    [ "Language", "struct_var_decl_properties.html#a23e082a537c6e197409ce48ef9af812e", null ],
    [ "MatcherSrcId", "struct_var_decl_properties.html#aa774406305a751790f3e21fcff46bf0f", null ],
    [ "TypeLocPosition", "struct_var_decl_properties.html#a55567e3131b24ca2b507a29c59fb13fe", null ],
    [ "TypeProperties_vec", "struct_var_decl_properties.html#a5d05f3d8f19549c807f24abc3f253845", null ],
    [ "VarName", "struct_var_decl_properties.html#a77e45ec60fb99e05f7911949af1d5c55", null ]
];