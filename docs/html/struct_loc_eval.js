var struct_loc_eval =
[
    [ "Expansion", "struct_loc_eval.html#a6bf9ed164ec6717b45fb0c631811bc9b", null ],
    [ "isEndLocation", "struct_loc_eval.html#a826f7bf4c0a856cb1b0c65e7265b381d", null ],
    [ "iterationLevel", "struct_loc_eval.html#ab3cd0e94dc075f84ce1efe9944efe4fa", null ],
    [ "macroType", "struct_loc_eval.html#aa9b2faaed0c9f81938ad5acbef465f13", null ],
    [ "spell_vec", "struct_loc_eval.html#abbfe089872a317e56ec1f4da2bc82acf", null ],
    [ "Spelling", "struct_loc_eval.html#a44494e1a0d6db41e5a44a52e776ebfec", null ],
    [ "spellInterationLevel", "struct_loc_eval.html#aed98e0e01aff7fa85c440cc593409a8f", null ],
    [ "spellNoParent", "struct_loc_eval.html#acc38785ad87ee00c5a476c55d7b44abc", null ]
];