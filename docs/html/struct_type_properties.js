var struct_type_properties =
[
    [ "BuiltInTypeName", "struct_type_properties.html#a511f452efc4a061b42deef459167e926", null ],
    [ "fieldname", "struct_type_properties.html#a706aadb979459874191d732c6615252a", null ],
    [ "fieldname_vec", "struct_type_properties.html#aca0ec11f4db2c1765fbe9c5462d8a065", null ],
    [ "fieldtype", "struct_type_properties.html#aa89021df483ad04f78d9439b21c663da", null ],
    [ "NameOfPointedType", "struct_type_properties.html#a78f7b7368aa7de3d1778948fe16abd78", null ],
    [ "NameOfType", "struct_type_properties.html#a9d9d85698d36cabfbfce6186da6ab2bf", null ],
    [ "PointeeTypeClassName", "struct_type_properties.html#a540191fb33a5ebc8b929c6520e04f127", null ],
    [ "PointeeTypeClassNameDesug", "struct_type_properties.html#a04c7138fed00feabb9ecac16e9a13ab6", null ],
    [ "ptr_fieldname_vec", "struct_type_properties.html#a334329965e9a6302676af7adc2b292d5", null ],
    [ "SizeModifier", "struct_type_properties.html#a93865f3503a044402baa02acda825958", null ],
    [ "TypeClassName", "struct_type_properties.html#a8077db0c71133414416372a8b1d078b0", null ],
    [ "TypeClassNameDesugar", "struct_type_properties.html#ac1fbdd003235147a32ab6ce7eeb7fcab", null ],
    [ "TypeEnumerator", "struct_type_properties.html#a7722c49b99b8fc7c80993c6a74fac2a3", null ],
    [ "TypeIdentifierName", "struct_type_properties.html#ade01cc4828d85363bb9230454030402d", null ],
    [ "TypeIsMacro", "struct_type_properties.html#a94c15b5c2e6f0969c05fa6c220060890", null ],
    [ "TypePosition", "struct_type_properties.html#a866d9efe64ae6b7ce5c7bc62e4b0e898", null ]
];