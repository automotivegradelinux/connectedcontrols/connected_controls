var searchData=
[
  ['hasargumentexpanion_1255',['hasArgumentExpanion',['../struct_res_loc_pairs_per_node.html#a1e557b7af99eef66f833ffbd32ef8e41',1,'ResLocPairsPerNode']]],
  ['hasbeginend_1256',['hasBeginEnd',['../struct_beg_end_decomposed_loc.html#a915191cc5709e749bef3feb7dda64d54',1,'BegEndDecomposedLoc']]],
  ['hasconcatenatedtokens_1257',['hasConcatenatedTokens',['../struct_res_loc_pairs_per_node.html#a74379f26a5c424b9cb29e1cdc5048f79',1,'ResLocPairsPerNode']]],
  ['hasexpansionpairinmacro_1258',['hasExpansionPairInMacro',['../struct_expansion_information.html#aa96f3eced1353806080fd01a40928f80',1,'ExpansionInformation']]],
  ['hasgloballhs_1259',['hasGlobalLHS',['../struct_node_hierarchy.html#a1206777cfac00bec59be3f6d6174c054',1,'NodeHierarchy::hasGlobalLHS()'],['../struct_binary_op_properties_light.html#ab656c923dd5ef79a507a431dcbdfd7e8',1,'BinaryOpPropertiesLight::hasGlobalLHS()'],['../struct_binary_op_properties.html#a1396f88754a6afd9de9765b10f52df2f',1,'BinaryOpProperties::hasGlobalLHS()']]],
  ['hasglobalstorage_1260',['hasGlobalStorage',['../struct_var_decl_properties.html#a5552839197deff5bda4a449a40cb07a1',1,'VarDeclProperties']]],
  ['haslocalstorage_1261',['hasLocalStorage',['../struct_var_decl_properties.html#acd5449c75601118681291dc9f0ed104b',1,'VarDeclProperties']]],
  ['hasmacroatbegin_1262',['hasMacroAtBegin',['../struct_beg_end_decomposed_loc.html#af6ee275ffb92659d00c4f2a076e70b80',1,'BegEndDecomposedLoc']]],
  ['hasmacroatend_1263',['hasMacroAtEnd',['../struct_beg_end_decomposed_loc.html#a0916f446accca958b92ccd14610c430e',1,'BegEndDecomposedLoc']]],
  ['hasspellpairforastnode_1264',['hasSpellPairForASTnode',['../struct_beg_end_decomposed_loc.html#af79c3560c1e42fe65c30b784d61054d4',1,'BegEndDecomposedLoc']]],
  ['history_1265',['history',['../structbson_docu_strct.html#aa6ea81c5be3c41df94b6ae31bf5d3f4c',1,'bsonDocuStrct']]]
];
