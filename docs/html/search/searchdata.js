var indexSectionsWithContent =
{
  0: "abcdefghijklmnoprstuvw",
  1: "abcdefilmnprstuv",
  2: "cdfgmprstw",
  3: "abcdefgilmnprstuvw",
  4: "abcdefghiklmnoprstuv",
  5: "abcdefijlmnprstuv",
  6: "emns",
  7: "abcdefgilmnprstuw",
  8: "bcdimpv",
  9: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

