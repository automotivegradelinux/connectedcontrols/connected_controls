var postgres_interface_8hpp =
[
    [ "MatchedItem_type", "struct_matched_item__type.html", "struct_matched_item__type" ],
    [ "matchType", "postgres_interface_8hpp.html#aa3c1001c4eb1a1aa685ba65afba32207", null ],
    [ "createPgTables", "postgres_interface_8hpp.html#a952e42de50e86fdd1ac069a8acf991e1", null ],
    [ "deleteCompleteTable", "postgres_interface_8hpp.html#a7ca537dcf4e9574d8db3d2794d5ffb4c", null ],
    [ "findItemInCollection", "postgres_interface_8hpp.html#a92b4b934ee79ac703b6db4b5f260c18d", null ],
    [ "genericQuery", "postgres_interface_8hpp.html#a23641ff228bc4a84c18a7ae15c42ed77", null ],
    [ "getJsonColAsTextAtId", "postgres_interface_8hpp.html#a7d1bfcadece56ec9ead10adf37583c6c", null ],
    [ "getMatchedItemVectorFromMongoDB", "postgres_interface_8hpp.html#a1d86e63a743b67cf37de4be135ecc87d", null ],
    [ "getPropertyAccJSONPath", "postgres_interface_8hpp.html#a0231ce735528de5cc4fbe68da76bfc33", null ],
    [ "getStringVectorFromJsonArray", "postgres_interface_8hpp.html#a05c138142fb11ee473b34d27512a3f7e", null ],
    [ "getStringVectorFromJsonObj", "postgres_interface_8hpp.html#abb99b0e2add80b662e70fc70543569d8", null ],
    [ "insertOneIntoCollection", "postgres_interface_8hpp.html#a93a897241339ada0d6a3676765ae1d21", null ],
    [ "isNumber", "postgres_interface_8hpp.html#a32365e377c80c8e22cf61b4f94ae1b02", null ],
    [ "updateOneArrayInDocument", "postgres_interface_8hpp.html#a22bf88c65162eca678164da7ba7536d6", null ],
    [ "updateOneInDocument", "postgres_interface_8hpp.html#a04ff0f05467fdbf12c44aee405f98832", null ],
    [ "glbUri", "postgres_interface_8hpp.html#ac88c2c0d8611af8462fbb3d73bc4dc5e", null ]
];