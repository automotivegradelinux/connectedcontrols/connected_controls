var _decl_proc_8h =
[
    [ "addTypeInfoToVar", "_decl_proc_8h.html#a9822762d5d3da96429cc8eb537152f0e", null ],
    [ "DeclDisPatcher", "_decl_proc_8h.html#a5f31e47fafc3b56fb55a8fdffa67a360", null ],
    [ "FieldDeclProc", "_decl_proc_8h.html#a76cf24d93902e040ec03c2d5d5080d9d", null ],
    [ "FuncDeclProc", "_decl_proc_8h.html#ab38ac3e115c112a92798d1bde32ed22d", null ],
    [ "GetTypeInformationForDecl", "_decl_proc_8h.html#a93970f3b00cf4f70b0eadb2f100c5034", null ],
    [ "GetTypeInformationForTypeDefNameDecl", "_decl_proc_8h.html#a21232d055ec5d99ccaf9029ee5654848", null ],
    [ "storeVarsToDB", "_decl_proc_8h.html#a04d41e256eeb951cffd2a19125abf654", null ],
    [ "StructDeclProc", "_decl_proc_8h.html#a45cceb4fcef384f85e1c10274c240dc1", null ],
    [ "TypeDefNameDeclProc", "_decl_proc_8h.html#a745102550bf2062df80f1291adc0378d", null ],
    [ "VarDeclProc", "_decl_proc_8h.html#a6f02fe5969c9a38f99703a3a29b776d6", null ]
];