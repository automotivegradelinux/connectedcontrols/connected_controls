var struct_expansion_information =
[
    [ "argExpLevel", "struct_expansion_information.html#a4522a6b133d52e83b213f3844b24dc66", null ],
    [ "EndDecomposedAST", "struct_expansion_information.html#a1db7ffd09ccac270b890b2d59925456a", null ],
    [ "EndDecomposedMACRO", "struct_expansion_information.html#adc8d61f974364146c46ea9e8d0dd6d7b", null ],
    [ "ExpansionInMacroEnd", "struct_expansion_information.html#a10f03b36c9e441a2ffdd6c930ad50d95", null ],
    [ "ExpansionInMacroStart", "struct_expansion_information.html#a4d7b7ff7445491bff1a4239f7500ad16", null ],
    [ "ExpansionLoc", "struct_expansion_information.html#a9568fb0c5344f760cf61cded7029b848", null ],
    [ "ExpEnum", "struct_expansion_information.html#a68b2a5873d87ab62c81c7b7f9edaf5aa", null ],
    [ "hasExpansionPairInMacro", "struct_expansion_information.html#aa96f3eced1353806080fd01a40928f80", null ],
    [ "IdOfFileOrExpInfo", "struct_expansion_information.html#aa80dcacd1a3f054a322dc497c893ec64", null ],
    [ "isASTnodePartner", "struct_expansion_information.html#af6f68c2f0fe25c6098e4d1d0ae9191f3", null ],
    [ "isEndPosition", "struct_expansion_information.html#ad6a82843ba0a6abbc6657e5fb4135de3", null ],
    [ "isfunctionTypePartner", "struct_expansion_information.html#aaefc3aad21aa1ff0f79e4235a674ce19", null ],
    [ "macrolevel", "struct_expansion_information.html#af8767b25b9517cd89d083ce1fcf02dc8", null ],
    [ "SpellLoc", "struct_expansion_information.html#a8b60144133a04ab0228e47629652977d", null ],
    [ "StartDecomposedAST", "struct_expansion_information.html#a6df9179bf982c49717c8b3f783315829", null ],
    [ "StartDecomposedMACRO", "struct_expansion_information.html#a709e69a21bc621142e166b71d4fce29b", null ],
    [ "Status", "struct_expansion_information.html#af60eeb5a7447c7b534e7322f2362f5ef", null ]
];