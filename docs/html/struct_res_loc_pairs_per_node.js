var struct_res_loc_pairs_per_node =
[
    [ "ArgumentExpansionPair", "struct_res_loc_pairs_per_node.html#acdc7360c78856fc01ce1f95c9ced36a9", null ],
    [ "hasArgumentExpanion", "struct_res_loc_pairs_per_node.html#a1e557b7af99eef66f833ffbd32ef8e41", null ],
    [ "hasConcatenatedTokens", "struct_res_loc_pairs_per_node.html#a74379f26a5c424b9cb29e1cdc5048f79", null ],
    [ "OuterExpansionPair", "struct_res_loc_pairs_per_node.html#a1f97df4fd9e4a7fa1915d7d9d46ab2ce", null ],
    [ "SpellingExpansionPair_vec", "struct_res_loc_pairs_per_node.html#a1ac06e5fe2d13fd4257411961a2f9cdf", null ]
];