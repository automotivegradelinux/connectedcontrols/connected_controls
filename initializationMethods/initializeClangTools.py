#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 10:51:44 2021

@author: martinhaussmann
"""

#!/usr/bin/python

#from flask import Flask, render_template, request
#from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import Table, Column,Integer, MetaData
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


import pandas as pd

# List all tables
#------------------------
# SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';
#
# List all columns of table 'intialdir'
#----------------------------------------
# SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = 'initialdir';
#
# List all columnnames of table 'intialdir'
#----------------------------------------
# SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = 'initialdir';
#
# List content of column 'toolsctrl'
#----------------------------------------
# SELECT toolsctrl FROM public."initialdir";

engine = create_engine('postgresql://ThermBaseAlgebraic:thermo@db/ThermoBaseDataStationary', echo=True)
allTblsDf = pd.read_sql_query('''SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';''', engine)
tbn_vec = allTblsDf['tablename']
b_empty_tbl=True
b_empty_col=True
for i in tbn_vec:
    print("There are tables",i)
    if(i == 'initialdir'):
        allColNmsDf = pd.read_sql_query('''SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = 'initialdir';''', engine)
        cln_vec = allColNmsDf['column_name']
        for k in cln_vec:
            print("There columns in the table",k)
            if(k == 'inidir'):
                allColCntDf = pd.read_sql_query('''SELECT toolsctrl FROM public."initialdir";''', engine)
                item_vec = allColCntDf['toolsctrl']                
                for l in item_vec:                                        
                    print("There are items in the column",l['reqdirstruct'])
                    b_empty_col =False                
        b_empty=False
        break; 
meta = MetaData()
thermoAlgebra = Table('initialdir', meta, 
                     Column('id', Integer, primary_key = True), 
                     Column('inidir', JSONB),
                     Column('incldir', JSONB),
                     Column('cfilestoparse', JSONB),
                     Column('toolsctrl', JSONB),
                     );                       
if b_empty_tbl:
    meta.create_all(engine, checkfirst=True)

if b_empty_col:
    ins = thermoAlgebra.insert().values(
            inidir={"pathnames": [{"pathname": "tbd"}]},            
            incldir={"pathnames": [{"pathname": "includes"}]},           
            cfilestoparse={"pathnames": []},             
            toolsctrl = {"isprocessed":"true","selecteddir":"tbd","reqdirstruct":"true","reqheaderstruct":"true","reqmetamodelstruct":"true","reqcfilestruct":"true","reqostaskstruct":"true","reqheadertree":"true","reqlocalbuild":"true","reqfuncdefs":"true","reqASTnodes":"true","reqCreateInterface":"true","stim_state":"no_stim_data"}
            )
    conn = engine.connect()
    conn.execute(ins)
else:
    update_statement = thermoAlgebra.update().values(inidir={"pathnames": [{"pathname": "tbd"}]},incldir={"pathnames": [{"pathname": "includes"}]},cfilestoparse={"pathnames": []},toolsctrl = {"isprocessed":"true","selecteddir":"tbd","reqdirstruct":"true","reqheaderstruct":"true","reqmetamodelstruct":"true","reqcfilestruct":"true","reqostaskstruct":"true","reqheadertree":"true","reqlocalbuild":"true","reqfuncdefs":"true","reqASTnodes":"true","reqCreateInterface":"true","stim_state":"no_stim_data"})
    conn = engine.connect()
    conn.execute(update_statement)




