#!/bin/bash --login
# The --login ensures the bash configuration is loaded,
# enabling Conda.
pwd
export PATH=/opt/conda/envs/inipgdata/bin:$PATH
export CONDA_DEFAULT_ENV=inipgdata
python ./initializationMethods/initializeClangTools.py
npm run start
