export function getAnalysisStatus(input)
{	   
	// Connect to socket.io
    var socket = io.connect('http://127.0.0.1:4000');


    d3.select('form').append('body').attr('id','txt');
    
    var directories = {};
    var headers = {};
    var metadata = {};

    var Time;
    var b_lockdir = false;

  
    socket.on('recDirs', function(data)
    {
        // get message status
        b_lockdir = true;
        var bodySel = d3.select('body').append('svg').attr('width','3000').attr('height','1200');
        var directories = data; 
        var directory = [];
        var ctr = 0;
        //setControlArrows();
        var sel = d3.select('svg').append("g").attr("class","directories").attr("transform","translate(0,0)");             
        processDirectoryTree(directories,ctr,sel,"0.",directory,socket);

        var dragHandler = d3.drag()
        .on("drag", function () 
            {
                var maxhigh = parseFloat(this.parentNode.height.baseVal.value);
                var yPar =  d3.event.y;
                if(yPar >= (maxhigh - maxhigh/10)) 
                { 
                    yPar = parseFloat(maxhigh) - parseFloat(maxhigh/10);
                }
                if(yPar <= 0) 
                { 
                    yPar = 0;
                }
                d3.select(this)            
                    .attr("y", yPar);
    
                var rel = parseFloat(yPar/(maxhigh - maxhigh/10));
                const root = d3.select(".directories");
                parent = d3.select(root.node().parentNode);
          
                var bbox = drawBoundingBoxScroll(parent,root);    
                const rootBBox = {
                    x: parseFloat(bbox.left),
                    y: parseFloat(bbox.top),
                    width: parseFloat(bbox.width),
                    height: parseFloat(bbox.hight)
                }
                    
                var offs = -1*parseFloat(rootBBox.height)*rel;
                root.attr("transform","translate(0,"+ offs + ")");
            });

            function drawBoundingBoxScroll(parentgroup,rootgroup)
            {
                var posObj = {};
                var svg = parentgroup;
                var clientrect = rootgroup.node().getBoundingClientRect(),
                svgrect = svg.node().getBoundingClientRect();

                posObj.x = clientrect.left - svgrect.left;
                posObj.y = clientrect.top - svgrect.top;
                posObj.width = clientrect.width;
                posObj.hight = clientrect.height;
                posObj.bottom = posObj.y + clientrect.height;
                posObj.top = posObj.y;
                posObj.left = posObj.x;
                posObj.right = posObj.x+posObj.width;
  
                return posObj;
            }

            function scrollPavastFileColumn()
            {
                let scrollDistance = 0;    
                const root = d3.select(".directories");
                parent = d3.select(root.node().parentNode);
  
                var bbox = drawBoundingBoxScroll(parent,root);    
                const rootBBox = 
                {
                    x: parseFloat(bbox.left),
                    y: parseFloat(bbox.top),
                    width: parseFloat(bbox.width),
                    height: parseFloat(bbox.hight)
                }
    
                var high = parent.node().height.baseVal.value;
                // Position the scroll indicator
                const scrollBar = parent.append('rect')
                .attr('width', 10)
                .attr('rx', 10/2)
                .attr('ry', 10/2)
                .attr('id', 'scrollPitch')
                .attr('height', parseFloat(high/10))
                .attr('transform', `translate(${rootBBox.x+rootBBox.width+10},${rootBBox.y})`);    

                dragHandler(scrollBar);   
            }
        scrollPavastFileColumn();
    });
    socket.on('recHeaders', function(data){
        // get message status
        var headers = data;         
        
    });
    socket.on('recMeta', function(data){
        // get message status
        var metadata = data;             
    });    

    var headerTreedata = [];
    var headerLock = false;
    socket.on('headerTree', function(data){
        // get message status
        
        headerTreedata.push(data);             
    });
    
    socket.on('headerTreedone', function(data){
        // get message status
        if(typeof data !== 'undefined' && !headerLock)
        {
            headerLock = true;
            processHeaderTree(data,socket);
            socket.emit('reqTaskList',true);
        }
        else
        {
            var test = "no data";
        }
                  
    }); 

    socket.on('TaskListDone', function(data){
        // get message status
        if(typeof data !== 'undefined')
        {
            processTaskList(data,socket);
            //processHeaderTree(data,socket);
        }
        else
        {
            var test = "no data";
        }
                  
    }); 
}