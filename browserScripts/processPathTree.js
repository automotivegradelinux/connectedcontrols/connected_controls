
function drawBoundingBox(rootgroup)
{
    var posObj = {};
    var svg = d3.select("svg");
    var clientrect = rootgroup.node().getBoundingClientRect(),
    svgrect = svg.node().getBoundingClientRect();

    posObj.x = clientrect.left - svgrect.left;
    posObj.y = clientrect.top - svgrect.top;
    posObj.width = clientrect.width;
    posObj.hight = clientrect.height;
    posObj.bottom = posObj.y + clientrect.height;
    posObj.top = posObj.y;
    posObj.left = posObj.x;
    posObj.right = posObj.x+posObj.width;
  
    return posObj;
}

function isSibling(did,parentId)
{
			var lendid = did.length; 
			var did_split = did.split(".");
			var parent_split = parentId.split(".");
			var len_par_spl = parent_split.length;
			var len_did_spl = did_split.length;

			var isChild = true;
			var len = len_did_spl-2;
			if(len_par_spl==len_did_spl)
			{
				for(var i=0;i<len-1;i++)
				{
					var b_isequal = !did_split[i].localeCompare(parent_split[i])
					if(!b_isequal) { isChild = false; break; } 				
				}
			}
			else
			{
				isChild = false;
			}
  return isChild;
}
function isChild(did,parentId)
{
			var lendid = did.length; 
			var did_split = did.split(".");
			var parent_split = parentId.split(".");
			var len_par_spl = parent_split.length;
			var len_did_spl = did_split.length;

			var isChild = true;
			var len = len_did_spl-2;
			if(len_par_spl<len_did_spl)
			{
				for(var i=0;i<len;i++)
				{
					var b_isequal = !did_split[i].localeCompare(parent_split[i])
					if(!b_isequal) { isChild = false; break; } 
				
				}
			}
			else
			{
				isChild = false;
			}

  return isChild;
}
function getYOffset(sel)
{
    var len = sel.nodes().length;
    var nds = sel.nodes();
    var yShift = 0;
    var firstStep = 15;
    var box;
    if(len>0)
    {
        for(var i=0;i<len;i++)
        {
            chldNds = nds[i].childNodes;
            chldNdslen = chldNds.length;
            if(chldNdslen>0)
            {
                for(var j=0;j<chldNdslen;j++)
                {
                    if(!chldNds[j].nodeName.localeCompare("g"))
                    {
                        var box = drawBoundingBox(d3.select(chldNds[j]));                                           
                        yShift += box.hight;     
                    }
                }
            }
        }        
    }
    return yShift+firstStep;
}
function processSelectionBox(socket,thisobject)
{
    var boxcolor = d3.select(thisobject).attr("fill");
    if(!boxcolor.localeCompare("blue"))
    {
        d3.select(thisobject).attr("fill","yellow");   
    }
    else
    {
        d3.select(thisobject).attr("fill","blue");  
        var data = d3.select(thisobject).data();
        var searchPath = data[0].filepath;

        socket.emit('Cfile', 
        {
            path:searchPath
        });    
    }
    //#dfac20 
}
function createSVGGroupHeaderTree(shortpath,cur,sel,yShift,socket)
{
    var Polygroup = sel.append('g').attr('id', cur.id).attr("transform","translate(25,"+yShift+")");
    Polygroup.data([cur]);    
    var text = Polygroup.append("text").text(`${cur.id}` + "........" +   `${shortpath}`)			
    .attr('x', 12)
    .attr('y', 12)
    .attr('font-family','serif')	
    .attr('font-size','14px')
    .attr('font-weight','bold');


    return Polygroup;
}
function createSVGGroup(shortpath,cur,sel,yShift,socket)
{
    var Polygroup = sel.append('g').attr('id', cur.index).attr("transform","translate(25,"+yShift+")");
    Polygroup.data([cur]);    
    var text = Polygroup.append("text").text(`${cur.index}` + "........" +   `${shortpath}`)			
    .attr('x', 12)
    .attr('y', 12)
    .attr('font-family','serif')	
    .attr('font-size','14px')
    .attr('font-weight','bold');
    Polygroup.append("rect").attr('id','selectionBox').attr("x",500).attr("y",0).attr("width",14).attr("height",14)
    .attr('fill','#dfac20')
    .attr('stroke','#306f91')
    .attr('stroke-width','1');

    var boxSel = d3.select('#selectionBox');
    d3.selectAll('#selectionBox').on("mousedown",function(){processSelectionBox(socket,this);});  
    return Polygroup;
}
function createChildBranchHeader(shortpath,cur,sel,socket)
{    
    var box = drawBoundingBox(sel);                                           
    var grp = createSVGGroupHeaderTree(shortpath,cur,sel,box.hight,socket);  
    return grp;

}
function createChildBranch(shortpath,cur,sel,socket)
{    
    var box = drawBoundingBox(sel);                                           
    var grp = createSVGGroup(shortpath,cur,sel,box.hight,socket);  
    return grp;

}
function createSiblingConnectorHeader(shortpath,cur,sel,socket)
{
    var len = sel.nodes().length;
    var nds = sel.nodes();
    var yShift = 0;    
    yShift = getYOffset(sel);
    var grp = createSVGGroupHeaderTree(shortpath,cur,sel,yShift,socket); 
    return grp;
}
function createSiblingConnector(shortpath,cur,sel,socket)
{
    var len = sel.nodes().length;
    var nds = sel.nodes();
    var yShift = 0;    
    yShift = getYOffset(sel);
    var grp = createSVGGroup(shortpath,cur,sel,yShift,socket); 
    return grp;
}
var classString = ".directories";
function changeClass()
{
    if(!classString.localeCompare(".directories"))
    {
        var dirSel = d3.select(".headertree").select("g");
        if(!dirSel.empty())
        {
            classString = ".headertree";
        }                
    }
    else
    {
        classString = ".directories";        
    }
}
function shiftdirectory(arrtp)
{
    var dirSel = d3.select(classString).select("g");
    var dat = dirSel.data();
    var stepsize = 50;
    if(typeof dat[0].x == "undefined")
    {dat[0].x = 0};
    if(typeof dat[0].y == "undefined")
    {dat[0].y = 0};
    if(!arrtp.localeCompare("up"))
    {dat[0].y = dat[0].y + stepsize;}
    if(!arrtp.localeCompare("down"))
    {dat[0].y = dat[0].y - stepsize;}
    if(!arrtp.localeCompare("left"))
    {dat[0].x = dat[0].x + stepsize;}
    if(!arrtp.localeCompare("right"))
    {dat[0].x = dat[0].x - stepsize;}
    dirSel.data(dat);
    dirSel.attr("transform","translate("+dat[0].x+","+dat[0].y+")");
}
function drawUserArrows(sel,rot,right,up,arrtp)
{
    var Polygroup = sel.append('g').attr("class",arrtp).attr("transform","translate("+right+","+up+")").append('g').attr("transform","rotate("+rot+")");    
    var poly = Polygroup.append('polyline');
    //transform="rotate(72)"
    poly.attr("points","50,0 25,25 40,25 40,50 60,50 60,25 75,25 50,0");
    poly.attr("fill","blue");
    poly.attr("stroke","black");
    poly.attr("stroke-width","2");  
    var selected = "."+arrtp;
   
    d3.select(selected).on("mousedown",function(){shiftdirectory(arrtp);});  
    return poly;
}
function setControlArrows()
{
    var arrSel = d3.select('svg').append("g").attr("class","shiftcontrols").attr("transform","translate(1000,-100)");    
    var arr1 = drawUserArrows(arrSel,0,400,100,"up");
    var arr2 = drawUserArrows(arrSel,90,520,120,"right");
    var arr3 = drawUserArrows(arrSel,270,380,220,"left");
    var arr4 = drawUserArrows(arrSel,180,500,240,"down");    
    arrSel.append("g").attr("class","switch").attr("transform","translate(450,170)")
    .append("circle").attr("r",20).attr("stroke","black").attr("stroke-width",3).attr("fill","blue");
    d3.select(".switch").on("mousedown",function(){changeClass();});
}
function processPathTree(filepaths,ctr,sel,prevId,directory)
{
    var len = filepaths.length;
    var initial_path= filepaths[0].filepath;
    var siblCtr=1;

        
    for(var i=ctr;i<len;i++)
    {
        var cur =filepaths[i];
        var filepath = cur.filepath;        
        var workDir = cur.workingDir;        
        var isFile = cur.isFile;           
        if(!isFile.localeCompare("false"))
        {
            directory.push(cur);             
            var id = cur.index;        
            if(typeof prevId != 'undefined')
            {
                var b_isChld    = isChild(id,prevId);                
                var b_isSibling = isSibling(id,prevId);
                
                if(b_isChld)
                {
                    var shortpath = filepath.split(initial_path);
                    var nselChld = d3.select(sel.node().lastChild);    
                    siblCtr = 1;                
                    var cursel = createChildBranch(shortpath[1],cur,nselChld,1);                   
                    i++; 
                    i = processPathTree(filepaths,i,nselChld,id,directory);  
                    sel = nselChld;
                    i--;
                }
                else
                {
                    if(b_isSibling)
                    {                                                
                        var shortpath = filepath.split(initial_path); 
                        siblCtr++;                                        
                        var curSel = createSiblingConnector(shortpath[1],cur,sel,siblCtr);                                            
                    }
                    else
                    {
                        return i;
                    }
                }
            }
            prevId = id;            
        }     
    }
    return len;
}
var rootDir;
function processDirectoryTree(filepaths,ctr,sel,prevId,directory,socket)
{
    var len = filepaths.length;
    var initial_path= filepaths[0].filepath;
    rootDir = initial_path;
    var siblCtr=1;
    
    // filepath: "/home/martinhaussmann/RefactoringPlayground/AirPath/AirModTar/P1084CDV5_P1701.1.0.0"
    // index: "0."
    // isFile: "false"
    // isProcessed: "true"
    // stmtNo: "0"
    // workingDir: "."
        
    for(var i=ctr;i<len;i++)
    {
        var cur =filepaths[i];
        var filepath = cur.filepath;        
        var workDir = cur.workingDir;        
        var isFile = cur.isFile;           
        if(!isFile.localeCompare("false"))
        {
            directory.push(cur);             
            var id = cur.index;        
            if(typeof prevId != 'undefined')
            {
                var b_isChld    = isChild(id,prevId);                
                var b_isSibling = isSibling(id,prevId);
                
                if(b_isChld)
                {
                    var shortpath = filepath.split(initial_path);
                    var nselChld = d3.select(sel.node().lastChild);    
                    siblCtr = 1;                
                    var cursel = createChildBranch(shortpath[1],cur,nselChld,socket);                   
                    i++; 
                    i = processDirectoryTree(filepaths,i,nselChld,id,directory,socket);
                    
                    i--;
                }
                else
                {
                    if(b_isSibling)
                    {                                                
                        var shortpath = filepath.split(initial_path); 
                        siblCtr++;                                        
                        var curSel = createSiblingConnector(shortpath[1],cur,sel,socket);
                        prevId = id;                                                        
                    }
                    else
                    {
                        return i;
                    }
                }
            }
            else
            {
                prevId = id;            
            }
        }     
    }
    return len;
}
function getRelativPath(absPath)
{
    var absPath_vec = absPath.split('/');

    var relUriCnt;
    if(typeof rootDir !== 'undefined')
    {
        var rootDir_vec = rootDir.split('/');
        for(var i=0;i<rootDir_vec.length;i++)
        {
            if(!rootDir_vec[i].localeCompare(absPath_vec[i]))
            {
                relUriCnt = i;                
            }

        }
    }
    var UriStr="";
    for(var i=relUriCnt+1;i< absPath_vec.length;i++)
    {
        UriStr = UriStr + "/"+absPath_vec[i];
    }
    return UriStr;
}
function processHeaderTreeRecursive(data_vec,ctr,sel,prevId,socket)
{
    for(var i=ctr;i<data_vec.length;i++)
    {
      var data_cur = data_vec[i];
      var id = data_cur.id; 

      var HPathRaw = data_cur.Header;
      var shortpath =  getRelativPath(HPathRaw);

      if(typeof prevId != 'undefined')
      {
        var b_isChld    = isChild(id,prevId);                
        var b_isSibling = isSibling(id,prevId);
        if(b_isChld)
        {
            var nselChld = d3.select(sel.node().lastChild);  
            var cursel = createChildBranchHeader(shortpath,data_cur,nselChld,socket); 
            i++; 
            i = processHeaderTreeRecursive(data_vec,i,nselChld,id,socket);            
            i--;
        } 
        else
        {
            if(b_isSibling)
            {                
                var curSel = createSiblingConnectorHeader(shortpath,data_cur,sel,socket);
                prevId = id;
            }
            else
            {
                return i;
            }

        }
      }
      else 
      {
        prevId = id; 
      }       
    }
}

function processHeaderTree(data_vec,socket)
{    
    data_init = data_vec[0];    
    var CPathRaw = data_init.CFile;    
    var shortpath =  getRelativPath(CPathRaw);
    var sel = d3.select('svg').append("g").attr("class","headertree");    
    var box = drawBoundingBox(d3.select(".directories").select("g"));
    var yShift = 0;
    var HeaderTreeSel = createSVGGroupHeaderTree(shortpath,data_init,sel,yShift,socket);
    
    var xShift = box.x + box.width + 30;
    HeaderTreeSel.attr("transform","translate("+xShift+","+yShift+")");
    var prevId = data_init.id;
    var ctr = 0;

    var dragHandler = d3.drag()
        .on("drag", function () 
        {
            var maxhigh = parseFloat(this.parentNode.height.baseVal.value);
            var yPar =  d3.event.y;
            if(yPar >= (maxhigh - maxhigh/10)) 
            { 
                yPar = parseFloat(maxhigh) - parseFloat(maxhigh/10);
            }
            if(yPar <= 0) 
            { 
                yPar = 0;
            }
            d3.select(this)            
                .attr("y", yPar);

            var rel = parseFloat(yPar/(maxhigh - maxhigh/10));
            const root = d3.select(".headertree");
            parent = d3.select(root.node().parentNode);
     
            var bbox = drawBoundingBoxScroll(parent,root);    
            const rootBBox = 
            {
                x: parseFloat(bbox.left),
                y: parseFloat(bbox.top),
                width: parseFloat(bbox.width),
                height: parseFloat(bbox.hight)
            }
               
            var offs = -1*parseFloat(rootBBox.height)*rel;
            root.attr("transform","translate(0,"+ offs + ")");
        });

        function drawBoundingBoxScroll(parentgroup,rootgroup)
        {
            var posObj = {};
            var svg = parentgroup;
            var clientrect = rootgroup.node().getBoundingClientRect(),
            svgrect = svg.node().getBoundingClientRect();

            posObj.x = clientrect.left - svgrect.left;
            posObj.y = clientrect.top - svgrect.top;
            posObj.width = clientrect.width;
            posObj.hight = clientrect.height;
            posObj.bottom = posObj.y + clientrect.height;
            posObj.top = posObj.y;
            posObj.left = posObj.x;
            posObj.right = posObj.x+posObj.width;

            return posObj;
        }

        function scrollHeaderTreeColumn()
        {
            let scrollDistance = 0;            
            const root = d3.select(".headertree");
            parent = d3.select(root.node().parentNode);

            var bbox = drawBoundingBoxScroll(parent,root);    
            const rootBBox = 
            {
                x: parseFloat(bbox.left),
                y: parseFloat(bbox.top),
                width: parseFloat(bbox.width),
                height: parseFloat(bbox.hight)
            }

            var high = parent.node().height.baseVal.value;
            // Position the scroll indicator
            const scrollBar = parent.append('rect')
            .attr('width', 10)
            .attr('rx', 10/2)
            .attr('ry', 10/2)
            .attr('id', 'scrollPitch')
            .attr('height', parseFloat(high/10))
            .attr('transform', `translate(${rootBBox.x+rootBBox.width+10},${rootBBox.y})`);
   
            dragHandler(scrollBar);
        }
        
    processHeaderTreeRecursive(data_vec,ctr,HeaderTreeSel,prevId,socket);
    scrollHeaderTreeColumn();

}