export function processRawIniDirPath(DirPathString)
{
    var s = document.getElementById('Data Selection');
    if(!s)
    {
        var DirPathString_split = DirPathString.split(",");
        var vecLen = DirPathString_split.length;
        var sel = d3.select('body');
        var form = sel.append('form').attr('id','Data Selection').attr('method','POST').attr('action','/GetInit');
        
        var select = form.append('select').attr('name','Base Directories');
        for(var i=0;i<vecLen;i++)
        {
            var option = select.append('option').attr('value',DirPathString_split[i]).text(DirPathString_split[i]);
        }    
        var input = form.append('input').attr('type','submit').attr('value','submit');    
        getAnalysisStatus(input);
    }
}