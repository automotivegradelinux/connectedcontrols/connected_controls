

//=========================================================================================
//
//             createDataViz.js : visualize calibration data
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
import {handleDataset} from "./handleModelData.js";



/**
 * Calculator module - See {@tutorial staticEngineModel}
 * @module b_create_caldata_buttons_client 
 * @description 
 * 
 *  This module is called from inside dispatcher.html there are 3 input forms
 *  which are connected to a http post action
 *  <li>form action="/submit" method="POST"</li>          
 *  <li>form action="/signals" method="POST"</li>          
 *  <li>form action="/staticcode" method="POST"</li>          
 * <br>
 * In addition to the 3 input forms there are 6 'div' elements which contain the following text headlines:
 * <li>engine</li>
 * <li>compressor</li>
 * <li>turbine</li>
 * <li>exhaust</li>
 * <li>emissions</li>
 * <li>roadloads</li>
 * <br>
 * This dataviz module is filling data content into the 6 'div' elements by adding push buttons dynamically via
 * javascript. The javascript is adding data from a postgres table in JSON format to the push button objects.
 * The following table shows the key words of the JSON data files and explains the data content. 
 * <br><br>
 * <table>
 *   <tr><th>Members</th><th>Editor</th><th>description</th></tr>
 *   <tr><td>EngineConstants</td><td>constant</td><td>physical constants</td></tr>
 *   <tr><td>EngineAmbientConditions</td><td>map</td><td>currently only set point for intake temperature</td></tr>
 *   <tr><td>EngineEta</td><td>map</td><td>energy efficiency map of combustion engine</td></tr>
 *   <tr><td>EngineFriction</td><td>map</td><td>engine friction map</td></tr>
 *   <tr><td>EngineVolumEff</td><td>map</td><td>engine volumetric efficiency</td></tr>
 *   <tr><td>EngineCompr</td><td>multiple curves</td><td>characteristic curves turbo charger compressor wheel</td></tr>
 *   <tr><td>EngineFullLoadData</td><td>multiple curves</td><td>engine full load characteristics</td></tr>
 *   <tr><td>EngineTurbine</td><td>multiple curves</td><td>characteristic curves for turbo charger turbine</td></tr>
 *   <tr><td>EngineExhaustParam</td><td>constant</td><td>physical constants</td></tr>
 *   <tr><td>EngineRawParticulates</td><td>map</td><td>characteristic curve for particulate emissions</td></tr>
 *   <tr><td>EngineRawNox</td><td>map</td><td>characteristic curve for NoX emissions</td></tr> 
 *   <tr><td>EngineRoadLoads</td><td>multiple curves</td><td>assumed torque and speed profile of typical drive situation</td></tr> 
 * </table>
 * <p> For each member a HTML button is created and a specific data handler is assigned to it which is getting activated once
 * the button is pressed. Each button gets a data member which contains the data according to the key in the table.</p>
 * <p> [data visualization]{@link b_create_caldata_buttons_client.b_1_dataviz}</p>
 */


/** @function 
 * @description Find the details about the dataviz function in the corresponding namespace
 * @see [data visualization]{@link b_create_caldata_buttons_client.b_1_dataviz}
*/

function dataviz(socket){

var d3 = window.d3;

   // line module:modulename ~ function name
   /**
     * Foo object (@see: {@link module:b_create_caldata_buttons_client~dataviz})
     * @name b_create_caldata_buttons_client
     * @inner
     * @private
     * @memberof module:b_create_caldata_buttons_client
     * @property {Object} b_create_caldata_buttons_client - The foo object
     * @property {Object} b_create_caldata_buttons_client.b_1_dataviz - The bar object     
     * @property {function} b_create_caldata_buttons_client.b_1_dataviz.b_1_2_getClassNames - The bar object     
     * @property {function} b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames - The bar object     
     * @property {function} b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client - The bar object     
     */

    /**
     * Generate inner namespace of the startFileExample
     * @memberof b_create_caldata_buttons_client
     * @type {object}
     * @namespace b_create_caldata_buttons_client.b_1_dataviz
     * @description After the client initiated a get request for the root path 
     * the server replies by sending 'dispatcher.hmtl' to the client. The client
     * starts the embedded javascript by connecting socket.io. The server reacts
     * on the connection by starting the connect callback. The connect callback
     * reads the configuration data for the algebraic engine model from postgres
     * and sends it to the client by emitting the 'thermdata' event. 
     * <br><br>Find more details about how the client processes the thermodynamic data by following the link.
     * <br>[client : callback function loading event thermodynamic data]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client}
     * <br><br>To get the details about how the server is triggered and sends its data to the client click on the
     * link below. 
     * <br>[server : callback function on connection request thermodynamic data]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected}     
     * 
     */
 
function getClassNames(nodes)
{
     /**
     * This namespace describes the body of the function getClassNames.
     * The call argument of the function is the array of push button nodes.
     * All names of all push button nodes are returned as an array of string. 
     * <br> Called by : [getter function for button names]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames}
     * 
     * @memberof b_create_caldata_buttons_client.b_1_dataviz
     * @type {object}
     * @namespace b_create_caldata_buttons_client.b_1_dataviz.b_1_1_getClassNames
     * 
     */

      /**
        * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_1_getClassNames
        * @type {Number[]}
        */       
      var name_ar = []

      /**
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_1_getClassNames
      * @function forLoop
      * @param {number} i iterator
      * @param {number} nodes.length break condition : amount of nodes
      * @param {function} i++ increment iterator
      * @return {numbers[]} name_ar
      * @description this for loop iterates across all selected html button nodes
      * and returns their classnames in an array
      */
      for(var i=0;i<nodes.length;i+=1)
      {
            /**
            * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_1_getClassNames
            * @function name_ar_push
            * @param {string}  className  nodes[i].className gets pushed to name array
            */
            name_ar.push(nodes[i].className);
      }
      return name_ar;      
}
function getAllButtonNames()
{
      /**
      * This namespace describes the body of the getAllButtonNames function
      * which returns all classnames of all engine model related push buttons
      * to the <br>['thermdata' event handler (calling function)]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client}.
      * @memberof b_create_caldata_buttons_client.b_1_dataviz
      * @type {object}
      * @namespace b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @see [getter function for classnames]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_1_getClassNames}
      * 
      */
      
      var names_map = [];  

      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.engine.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return ENGINE button nodes
      * 
      */
      var nms = d3.selectAll('div').selectAll('.engine').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }   

      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.compressor.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return COMPRESSOR button nodes
      * 
      */ 
      nms = d3.selectAll('div').selectAll('.compressor').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }  

      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.turbine.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return TURBINE button nodes
      * 
      */   
      nms = d3.selectAll('div').selectAll('.turbine').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }    

      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.exhaust.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return EXHAUST button nodes
      * 
      */ 
      nms = d3.selectAll('div').selectAll('.exhaust').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }    

      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.emissions.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return EMISSION button nodes
      * 
      */ 
      nms = d3.selectAll('div').selectAll('.emissions').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }    
      /**
      * 
      * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames
      * @function d3
      * @param {function} selectAll('div.roadloads.button').nodes() get all html DOM button nodes
      * which are part of class engine and are inside a div element
      * @description return ROADLOADS button nodes
      * 
      */ 
      nms = d3.selectAll('div').selectAll('.roadloads').selectAll('button').nodes();
      if(nms.length>0)
      {
            names_map.push(...getClassNames(nms));        
      }    
      return names_map;
}


 /**     
   * @memberof b_create_caldata_buttons_client.b_1_dataviz
   * @event thermdata
   * @type {object}   
   * @description ouput of the disconnected status on the standard console  
   * [client : callback function on receive event of thermodynamic data]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client}
   */
socket.on('thermdata', function(postgresData) {

      /**
      * call argument : postgresData - all data needed for the algebraic engine model
      * <h4>Overview callback and surrounding html</h4>
      * This namespace describes the callback function body of client sided
      * thermdata event. The callback creates 4 different types of selection fields
      * implemented as push buttons as described in the list below. 
      * The push buttons for the engine characterizing data are implemented
      * as push buttons with a connected callback which gets activated by
      * clicking on the button. The other 3 types of buttons are activating
      * views by sending post requests to the server. The server reacts by
      * redirecting the request to the correct destination according to 
      * the 'ActiveView' state of this function selection view.
      * <br>
      * The lists shows the 4 execution steps of this view
      * <br>
      * <li>create activation button for graphical editors by an input form <a href="tutorial-webclients.html#post_reqs_graph">: graphical view : HTML elements</a></li>
      * <li>create activation button for simulation view by an input form <a href="tutorial-webclients.html#post_reqs_signals">: simulation view : HTML elements</a></li>
      * <li>create activation button for static code analysis view by an input form <a href="tutorial-webclients.html#post_reqs_staticA">: static code view : HTML elements</a></li>
      * <li>create push buttons for thermodynamic engine data (find the explanation of the subfunctions executed during the button creation in the links)
      *    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1. get all already existing button names]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames}
      *    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2. iterate across all data items and create a push button per item if it does not yet exist]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client.forAllObjectinPostgresDataset} 
      *       <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2.1 add data handler, process data on mouse click]{@link module:d_handleModelData_client}
      * <br>
      * <br>
      * </li>
      * <h4>Local editor</h4>
      * In addition to the graphical editing views there is a simple table editor for quick editing available in this function selection view. This
      * editor is mainly for editing constants but may also be used if no graphical 3d or 2d information is necessary. 
      * <li>create activation button for static code analysis view by an input form <a href="tutorial-webclients.html#post_reqs_table">:empty table HTML elements</a></li>
      * <br>
      * <h4>Concept of this callback function</h4>
      * This function creates push buttons for each data element in the postgres
      * dataset. The dataset is coded in JSON. There are expected data elements in that
      * dataset. Usually all data is sent during send/receive event. It may happen
      * that the send/receive event gets interrupted and the dataset is sent multiple
      * times. In such a case only a subset of buttons is generated during an early
      * transmission. During later transmissions the data is sent again but already
      * existing buttons must no be created again. 
      * <br>
      * program steps : 
      * <li>copy json data to buffer 'obj'</li>
      * <li>retrieve all class names of existing buttons : [getter function for button names]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_3_getAllButtonNames}</li>
      * <li>create push buttons for data elements where there is not already a corresponding button</li>
      * <li>connect mouse click handler to the pushbutton and use the classname of the pushbutton as a call argument</li>
      * <br>
      * Result : each data element gets an own push button. Only the first transmission of a data element gets recognized. 
      * Further transmissions get ignored. A handler specific to the button/data element gets called as the push button
      * gets pushed. 
      * <br>
      * mouse click event : {@link data.d_1_handler}
      * @memberof b_create_caldata_buttons_client.b_1_dataviz
      * @type {object}
      * @namespace b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client
      * 
      */

    /** 
     * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client 
     * @type {object} 
     * @description postgres data in json format for all parameters of the 
     * algrebraic theremodynamic engine model
     */
      var obj = postgresData;
      console.log(postgresData)
  
   /**
    * Connect
    * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client
    * @type {Array.<string>}
    * 
    */  

    var names_vec = getAllButtonNames();

 /**
    * Connect
    * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client
    * @param {object[]} obj array of objects  
    * @function forAllObjectinPostgresDataset
    * @description if a key value of the postgres data is not yet used as a button name, a
    * button is created and named after the key value. A handler which is activated
    * on a mouse click it is attached to the button. The handler receives the postgres data
    * set as an argument. 
    * 
    * <br>
    * variables : 
    * <li>k - key name of data element (local to the loop)</li>
    * <li>names_vec - array of class names of push buttons (created out of the existing push buttons)</li>
    * <br>
    * if no push button class name is equal to the key name of the data object in the current loop revolution
    * a new push button is created by a switch case expression. Each case comes with a specific text for the 
    * button, a specific assignment to the html 'div' element which is identified by its classname and a
    * handler with a specific call argument. 
    * <br><br>
    * Upper level div elements:
    * <li>engine</li>
    * <li>compressor</li>
    * <li>turbine</li>
    * <li>exhaust</li>
    * <li>emissions</li>
    * <li>roadloads</li>
    * <br>
    * key names of data elements:
    * <li>EngineConstants</li>
    * <li>EngineAmbientConditions</li>
    * <li>EngineEta</li>
    * <li>EngineFriction</li>
    * <li>EngineVolumEff</li>
    * <li>EngineCompr</li>
    * <li>EngineFullLoadData</li>
    * <li>EngineTurbine</li>
    * <li>EngineExhaustParam</li>
    * <li>EngineRawParticulates</li>
    * <li>EngineRawNox</li>
    * <li>EngineRoadLoads</li>
    * <br>
    * [mouse click event handler]{@link module:d_handleModelData_client}
    */  

    for(var k in obj){  
      var found = false;
      if(names_vec.length>0)
      {
            found = names_vec.includes(k);      
      }
      if(found==false)
      {
            switch(k)
            {
              case 'EngineConstants' : 
                    d3.select('.configData').select('.engine').append('button').text('Engine Constants').attr('class',k).on('click', function(){handleDataset(postgresData,'EngineConstants',socket);}); 
                    break;
              case 'EngineAmbientConditions' : 
                    d3.select('.configData').select('.engine').append('button').text('Engine Ambient Conditions').attr('class',k).on('click', function(){handleDataset(postgresData,'EngineAmbientConditions',socket);}); 
                    break;
              case 'EngineEta' : 
                    d3.select('.configData').select('.engine').append('button').text('Engine Efficiency').attr('class',k).on('click', function(){handleDataset(postgresData,'EngineEta',socket);}); 
                    break;
              case 'EngineFriction' : 
                    d3.select('.configData').select('.engine').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineFriction',socket);}); 
                    break;
              case 'EngineVolumEff' : 
                    d3.select('.configData').select('.engine').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineVolumEff',socket);});  
                    break;
              case 'EngineCompr' : 
                    d3.select('.configData').select('.compressor').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineCompr',socket);});  
                    break;
              case 'EngineFullLoadData' : 
                    d3.select('.configData').select('.compressor').append('button').text('Engine Parameters').attr('class',k).on('click', function(){handleDataset(postgresData,'EngineFullLoadData',socket,d3);}); 
                    break;      
              case 'EngineTurbine' : 
                    d3.select('.configData').select('.turbine').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineTurbine',socket);}); 
                    break;
              case 'EngineExhaustParam' : 
                    d3.select('.configData').select('.exhaust').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineExhaustParam',socket);}); 
                    break;
              case 'EngineRawParticulates' : 
                    d3.select('.configData').select('.emissions').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineRawParticulates',socket);}); 
                    break;
              case 'EngineRawNox' : 
                    d3.select('.configData').select('.emissions').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineRawNox',socket);}); 
                    break;
              case 'EngineRoadLoads' : 
                    d3.select('.configData').select('.roadloads').append('button').text(k).attr('class',k).on('click', function(){handleDataset(postgresData,'EngineRoadLoads',socket);}); 
                    break;
              //default : d3.select('.container').append('button').text(k).attr('class',k);
        
            }
      }
          
    }
  /**
    * @memberof b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client
    * @function handleDataset
    * @param {object} postgresData dataset received from postgres
    * @param {string} EngineAmbientConditions default call parameter
    * @param {object} socket socketIO connection to request server action 
    * @description as long as there is no push button clicked, the 
    * 'Engine Ambient Condition Data' is selected and prepared for 
    * further processing by calling the push button handler.
    * @see [mouse click event handler]{@link module:d_handleModelData_client}
    * 
    */  
    handleDataset(postgresData,'EngineAmbientConditions',socket);
   
   });
}

export {dataviz}