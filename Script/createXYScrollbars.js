
export function createXYScrollbars(objStrId)
{
    function getTranslateXYParm(translateString)
    {
        var split1st =translateString.split("(");
        var split2nd = split1st[1].split(")");
        var split3rd = split2nd[0].split(",");
        return split3rd;
    }
    function dragHandlerX(event) 
    {      
        var bindingGrp = this.parentNode.getBoundingClientRect();            
        var maxwidth = bindingGrp.width;
        var pitchwidth = this.getBoundingClientRect().width;
        var xPar =  event.x;
        if(xPar >= (maxwidth-pitchwidth)) 
        { 
          xPar = parseFloat(maxwidth-pitchwidth);
        }
        if(xPar <= 0) 
        { 
          xPar = 0;
        }
        d3.select(this)            
            .attr("x", xPar);
        var element2d = document.getElementById(objStrId);
        var element2dwidth = element2d.getBoundingClientRect().width;
        var xshift = xPar * (element2dwidth/(maxwidth-pitchwidth));
        var cur_transf = d3.select(element2d).attr('transform');
        var XY_vec = [0,0];
        if(cur_transf!==null)
        {
          var XY_vec = getTranslateXYParm(cur_transf);
        }        
        d3.select(element2d).attr('transform','translate('+(-xshift)+','+XY_vec[1]+')');
    }

    function dragHandlerY(event) 
    {      
        var bindingGrp = this.parentNode.getBoundingClientRect();            
        var maxheight = bindingGrp.height;
        var pitchheight = this.getBoundingClientRect().height;
        var yPar =  event.y;
        if(yPar >= (maxheight-pitchheight)) 
        { 
          yPar = parseFloat(maxheight-pitchheight);
        }
        if(yPar <= 0) 
        { 
          yPar = 0;
        }
        d3.select(this)            
            .attr("y", yPar);
        var element2d = document.getElementById(objStrId);
        var element2dheight = element2d.getBoundingClientRect().height;
        var yshift = yPar * (element2dheight/(maxheight-pitchheight));
        var cur_transf = d3.select(element2d).attr('transform');
        var XY_vec = [0,0];
        if(cur_transf!==null)
        {
          var XY_vec = getTranslateXYParm(cur_transf);
        }             
        d3.select(element2d).attr('transform','translate('+XY_vec[0]+','+(-yshift)+')');
        
    }
    d3.select("#scrollgroupX").remove();
    d3.select("#scrollgroupY").remove();
    
          //Implement scroll groups
          var element2d = document.getElementById(objStrId);
          var scrgrpselY = d3.select(element2d.parentNode).append('g').attr('id','scrollgroupY');
          var scrgrpselX = d3.select(element2d.parentNode).append('g').attr('id','scrollgroupX');
          var frm = element2d.parentNode.getBoundingClientRect()
          var high = frm.height;      
          var width = frm.width;      
          scrgrpselY.append('rect')
          .attr('width', 10)
          .attr('rx', 10/2)
          .attr('ry', 10/2)
          .attr('fill', 'transparent')
          .attr('id', 'scrollFrameY')
          .attr('height', parseFloat(high));
          
          scrgrpselY.append('rect')
          .attr('width', 10)
          .attr('rx', 10/2)
          .attr('ry', 10/2)
          .attr('id', 'scrollPitchY')
          .attr('height', parseFloat(100))
          .call(d3.drag()        
            .on('drag', dragHandlerY)        
            );
    
          scrgrpselX.append('rect')
          .attr('width', parseFloat(width))
          .attr('rx', 10/2)
          .attr('ry', 10/2)
          .attr('fill', 'transparent')
          .attr('id', 'scrollFrameX')
          .attr('height', 10);
    
          scrgrpselX.append('rect')
          .attr('width', parseFloat(100))
          .attr('rx', 10/2)
          .attr('ry', 10/2)
          .attr('id', 'scrollPitchX')
          .attr('height', 10)
          .call(d3.drag()        
            .on('drag', dragHandlerX)        
            );
}
