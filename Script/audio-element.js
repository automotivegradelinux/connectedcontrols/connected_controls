//=========================================================================================
//
//             audio-element.js : uses the web browser audio context to create an 
//             oscislloscope and visualize the amplitude over time of an audio file. 
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Stimulation Client
 * @module k_select-stimulation_client
 */

/** @function k_1_startStimulation
 * @description exported function : needed to select and visualize audio data
 * <p> - the internals of this function are described in the linked namespace
 * <p>{@link audio.k_1_startStimulation}
*/
  function startFileExample (filePath,d3select,Oscilloscope) {

   /**
     * Foo object (@see: {@link module:audio-element~startFileExample})
     * @name audio
     * @inner
     * @private
     * @memberof module:audio-element
     * @property {Object} audio - The foo object
     * @property {Object} audio.k_1_startStimulation - The bar object     
     */
 
   /**
     * Generate inner namespace of the startFileExample
     * @memberof audio
     * @type {object}
     * @namespace audio.k_1_startStimulation
     * 
     */

    /**
      * HTML element group needed for sound processing
      * <p> - canvas
      * <p> - audio handlers
      * <p> - oscilloscope
      * @memberof audio.k_1_startStimulation
      * @type {object}
      */ 

  var selAudio = d3select('body').append('g').attr('class','audio');
  selAudio.append('div').append('p').text('no signal selected');

     
        /**
         * audio context : browser ressource to handle sound data
         * @memberof audio.k_1_startStimulation
         * @type {object}
         */


  const audioContext = new window.AudioContext()

       /**
         * Test child object with child namespace
         * @memberof audio.k_1_startStimulation
         * @type {object} 
         * @description Transparent frame for the oscilloscope
         * <p> - width : half of the window width
         * <p> - height : half of the window height
         */         
 
  const canvas = document.createElement('canvas')
  canvas.width = window.innerWidth/2
  canvas.height = window.innerHeight/2 - 50  
  
  selAudio.node().appendChild(canvas)

  /**
    * Audio element
    * @memberof audio.k_1_startStimulation
    * @type {object} 
    * @description Handler object for sound files
    * <p> - filepath : path of sound file which serves as the audio source
    * <p> - controls : true => amplify, stop, pause and start handles are created
    * <p> - autoplay : true => starts the audio file instantaneously after object creation
    */         
  const audioElement = document.createElement('audio')
  audioElement.setAttribute('class','myAudio');
  audioElement.controls = true
  audioElement.autoplay = true
  audioElement.src = filePath
  selAudio.node().appendChild(audioElement)

  // create source from html5 audio element
  const source = audioContext.createMediaElementSource(audioElement)

  /**
    * Oscilloscope
    * @memberof audio.k_1_startStimulation
    * @type {object} 
    * @description Oscilloscope object receives the media element source which is created out of 
    * the audio context  
    */   
  const scope = new Oscilloscope(source)

  /**
    * Connect
    * @memberof audio.k_1_startStimulation
    * @function connect
    * @param {Connection} audioContext.destination connect audio source to speaker
    * @description Connect the audio file to the speakers 
    * the audio context  
    */   
  // reconnect audio output to speakers
  source.connect(audioContext.destination)

/**
    * Connect
    * @memberof audio.k_1_startStimulation
    * @function getContext
    * @param {string} 2d customize drawing of the oscilloscope
    * @description Connect the audio file to the speakers 
    * the audio context  
    */   

  const ctx = canvas.getContext('2d')
  ctx.lineWidth = 2
  ctx.strokeStyle = '#ffffff'
  ctx.fillStyle = "blue";
  
  

  // start default animation loop
  scope.animate(ctx)
}

export {startFileExample}