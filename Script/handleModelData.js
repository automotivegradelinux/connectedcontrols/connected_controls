/**
 * Calculator module - See {@tutorial staticEngineModel}
 * @module d_handleModelData_client
 * @description  
 * The module exports only one function which is
 * <h4>Exported function : handleDataset</h4>
 * <h4>call arguments</h4>
 * postgresData
 * <br> cmpString
 * <br> socket
 * <p> The handler function is attached to the click button event in the dataviz module</p>
 * [Find the details about the callback function (click to load description)]{@link data.d_1_handler}
 * <h3>Child modules</h3>
 */

var d3 = window.d3;

/**
 * Configuration structure
 * @typedef {Object} config
 * @property {string} name - key value / identifier
 * @property {string} data - contained data
 * 
 */

/**
 * Array of protected data objects
 * @type {Array<config>} 
  * @description
 * <table>
 * <caption>configData'div'</caption>
 * <thead>
 * <tr>
 *  <th>name</th>
 *  <th>data</th>
 *  <th>description</th>
 * </tr>
 * </thead>
 * <tbody>
 * <tr>
 *  <td>EngineConstants</td>
 *  <td>'Tamb[K]','Pamb[bar]','R [J/kg/K]','Lambda_ref','E_LHV [J/kg]','C_p[J/kg/K]','Gamma(C_p/C_v)','Fuel H/C Ratio'</td>
 *  <td>Ambient temp+pressure, spec.gas const, stoich.mass ratio air/fuel ,energy dens.fuel, therm.cap, adiab.const, fuel molec. H to C ratio</td>
 * </tr>
 * <tr>
 *   <td>EngineFullLoadData</td>
 *   <td>N[rpm]	BMEP_full[bar]	dTman[K]	Fman	DFuelSp_mult	Pman/Pamb</td>
 *   <td>Engine speed, brake mean press. full load, deltaTemp man, des.burned gas frac.man, fuel dens, press.ratio manifold/ambient pres</td>
 * </tr>
 * <tr>
 *   <td>EngineAmbientConditions</td>
 *   <td>'DfuelSp[kg/m^3]/S_p[m/s]</td>
 *   <td>Desired manifold air temperature (3d map)</td>
 * </tr>
 * <tr>
 *   <td>EngineEta</td>
 *   <td>Fman, Fex</td>
 *   <td>Engine efficiency as a function of burned gas fraction ration intake to exhaust (3d map)</td>
 * </tr>
 * <tr>
 *   <td>EngineFriction</td>
 *   <td>Dman [kg/m^3] / S_p [m/s][kPa]</td>
 *   <td>Engine friction in a kPa pressure equivalent as a function of intake air density and piston speed</td>
 * </tr>
 * <tr>
 *   <td>EngineVolumEff</td>
 *   <td>Dman [kg/m^3] / S_p [m/s]</td>
 *   <td>Volumetric efficiency factor (3d map) as function of intake air density and piston speed</td>
 * </tr>
 * <tr>
 *   <td>EngineCompr</td>
 *   <td>'Ncor[rpm]', 'WcompCor[kg/s]','PrComp[-]','EtaComp[-]','P_ref [Pa]','T_ref [K]','TCompDoCrit [K]'</td>
 *   <td>corrected compressor wheel speed, corrected mass flow compressor, compr.efficiency, Reference Pressure, Ref Temp, crit./max temp compressor</td>
 * </tr>
 * <tr>
 *   <td>EngineData</td>
 *   <td>'Engine type, 2-stoke:1, 4-Stroke:2','Engine Cylinders','Engine Displacement [m^3]'</td>
 *   <td>2 or 4 stroke engine, no of cylinders, engine displacement</td>
 * </tr>
 * <tr>
 *   <td>EngineTurbine</td>
 *   <td>'ThVNT','Ncor','PrTurb','WTurbCor','EtaTurb'</td>
 *   <td>vane position VTG, ratio pressure up/downstream turbine, corrected mass flow across turbine, turbine efficiency</td>
 * </tr>
 * </tbody>
 * </table>
 */

const configArray = 
[
 {"name": 'EngineConstants', "data":['Tamb[K]','Pamb[bar]','R [J/kg/K]','Lambda_ref','E_LHV [J/kg]','C_p[J/kg/K]','Gamma(C_p/C_v)','Fuel H/C Ratio']},
 {"name": 'EngineFullLoadData', "data":['Engine type, 2-stoke:1, 4-Stroke:2','Engine Cylinders','Engine Displacement [m^3]']},
 {"name": 'EngineAmbientConditions', "data":['DfuelSp[kg/m^3]/S_p[m/s]']},
 {"name": 'EngineEta', "data":['FExh/Fman']},
 {"name": 'EngineFriction', "data":['Dman [kg/m^3] / S_p [m/s][kPa]']},
 {"name": 'EngineVolumEff', "data":['Dman [kg/m^3] / S_p [m/s]']},
 {"name": 'EngineCompr', "data":['Ncor[rpm]', 'WcompCor[kg/s]','PrComp[-]','EtaComp[-]','P_ref [Pa]','T_ref [K]','TCompDoCrit [K]']},
 {"name": 'EngineFullLoadData', "data":['Engine type, 2-stoke:1, 4-Stroke:2','Engine Cylinders','Engine Displacement [m^3]']},
 {"name": 'EngineTurbine', "data":['ThVNT','Ncor','PrTurb','WTurbCor','EtaTurb']},
 {"name": 'EngineExhaustParam', "data":['Assumed Average Turbine Efficiency','Average Exhaust Specific Heat,Cp[J/kg/K]','Average Exhaust Specific Heat Ratio[-]', 
 'Average Gas Constant, R[J/kg/K','Average Exhaust Gas Visc.[Pa*s]','Piece','Pipe Length[mm]','Outer Pipe Diameter[mm]',
 'pieces','Element length[mm]','Element Diameter[mm]','Element Cell Density[1/m^2]','AT element cell wall thickness[mm]','Fraction of cells blocked','Relative Roughness']},
 {"name": 'EngineRawParticulates', "data":['DFuelSp [kg/m^3] / S_p [m/s]']},
 {"name": 'EngineRawNox', "data":['DFuelSp [kg/m^3] / S_p [m/s]']},
 {"name": 'EngineRoadLoads', "data":['Time[s]','Engine Speed[rpm]','BMEP[bar]']}  
] 






 /**
 * Handle data set
 * @param {object} postgresData - process data in json received from postgres db
 * @param {string} cmpString - key value as a string which decides about what editor and visualization shall be chosen for the data
 * @returns {null}
 */

 /** @function 
 * @description exported function : visualize incoming postgres data
 * <p> - the internals of this function are described in the linked namespace
 * <p>{@link data.d_1_handler}
*/

 export function handleDataset(postgresData,cmpString,socket)
 {
     // line module:modulename ~ function name
   /**
     * Foo object (@see: {@link module:handleModelDatar~data})
     * @name data
     * @inner
     * @private
     * @memberof module:handleModelData
     * @property {Object} data - The foo object
     * @property {Object} data.d_1_handler - The bar object     
     * @property {function} data.d_1_handler.function - The bar object     
     */

//------------------------------------------------------------------------------------------------------------
// Documentation of handleDataset content - start
//------------------------------------------------------------------------------------------------------------   
    /**
     * This namespace describes the function body of the mouse click callback function on
     * a calibration parameter in the function selection view. 
     * <br>
     * The function body executes the following steps
     * <li><a href="data.d_1_handler.html#.remove">1. delete all previous table rows (tr html elements) in the .table class</a></li>
     * <li><a href="data.d_1_handler.html#.JSON_parse">2. Convert postgres data format from JSON to JS and store it in local object</a></li>
     * <li><a href="data.d_1_handler.html#.insertHeadline">3. insert / create headline in .table class according to the currently clicked data element name.</a></li>
     * <li><a href="data.d_1_handler.html#.extractIndexColumn">3. Read index column from engine data</a></li>
     * <li><a href="data.d_1_handler.html#.insertRow">4. Insert all data into the data rows of the table</a></li>
     * <li><a href="data.d_1_handler.html#.colorIndexColumn">5. Color the index column (1st column) if data structure is a 3d map</a></li>
     * <li><a href="data.d_1_handler.html#.createFormattedTableData">6. Create formatted table data for potential storage</a></li>
     * <li><a href="data.d_1_handler.html#.createSetOfCurveStrc">7. Create configuration structure to convert data into a set of curves</a></li>
     * <li><a href="data.d_1_handler.html#.storeHTMLTblDatAsCurves">8. Store html table data as a set of curves in postgres</a></li>         
     * <b>Handling of 3d map objects</b><br>
     * If the name of the database object appears in a list of map names the object describes a 3d map. 
     * <br><br>
     * &nbsp;&nbsp;&nbsp;&nbsp; 8.2.2 Send variable 'outercollector' by emitting the socket.io event 'tabledata' to the server
     * <p></p>
     * @memberof data
     * @type {object}
     * @namespace data.d_1_handler
     */
//------------------------------------------------------------------------------------------------------------
// Documentation of handleDataset content - end
//------------------------------------------------------------------------------------------------------------       

//------------------------------------------------------------------------------------------------------------
// Function insertHeadline - start
//------------------------------------------------------------------------------------------------------------
    /** 
      * @memberof data.d_1_handler
      * @function insertHeadline      
      * @param {string} headline headline text 
      * @description Selects the table identified by its ID attribute 'table' and creates a caption 
      * = headline of the table by filling the innerHTML object member of the caption element with 
      * the headline string.
  */
function insertHeadline(headline) {
  const table = document.getElementById('table');
  // schreibe Tabellenüberschrift
  const caption = table.createCaption();
  caption.innerHTML = headline;
}
//------------------------------------------------------------------------------------------------------------
// Function insertHeadline - end
//------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------
// Function insertRow - start
//------------------------------------------------------------------------------------------------------------
    /** 
      * @memberof data.d_1_handler
      * @function insertRow      
      * @param {string[]} collector column names / column headline numbers
      * @param {string[]} data array of table rows
      * @description This function is called on the receipt of the engine configuration data
      * from postgres. It creates a table out of the header columns (collector) and the data of the postgres
      * engine parameters.<br><br> The function implements the following steps : 
      * <li>select the empty html table element according to the class identifier 'table'</li>
      * <li><b>outer loop = rows iterator</b> : iterate across all data elements plus one headline (this excludes the index column which is extracted before)</li>
      * <li>insert new row at the end of the table with each loop revolution</li>
      * <li><b>inner loop = columns iterator</b> : iterate across all column names</li>
      * <li><b>1st outer loop iteration (iterator equals zero)</b> : content = column names = collector array (call argument)</li>
      * <li><b>consecutive loop iterations</b> : content = data[outer loop iterator][inner loop iterator]</li>
      * <li><b>stop adding table element and break inner loop if content is empty</b></li>
      * <li>insert a new column cell into the row</li>
      * <li><b>Special case 1st row (outer loop iterator equals zero)</b>: set background color to darkblue</li>
      * <li>store content in innerHTML of new cell and make the cell content editable by setting the corresponding attribute</li>
  */
  function insertRow (collector,data) {

    const table = document.getElementById('table');	  
    // loop across all data rows plus headline

    var len = data.length;
    for(var k=0;k<len+1;k+=1)
    {
      // Create 1st row with the column names
      const row = table.insertRow(-1);
      // Row across all column names
      for (var i in collector) 
      { 
         let content; 
        if(k===0)
        {
          // Column name
          content = collector[i];            
        }
        else
        {
          // Data tables
          content = data[k-1][i];
          if (content===null || typeof content===undefined){break;}               
        }
        var zelle = row.insertCell();  
       
        var sel = d3.select(zelle)     
        if(k===0){sel.attr('bgcolor','darkblue');}  
                                        
        var selIn = d3.select(zelle);
        var seldiv = selIn.append('div').attr('contenteditable','true');                      
        seldiv.node().innerHTML = content;	
     }       
    }
  }
//------------------------------------------------------------------------------------------------------------
// Function insertRow - end
//------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------
// Function extractIndexColumn - start
//------------------------------------------------------------------------------------------------------------
    /**    
    * @memberof data.d_1_handler
    * @function extractIndexColumn
    * @param {obj} k iterator over postgres data
    * @param {object[]} configArray configuration array
    * @param {number} length handler function
    * @param {number} cmpString name of calling data object
    * @returns {number[]} 3d map data in case of cmpString parameter 
    * stands for a calibration map. Each row array stands for a column, each
    * element of a row array stands for a row element of the given column. 
    * @description The call parameter 'cmpString' contains the name of
    * the calibration data set which corresponds to the push button. 
    * 'cmpString' is compared to each name in the 'configArray' configuration
    * data structure. If a name in the configuration is equal to call parameter
    * an iteration across the data field of the configuration starts. 
    * <br>
    * <br>
    * <a href="module-d_handleModelData_client.html#~configArray">Find <b>data</b> in the configuration array (click to find the configuration data field)</a>
    * <br><br>
    * <a href="module-d_handleModelData_client.html#~specialMap">If <b>data</b> represents a calibration map function specialMap returns 'true'</a>
    * <br><br>
    * If the data element stands for a calibration map the corresponding columns of the postgres table are pushed to an array named <b>collector</b>.
    */  

     function extractIndexColumn()
     {
      var result=[];
      for(var cnfIt=0;cnfIt<configArray.length;cnfIt++)
      {
       if(configArray[cnfIt].name===cmpString)
       {
           for(var i in configArray[cnfIt].data)
           {            
               for(var k in Obj.columns)
               {
                   if(Obj.columns[k]===configArray[cnfIt].data[i] || specialMap(cmpString))
                   {
                    result.push(Obj.columns[k]);    
                   }               
               }
           }
       }
      }    
      return result;
     }
//------------------------------------------------------------------------------------------------------------
// Function extractIndexColumn - end
//------------------------------------------------------------------------------------------------------------     
/** 
 * @memberof data.d_1_handler
 * @function specialMap
 * @param cmpString name of the data element which corresponds the clicked push button
 * @description The function returns true if cmpString is equal to one of the following strings
 * <li>EngineAmbientConditions</li>
 * <li>EngineEta</li>
 * <li>EngineFriction</li>
 * <li>EngineVolumEff</li>
 * <li>EngineRawParticulates</li>
 * <li>EngineRawNox</li>
*/
function specialMap(cmpString)
{
    switch(cmpString)
    {
        case 'EngineAmbientConditions' : return true; 
        case 'EngineEta' : return true; 
        case 'EngineFriction' : return true; 
        case 'EngineVolumEff' : return true; 
        case 'EngineRawParticulates' : return true; 
        case 'EngineRawNox' : return true; 
    }
}
//------------------------------------------------------------------------------------------------------------
// Function colorIndexColumn - start
//------------------------------------------------------------------------------------------------------------   
   /**
    * Connect
    * @memberof data.d_1_handler
    * @function colorIndexColumn
    * @param {string} cmpString
    * @description Colors background of the index column darkblue. 
    * This function is executed only if the the call parameter 'cmpString'
    * identifies the data element as a 3d data table. This is the case
    * if the function 'specialMap' returns true.
    * All row (tr) nodes of the table (according to class name 'table')
    * are selected. The d3 method 'select('td')' selects only the 1st
    * td / cell element in a row. Setting the background color
    * attribute 'bgcolor' to 'darkblue' colors the 1st column of 
    * a row. 
 */  
function colorIndexColumn(cmpString)
{
  if(specialMap(cmpString))
  {
    var selNds = d3.select('.table').selectAll('tr').nodes();
    for(var it in selNds)
    {     
        d3.select(selNds[it]).select('td').attr('bgcolor','darkblue')
    }
  }
}
//------------------------------------------------------------------------------------------------------------
// Function colorIndexColumn - end
//------------------------------------------------------------------------------------------------------------     

//------------------------------------------------------------------------------------------------------------
// Function createFormattedTableData - start
//------------------------------------------------------------------------------------------------------------   
  /**    
    * @memberof data.d_1_handler
    * @function createFormattedTableData
    * @description Merges all table data, including the index column
    * into one result vector. <br><br>
    * 
    * Outer loop : All table row html elements (tr) of a 3d map are 
    * read into a variable (tableHTML) as an array of html nodes <br><br>
    * Inner loop : the html 'div' elements of all cells of the current
    * table row are read into a nodes array. The data field of all div
    * nodes is pushed into an inner array 'innerCollector'. <br><br>
    * Outer loop (at return from inner loop) : push the innerCollector array 
    * into the result vector, which is an array of 'innerCollector' arrays. 
    * Foo object (@see: {@link module:handleModelData})
    * 
    */  
function createFormattedTableData()
{
  var result = [];
  
  var tableHTML =  d3.select('.table').selectAll('tr').nodes();
  


  for(var outIt=0;outIt<tableHTML.length;outIt+=1)
  {
       var innerCollector = [];
  
       var cells = d3.select(tableHTML[outIt]).selectAll('div').nodes();
       for(var inIt=0;inIt<cells.length;inIt+=1)
       {
           innerCollector.push(cells[inIt].childNodes[0].data);       
       }
       result.push(innerCollector);
  }

  return result;
}
//------------------------------------------------------------------------------------------------------------
// Function createFormattedTableData - end
//------------------------------------------------------------------------------------------------------------   

//------------------------------------------------------------------------------------------------------------
// Function createSetOfCurveStrc - start
//------------------------------------------------------------------------------------------------------------ 
  /**    
    * @memberof data.d_1_handler
    * @function createSetOfCurveStrc
    * @description Creates and initializes a data structure for compressor and turbine
    * objects. This data structures are used to identify a set of curves
    * in a measurement dataset e.g. there is a triplet of measurement data where
    * the 1st element is constant thoughout one dataset. The transition from 
    * one datset to the other is identified by the change of this 1st element. 
    * If it changes, a new dataset starts. One curve in that set of curves would
    * be signal 2 over signal 3 or vice versa with a constant signal 1. <br><br>
    * 
    * name : name of the dataset<br>
    * trig : name of the trigger signal i.e. the signal which is constant during set of curves
    * trigNo : column number of the trigger signal in the dataset
    * colNames : the element names of those elements which will be displayed in curves
    * colNmbrs : array of column numbers - each column number position corresponds to the element name and stands 
    * for the column position of the data element in the data vector
    * databuf : contains the data for one set of curve e.g. in case of 4 signals and the first signal is the <br>
    * trigger signal this is a 3 x n vector with n : amount of samples
    * 
    */  
function createSetOfCurveStrc()
{
  var headAr = [];
    /**
    * Turbine object
    * @memberof data.d_1_handler        
    * @typedef {object} Turbine
    * @property {string} name 'EngineTurbine' string identifier is used to find the correct dataset in the configuration
    * @property {string} trig 'ThVNT' string identifier which describes the x-xis of the data multiple x-axis lead to multiple graphs
    * @property {string} trigNo 0 integer ID that characterizes the number of the x axis
    * @property {string} colNames ['PrTurb','WTurbCor','EtaTurb'] y - value identifiers of the 2d graphs
    * @property {number[]} colNmbrs[3] [0,0,0] initialization data
    * @property {number[]} databuf [] values - initialized as an empty array
    * 
    */     

    /**
     * Instance of turbine object
     * @memberof data.d_1_handler        
     *@param {Turbine} TurbineObj
     *@see {@link data.d_1_handler.Turbine}    
     */
     var TurbineObj = {};
     TurbineObj.name = 'EngineTurbine';
     TurbineObj.trig = 'ThVNT';
     TurbineObj.trigNo = 0;
     TurbineObj.trigVal = null;
     TurbineObj.colNames = ['PrTurb','WTurbCor','EtaTurb'];
     TurbineObj.colNmbrs = [0,0,0];
     TurbineObj.databuf = [];
  
       /**
      * Turbine object
      * @memberof data.d_1_handler        
      * @typedef {object} Compressor
      * @property {string} name 'EngineCompr' string identifier is used to find the correct dataset in the configuration
      * @property {string} trig 'Ncor[rpm]' string identifier which describes the x-xis of the data multiple x-axis lead to multiple graphs
      * @property {string} trigNo 0 integer ID that characterizes the number of the x axis
      * @property {string} colNames ['WcompCor[kg/s]','PrComp[-]','EtaComp[-]'] y - value identifiers of the 2d graphs
      * @property {number[]} colNmbrs[3] [0,0,0] initialization data
      * @property {number[]} databuf [] values - initialized as an empty array
      * 
      */     
     
    /**
      * compressor : Instance of turbine object
      * @memberof data.d_1_handler         
      * @param {Turbine} comprObj
      * @see {@link data.d_1_handler.Compressor}    
      */   
  
     var comprObj = {};
     comprObj.name = 'EngineCompr';
     comprObj.trig = 'Ncor[rpm]';
     comprObj.trigNo = 0;
     comprObj.trigVal = null;
     comprObj.colNames = ['WcompCor[kg/s]','PrComp[-]','EtaComp[-]'];
     comprObj.colNmbrs = [0,0,0];
     comprObj.databuf = [];
     
     /**
      * Array : turbine + compressor object
      * @memberof data.d_1_handler         
      * @param {Turbine[]} headAr
      * @description array of turbine & compressor objects
      * <br>var headAr = [TurbineObj,comprObj];
      * <br>[Turbine Object]{@link data.d_1_handler.Turbine}    
      * <br>[Compressor Object]{@link data.d_1_handler.Compressor}    
      */   
     headAr = [TurbineObj,comprObj];

  return headAr; 
}
//------------------------------------------------------------------------------------------------------------
// Function createSetOfCurveStrc - end
//------------------------------------------------------------------------------------------------------------ 

//------------------------------------------------------------------------------------------------------------
// Function findAndStoreColumnPositions - start
//------------------------------------------------------------------------------------------------------------ 
/**    
    * @memberof data.d_1_handler
    * @function findAndStoreColumnPositions(headAr,headIt)
    * @description Finds the signal names of the column names 
    * configuration array in the header array and stores 
    * the positions in the header array. These are the 
    * positions in the dataset. The position numbers
    * are stored in the 'colNmbrs' of the configuration structure <br>
    * The same principle is used for the trigger signal. The name
    * of the trigger signal is found in the header array and the 
    * position number is stored in the 'trigNo' field. 
    */
  
function findAndStoreColumnPositions(headAr,headIt)
{
     /**  
       * @memberof data.d_1_handler        
       * @params {selection} 
       * @description select all cells in the html data table
      */
      var nds = d3.select('.table').select('tr').selectAll('td').nodes()

     //=====================================================================
     // Find columns numbers of column headlines
     // Find column number of trigger signal
     //=====================================================================  
     for(var iNds=0;iNds<nds.length;iNds+=1)
     {
        
        var innerText = d3.select(nds[iNds]).select('div').node().innerHTML;      
        for(var colIt=0;colIt<headAr[headIt].colNames.length;colIt+=1)
        {
          if(innerText===headAr[headIt].colNames[colIt])
          {
            headAr[headIt].colNmbrs[colIt]=iNds; 
          }
        }
        if(innerText===headAr[headIt].trig)
        {
          headAr[headIt].trigNo = iNds;
        }       
     }

  return headAr;
}
//------------------------------------------------------------------------------------------------------------
// Function findAndStoreColumnPositions - end
//------------------------------------------------------------------------------------------------------------ 

//------------------------------------------------------------------------------------------------------------
// Function createEmptyRowArrays - start
//------------------------------------------------------------------------------------------------------------ 
 
/**    
    * @memberof data.d_1_handler
    * @function createEmptyRowArrays
    * @description Initialize the databuffer field of the curves
    * data structure by pushing empty array into it for each signal
    * column according to the configured signal names. 
*/
function createEmptyRowArrays(headAr,headIt)
{
  for(var colIt=0;colIt<headAr[headIt].colNames.length;colIt+=1)
  {     
   headAr[headIt].databuf.push([])
  }     
  return headAr;
}
//------------------------------------------------------------------------------------------------------------
// Function createEmptyRowArrays - end
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
// Function storeDataOnChangeOfTrigger - start
//------------------------------------------------------------------------------------------------------------
/**    
    * @memberof data.d_1_handler
    * @function storeDataOnChangeOfTrigger    
    * @description The table data is loaded from the html table
    * and split into sets of curves. The sets of curves are
    * send to the server using a socket.io interface. 
    * The server stores the data in postgres.<br>
    * <br>
    * Each row is devided in cells. The cells are organized
    * in columns of a row. Using the configured column position
    * of the trigger signal this function splits a new set of
    * curves by detecting a change in the trigger signal.<br> 
    * On a changed trigger signal a databuffer in the curves
    * datastructure is set to an empty string. The string
    * is filled with each new row as long as the signal in the 
    * trigger columns remains contant. <br>
    * On a change of the trigger signal the previously collected
    * data in the buffer is send to the server where the data
    * is stored in a postgres database before the buffer is freed
    * to receive new data for the next set of curves. 
    */

function storeDataOnChangeOfTrigger(headAr,headIt,inNds,refVal,cntr)
{
       // Find change of trigger signal in current row and store
       // collected array
       for(var iNdsIt=0;iNdsIt<inNds.length;iNdsIt+=1)
       {
       
          var val = parseFloat(d3.select(inNds[iNdsIt]).select('div').node().innerHTML);
          if(iNdsIt === headAr[headIt].trigNo)
          {
            if(!isNaN(val))
            {
              if(val !== refVal)
              {
                  cntr+=1;
                  refVal = val;                                               
                  if(cntr>1)
                  {         
                    /**     
                      * @memberof data.d_1_handler    
                      * @event curvedata
                      * @type {object}
                      * @property {string} table - Indicates whether the snowball is tightly packed.     
                      * @description {@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:curvedata}
                    */                      
                    headAr[headIt].trigVal = val;           
                    socket.emit('curvedata', headAr[headIt]);      
       
                    for(var colIt=0;colIt<headAr[headIt].colNames.length;colIt+=1)
                    {     
                     headAr[headIt].databuf[colIt]=[];
                    }
                    break;                                                    
                  }              
                                    
              }
            }     
          }
        }       
  return [headAr,refVal,cntr];
}
//------------------------------------------------------------------------------------------------------------
// Function storeDataOnChangeOfTrigger - end
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
// Function bufferColumnData - start
//------------------------------------------------------------------------------------------------------------
/**    
    * @memberof data.d_1_handler
    * @function bufferColumnData    
    * @description The table data is loaded from the html table
    * and split into sets of curves. The sets of curves are
    * send to the server using a socket.io interface. 
    * The server stores the data in postgres.<br>
    * <br>
    * Each row is devided in cells. The cells are organized
    * in columns of a row. Using the configured column position
    * of the trigger signal this function splits a new set of
    * curves by detecting a change in the trigger signal.<br> 
    * On a changed trigger signal a databuffer in the curves
    * datastructure is set to an empty string. The string
    * is filled with each new row as long as the signal in the 
    * trigger columns remains contant. <br>
    * On a change of the trigger signal the previously collected
    * data in the buffer is send to the server where the data
    * is stored in a postgres database before the buffer is freed
    * to receive new data for the next set of curves. 
    */
function bufferColumnData(headAr,headIt,inNds)
{
  for(var iNdsIt=0;iNdsIt<inNds.length;iNdsIt+=1)
  {
    
    var valNw = parseFloat(d3.select(inNds[iNdsIt]).select('div').node().innerHTML);
    if(!isNaN(valNw))
    {
      // Iterate over all column names 
     for(var colIt=0;colIt<headAr[headIt].colNames.length;colIt+=1)
     {
       // If column number of current column name is equal to 
       // current column, then push the corresponding data
       if(iNdsIt === headAr[headIt].colNmbrs[colIt])
       {
         headAr[headIt].databuf[colIt].push(valNw);
       }
     }       
   }
  }
  return headAr;
}
//------------------------------------------------------------------------------------------------------------
// Function bufferColumnData - end
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
// Function sendCurveDataToServer - start
//------------------------------------------------------------------------------------------------------------
/**    
    * @memberof data.d_1_handler
    * @function sendCurveDataToServer    
    * @description The table data is loaded from the html table
    * and split into sets of curves. The sets of curves are
    * send to the server using a socket.io interface. 
    * The server stores the data in postgres.<br>
    * <br>
    * Each row is devided in cells. The cells are organized
    * in columns of a row. Using the configured column position
    * of the trigger signal this function splits a new set of
    * curves by detecting a change in the trigger signal.<br> 
    * On a changed trigger signal a databuffer in the curves
    * datastructure is set to an empty string. The string
    * is filled with each new row as long as the signal in the 
    * trigger columns remains contant. <br>
    * On a change of the trigger signal the previously collected
    * data in the buffer is send to the server where the data
    * is stored in a postgres database before the buffer is freed
    * to receive new data for the next set of curves. 
    */
function sendCurveDataToServer(headAr,headIt)
{
    /**
      * @memberof data.d_1_handler         
      * @param {selection}
      * @description all rows in the engine model cal. value table
      */
     var ndsData = d3.select('.table').selectAll('tr').nodes();
     var refVal=0;
     var cntr = 0;

     for(var iNds=1;iNds<ndsData.length;iNds+=1)
     {
       // Find all table data (td) cells in the current table row
       
       var inNds = d3.select(ndsData[iNds]).selectAll('td').nodes();
       [headAr,refVal,cntr] = storeDataOnChangeOfTrigger(headAr,headIt,inNds,refVal,cntr);  
       headAr = bufferColumnData(headAr,headIt,inNds);
       
     }
}
//------------------------------------------------------------------------------------------------------------
// Function sendCurveDataToServer - end
//------------------------------------------------------------------------------------------------------------ 
//------------------------------------------------------------------------------------------------------------
// Function storeHTMLTblDatAsCurves - start
//------------------------------------------------------------------------------------------------------------ 
/**    
    * @memberof data.d_1_handler
    * @function storeHTMLTblDatAsCurves    
    * @description The table data is loaded from the html table
    * and split into sets of curves. The sets of curves are
    * send to the server using a socket.io interface. 
    * The server stores the data in postgres.<br>
    * <br>
    * Each row is devided in cells. The cells are organized
    * in columns of a row. Using the configured column position
    * of the trigger signal this function splits a new set of
    * curves by detecting a change in the trigger signal.<br> 
    * On a changed trigger signal a databuffer in the curves
    * datastructure is set to an empty string. The string
    * is filled with each new row as long as the signal in the 
    * trigger columns remains contant. <br>
    * On a change of the trigger signal the previously collected
    * data in the buffer is send to the server where the data
    * is stored in a postgres database before the buffer is freed
    * to receive new data for the next set of curves. 
    */
function storeHTMLTblDatAsCurves(headAr)
{
  for(var headIt = 0;headIt<headAr.length;headIt+=1)
   {
     /**
     * @memberof data.d_1_handler         
     * @param {selection}
     * @description all rows in the engine model cal. value table
     */ 
    var headline = d3.select('.table').select('caption').node().innerHTML;

    if(headline===headAr[headIt].name)
    {
    /**     
     * @memberof data.d_1_handler    
     * @event initNewData
     * @type {object}
     * @description This event of the web client triggers a callback function on the server side
     * which prepares a new set of curves for editing and visualization.<br><br>Find the details
     * about the server sided event in the link <br>
     * [server : start processing new curve data]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:initNewData}
     */
     socket.emit('initNewData');
     headAr = findAndStoreColumnPositions(headAr,headIt);
     headAr = createEmptyRowArrays(headAr,headIt);
     sendCurveDataToServer(headAr,headIt);
     return true;
    }// End of specific curve type 
   }// End of loop over all curves
   return false;
}
//------------------------------------------------------------------------------------------------------------
// Function storeHTMLTblDatAsCurves - end
//------------------------------------------------------------------------------------------------------------ 


  /**    
  * @memberof data.d_1_handler        
  * @function remove
  * @parameter {string} .table select table class
  * @parameter {string} tr select table row node
  * @description deletes all rows of the html table element. <br>
  * There is one html table class with a potentially filled table. <br>
  * d3.select('.table') selects the one table class. <br>
  * d3.select('.table').selectAll('tr') selects all table rows in it<br>
  * d3.select('.table').selectAll('tr').remove() removes all table rows but
  * lets the surrounding table element untouched. <br>
  */
   d3.select('.table').selectAll('tr').remove();

  /**  
  * @memberof data.d_1_handler        
  * @function JSON_parse
  * @parameter postgresData data from postgres in JSON format
  * @parameter cmpString string selector which describes the data element which is selected by this mouse event
  * @description JSON_parse converts the JSON object into a Javascript object.
  */
   var Obj = JSON.parse(postgresData[cmpString]);

   insertHeadline(cmpString)

   var collector=[];
   collector=extractIndexColumn();   
   insertRow(collector,Obj.data);
   colorIndexColumn(cmpString);
  
   var outerCollector = [];
   outerCollector = createFormattedTableData();
   
   var headAr = []; 
    headAr = createSetOfCurveStrc();
   if(!storeHTMLTblDatAsCurves(headAr))
   {
      /**     
        * @memberof data.d_1_handler    
        * @event tabledata
        * @type {object}
        * @property {string} table - Indicates whether the snowball is tightly packed.     
        * @description {@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:tabledata}
      */
      socket.emit('tabledata', outerCollector);
   } 

   return;  
}

