/**
 * Stimulation Client
 * @module n_create_simulation_view_client
 */

/** @function n_1_startStimulation
 * @description exported function : needed to select and visualize audio data
 * <p> - the internals of this function are described in the linked namespace</p>
 * <h2>Parent</h2>
 * <p>This is the top level script. It is embedded in signalsNOsci.html</p>
 * <h2>Child</h2>
 * <p>[function body n_create_simulation_view_client]{@link  n_create_simulation_view_client.n_1_create_simulation_view}</p>
 * <h2>Emitted socket.io Events and Receivers</h2>
 * <table>
 * <tr>
 * <thead>
 * <th>Initial event emit</th><th>Server : receive event</th><th>Client : get response</th>
 * </thead>
 * <tbody>
 * <td>getAudioFlPths</td>
 * <td>[server reacts on stimulation request]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:getAudioFlPths}</td> 
 * <td>[client receives stimulation data]{@link n_create_simulation_view_client.n_1_create_simulation_view.event:sendAudioFlPths}
 * <tr>
 * </tbody>
 * </table>
*/

var d3 = window.d3;
import {createXYScrollbars} from "./createXYScrollbars.js";

function createInputSignalImage(socket,inCirc,x_,y_,idstr,curObj)
{
  const radius = 10;
  var circgrp = inCirc.append('g').attr('class','incircgrp').append('circle').attr('cx',x_).attr('cy',y_).attr('r',radius).attr('stroke','black').attr('fill','green'); 
  d3.select(circgrp.node().parentNode).append('text').text(idstr).attr('x',x_).attr('y',y_);
  var bboxtxt = d3.select(circgrp.node().parentNode).select('text').node().getBoundingClientRect();
  var txtX = x_ - bboxtxt.width - radius;
  d3.select(circgrp.node().parentNode).select('text').attr('x',txtX);  
  var nodeData = {};
  nodeData.name = curObj.name;
  nodeData.Arraysize = curObj.Arraysize;
  circgrp.data([nodeData]);   
  circgrp.on('click', function(){ 
    var nd= d3.select('.audio').select('p').node();
    var ndtext = nd.innerHTML;
    let result = ndtext.includes("signal no");
    if(result)
    {
      var words = ndtext.split("signal no");
      d3.select(circgrp.node().parentNode).append('text').attr('class','selected').text(words[1]).attr('x',x_-radius/2).attr('y',y_+radius/2);
      var sels_selected= d3.selectAll('.selected').nodes().length;
      var sels_inputs= d3.selectAll('.incircgrp').nodes().length;
      
      if(sels_selected==sels_inputs)
      {
        var nds= d3.selectAll('.incircgrp').nodes(); 
        var sig_vec = [];       
        for(var ndIt=0;ndIt<nds.length;ndIt++)
        {
          var resObj = {};
          var curSel = d3.select(nds[ndIt]).select('circle');
          var dat = curSel.data()[0];
          resObj.name = dat.name;          
          resObj.Arraysize = dat.Arraysize;                
          var nd_nametext = curSel.node().nextSibling;
          var nd_signalidtext = nd_nametext.nextSibling;
          resObj.nameflatelement = nd_nametext.textContent;      
          var signalNoTxt = nd_signalidtext.textContent;      
          var signalNoTxt= signalNoTxt.split(' ').join('');
          var signalId = "#signalno"+ signalNoTxt;
          var waveFilePath = d3.select(signalId).data()[0];
          var path_vec = waveFilePath.split("/");
          resObj.audiofilename= path_vec[path_vec.length-1];

          sig_vec.push(resObj);
        }
    
        socket.emit('prepareStim',sig_vec);
      }
    }
  }); 
  
}
                                
function createOutputSignalImage(socket,outCirc,outx,yo_,idstr,curObj,startFileExample)
{
  const radius = 10;
  var circgrp = outCirc.append('g').attr('class','outcircgrp').append('circle').attr('cx',outx).attr('cy',yo_-20).attr('r',radius).attr('stroke','black').attr('fill','cyan');   
  d3.select(circgrp.node().parentNode).append('text').text(idstr).attr('x',outx+10).attr('y',yo_-20);  
  circgrp.data([curObj]); 
  circgrp.on('click', function()
  { 
    d3.select(circgrp.node().parentNode).append('text').text("x").attr('x',outx-radius/2).attr('y',yo_-20+radius/2);
    
    /**     
      * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
      * @function onstimFileSelector_click      
      * @property {number} data name of the emitted event
      * @property {function} function(data) processes the received data
      * @description Callback function of mouse click on the wav file 
      * source button. The callback deletes a preexisting audio 
      * node and creates a new one from the selected data. The
      * selection happens by clicking the push button which corresponds
      * to a wav file. 
      * <br>[start simulation creates the audio context]{@link module:k_select-stimulation_client}
    */               
    
     
        var pth = d3.select(this).data()[0].name+"16k.wav";    
        pth = "./simulationout/" + pth;
        if(d3.select('.audio').node()!==null)
        {
            var nd = d3.select('.audio').node();          
            nd.remove();
        }        
        startFileExample(pth,d3.select,Oscilloscope);   
        var elementText =  this.innerHTML;
        d3.select('.audio').select('p').text(elementText)
        var audio = document.getElementsByClassName("myAudio");
        audio[audio.length-1].loop = true;
        audio[audio.length-1].load();
    

    //var name = this.nextSibling.textContent;  
    //var sig_vec = [];
    //sig_vec.push(name);    
    //var dat = d3.select(this).data();  
    //sig_vec.push(dat);  
   // socket.emit('registerDAQ',sig_vec);
  });
}

function  createSimulationView(socket,startFileExample,Oscilloscope){

  /**
     * Foo object (@see: {@link module:audio-element~startFileExample})
     * @name n_create_simulation_view_client
     * @inner
     * @private
     * @memberof module:n_create_simulation_view_client
     * @property {Object} n_create_simulation_view_client - The foo object
     * @property {Object} n_create_simulation_view_client.n_1_create_simulation_view - The bar object     
     * @property {Object} n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths - The bar object     
     */
 
   /**
     * Generate inner namespace of the startFileExample
     * @memberof n_create_simulation_view_client
     * @type {object}
     * @namespace n_create_simulation_view_client.n_1_create_simulation_view
     * @description <h2>Parent</h2> [function call n_create_simulation_view_client]{@link module:n_create_simulation_view_client}
     * <h2>Child</h2>
     * [function body n_create_simulation_view_client]{@link  n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths}
     * 
     */


    /**     
      * @memberof n_create_simulation_view_client.n_1_create_simulation_view    
      * @event periodic_time
      * @type {object}
      * @property {function} console.log(timeString)      
      * @description This event is currently triggering an almost empty 
      * function which only displays the timestamps sent by the server. 
      * <br>[server: create basic time period]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:time}
    */ 
     socket.emit('getMdxDataSize');                 
     socket.on('sendMdxDataSize',function(res)
     {           
        if(res.hasOwnProperty('exact_count'))
        {
          var comp = parseInt(res.exact_count);
          if(comp>0)
          {            
            d3.select('#xml').style('background-color', "green");
            d3.select('#xml').text('Overwrite existing XML data');
          }
          else
          {
            socket.emit('getTableLength');
            socket.emit('deletePrevTable');
            d3.select('#xml').style('background-color', "red");
            d3.select('#xml').text('Initial read of XML data');
          }
        }
        else
        {
          socket.emit('getTableLength');
          socket.emit('deletePrevTable');
          d3.select('#xml').style('background-color', "red");
          d3.select('#xml').text('No counter defined');
        }
      });

      socket.on('sendTableLength',function(obj)
      {
        var objBuf = obj.rows;
        console.log(obj.rows[obj.cnt].id);        
        var cnt = obj.cnt;
        cnt++;
        socket.emit('getNextASAMMdxFile',cnt); 
        socket.on('ASAMDataReady',function(dat)
        {            
          cnt++;          
          if(cnt<objBuf.length)
          {
            var reqID = objBuf[cnt].id;
            socket.emit('getNextASAMMdxFile',reqID);                     
            d3.select('#xml').style('background-color', "red");
            d3.select('#xml').text('Read all xml files');
          }  
          else
          {
            d3.select('#xml').style('background-color', "green");
            d3.select('#xml').text('All xml files are read');
          }          
        });
      });


     d3.select('#xml').on('click',function()
     {
         socket.emit('getTableLength'); 
         socket.emit('deletePrevTable');
     });      


    socket.on('time', function(timeString) {
        console.log(timeString)
        socket.emit('getStatusInfo',1);
      });

      socket.on('sendStatusInfo',function(data){
        console.log(data);
        var state = data.toolsctrl.stim_state;
        if(state.localeCompare("stim_processed")==0)
        {
          socket.emit('prepareDaq',1);
        }
      });
      const noInputs = 5;
      const noOutputs = 7;
      
    /**     
      * @memberof n_create_simulation_view_client.n_1_create_simulation_view    
      * @event getAudioFlPths      
      * @property {string} getAudioFlPths name of the emitted event
      * @property {number} constant 1 : number of the requested mode
      * @description This event is requesting stimulation data from the server
      * which is stored as a set of audio files on the server host engine. 
    */ 
      socket.emit('getAudioFlPths',1);
      socket.emit('getIOSignals',1);
      socket.on('sendIOSignals', function(data) 
      {
        console.log(data);
        var lhs_var_vec = [];
        var rhs_var_vec = [];
        for(var i=0;i<data.length;i++)
        {
          if(!data[i].isLHS.localeCompare("true"))
          {                    
            lhs_var_vec.push(data[i]);            
          }
          else 
          {
            rhs_var_vec.push(data[i]);
          }      
        }

        var noInputs = rhs_var_vec.length;
        var noOutputs = lhs_var_vec.length;
        
        /**     
          * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
          * @function append_in_out_table_grp
          * @property {string} body body of the html 
          * @property {string} table html table
          * @property {class} inandout name of the table class
          * @description Creates an empty table and appends an html group. The table is used as a framework
          * to place push buttons for input and output signals. 
          * <br>[start simulation creates the audio context]{@link module:k_select-stimulation_client}
        */               
        var grpSel = d3.select('body').append('table').attr('class','inandout');//.append('g');

        /**     
          * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
          * @type {number}
          * @description number of rows of the I/O is the maximum of the number of inputs and outputs
        */
        var norows = noInputs > noOutputs ? noInputs:noOutputs;
        var x_= 200;
        var y_= 100;
      
        var svgGrp =null;
        
        var svg = d3.select('body').append('svg');
        svgGrp = svg.append('g').attr('id','graph');


        svgGrp.append('g').attr('class','inheadline').append('text').text('Input values').attr('x',x_-140).attr('y',30).style("font-size", "34px");
        svgGrp.append('g').attr('class','outheadline').append('text').text('Output values').attr('x',x_+120).attr('y',30).style("font-size", "34px");

        /**     
          * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
          * @function createIOFrame
          * @param {string} inval
          * @param {string} outval
          * @param {string} plant
          * @param {number} rectWidth
          * @param {number} rectHeight
          * @description number of rows of the I/O is the maximum of the number of inputs and outputs
        */

        var inCirc = svgGrp.append('g').attr('class','inval');
        var outCirc = svgGrp.append('g').attr('class','outval');
        // var plantSel = svgGrp.append('g').attr('class','plant');        
        var outx = x_ + 120;
        // plantSel.append('rect').attr('x',x_+100).attr('y',0).attr('width',rectWidth).attr('height',rectHeight).attr("fill",'blue');  

        /**     
          * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
          * @function getBoundingClientRect
          * @param {string} plant
          * @description get the coordinates of the 
        */
        var Rect = svg.node().getBoundingClientRect();
        var dYI = 0.9*(Rect.height)/noInputs;
        var dYO = 0.9*(Rect.height)/noOutputs;

        dYI = dYI<dYO ? dYI:dYO;

        /**     
          * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
          * @function forAllRows
          * @param {string} plant
          * @description get the coordinates of the 
        */
         var idstr_vec = [];
        for(var i=0;i<norows;i+=1)
        {    
          if(i< noInputs)
          {
            /**     
              * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
              * @function createInputSignalImage
              * @param {object} grpSel,inCirc,x_,y_,idstr,i
              * @param {object} inCirc
              * @param {number} x_
              * @param {number} y_
              * @param {string} idstr
              * @param {number} i
              * @description get the coordinates of the 
            */
            var idstr = rhs_var_vec[i].name;    
            var noArrEls = parseInt(rhs_var_vec[i].Arraysize);
            
            if(noArrEls>1)
            {
             for(var k=0;k<noArrEls;k++)
             {
                idstr =  rhs_var_vec[i].name + "_ar_" + k;
                idstr_vec.push(idstr);
                createInputSignalImage(socket,inCirc,x_,y_,idstr,rhs_var_vec[i]); 
                y_ += dYI;           
             }                           
            }
            else
            {
              idstr_vec.push(idstr);
              createInputSignalImage(socket,inCirc,x_,y_,idstr,rhs_var_vec[i]);            
            }                        
          }
          
          if(i< noOutputs)
          {
            /**     
              * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
              * @function createOutputSignalImage
              * @param {object} grpSel,inCirc,x_,y_,idstr,i
              * @param {object} outCirc
              * @param {number} outx
              * @param {number} y_            
              * @param {number} i
              * @description get the coordinates of the 
            */
             var idstr = lhs_var_vec[i].name;             
             var noArrEls = parseInt(lhs_var_vec[i].Arraysize);
             
             if(noArrEls>1)
             {
              for(var k=0;k<noArrEls;k++)
              {
                 idstr = lhs_var_vec[i].name + "_ar_" + k;
                 idstr_vec.push(idstr);
                 createOutputSignalImage(socket,outCirc,outx,y_,idstr,lhs_var_vec[i],startFileExample);               
                 y_ += dYI;
              }                           
             }
             else
             {
                 idstr_vec.push(idstr);
                 createOutputSignalImage(socket,outCirc,outx,y_,idstr,lhs_var_vec[i],startFileExample);               
             }

          }
          
          y_ += dYI;
        }
        
        socket.emit('createDAQSTIMTable',idstr_vec);
        createXYScrollbars('graph');
        //var inCircRect = d3.select('.inval').node().getBoundingClientRect();
        //var outCircRect = d3.select('.outval').node().getBoundingClientRect();
        //var grpRect = svgGrp.node().getBoundingClientRect();
 
        //var YshiftOut = (Rect.bottom - outCircRect.bottom)-(Rect.height - outCircRect.height)/2;
        //var YshiftIn = (Rect.bottom - inCircRect.bottom)-(Rect.height - inCircRect.height)/2;
        //var YshiftGrp = (svgRect.bottom - grpRect.bottom)-(svgRect.height - grpRect.height)/2;
        //var XshiftGrp = (svgRect.right - grpRect.right)-(svgRect.width - grpRect.width)/2;
 
        //d3.select('.inval').attr('transform','translate(0,'+YshiftIn+')');
        //d3.select('.outval').attr('transform','translate(0,'+YshiftOut+')');
        //svgGrp.attr('transform','translate('+XshiftGrp+','+YshiftGrp+')');

      });

    /**     
      * @memberof n_create_simulation_view_client.n_1_create_simulation_view    
      * @event sendAudioFlPths      
      * @property {string} sendAudioFlPths name of the emitted event
      * @property {function} function(data) processes the received data
      * @description This event is requesting stimulation data from the server
      * which is stored as a set of audio files on the server host engine. 
      * [process stimulation data]{@link n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths}
    */ 
      socket.on('sendAudioFlPths', function(data) {

   /**
     * Generate inner namespace of the startFileExample
     * @memberof n_create_simulation_view_client.n_1_create_simulation_view    
     * @type {object}
     * @namespace n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
     * 
     */

        /**
        * Generate inner namespace of the startFileExample
        * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
        * @function append_svg
        * @param {string} body append svg object to existing body
        * @description create a 1st area for scalabe vector graphics, 
        * 
        */
        var svg = d3.select('body').append('svg');
        /**
        * Generate inner namespace of the startFileExample
        * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
        * @function append_table_group
        * @param {string} body append table object to existing body
        * @param {string} table append table object
        * @param {attribute} class 'signal'
        * @param {string} g group element of HTML DOM 
        * @description append a table to the existing html body, indicate the table
        * as a class names 'signals' and append a HTML group to the table. 
        */
        var grpSel = d3.select('body').append('table').attr('class','signals').append('g');
        var lock = false;

        /**
        * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
        * @function for_data_iterator
        * @param {number} 0 iterator initial value 
        * @param {number} i iterator
        * @param {number} length stop condition of the loop is the data length 
        * @description iterator over all data elements. Once the data is not empty
        * the underlying audio file is further processed. 
        */
        
        for(var i=0;i<data.length;i+=1)
        {    
          if(data[i]!=="")
          {     
            if(lock===false)                      
            {
                /**
                  * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
                  * @function startFileExample
                  * @param {number} 0 iterator initial value 
                  * @param {number} i iterator
                  * @param {number} length stop condition of the loop is the data length 
                  * @description iterator over all data elements. Once the data is not empty
                  * the underlying audio file is further processed. 
                  * <br><br>[start simulation]{@link module:k_select-stimulation_client}
                */
              startFileExample(data[i],d3.select,Oscilloscope);
              
              lock=true;
              
            }

            /**
              * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
              * @type {string}
              */
            var str = "signal no " + i;
            /**
            * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths
            * @type {string}
            */
            var idstr = "signalno" + i;      

            /**     
             * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
             * @event click_stimFileSelector      
             * @property {number} data name of the emitted event
             * @property {function} function(data) processes the received data
             * @description This event is requesting stimulation data from the server
             * which is stored as a set of audio files on the server host engine. 
            */ 
            grpSel.append('tr').append('td').append('button').text(str).attr('id',idstr)
              .data([data[i]])
              .on('click',
                /**     
                  * @memberof n_create_simulation_view_client.n_1_create_simulation_view.n_1_1_rcvAudioFilePaths    
                  * @function onstimFileSelector_click      
                  * @property {number} data name of the emitted event
                  * @property {function} function(data) processes the received data
                  * @description Callback function of mouse click on the wav file 
                  * source button. The callback deletes a preexisting audio 
                  * node and creates a new one from the selected data. The
                  * selection happens by clicking the push button which corresponds
                  * to a wav file. 
                  * <br>[start simulation creates the audio context]{@link module:k_select-stimulation_client}
                */               
                function()
                {
                    var pth = d3.select(this).data()[0];
                    if(d3.select('.audio').node()!==null)
                    {
                        var nd = d3.select('.audio').node();          
                        nd.remove();
                    }        
                    startFileExample(pth,d3.select,Oscilloscope);   
                    var elementText =  this.innerHTML;
                    d3.select('.audio').select('p').text(elementText)
                    var audio = document.getElementsByClassName("myAudio");
                    audio[audio.length-1].loop = true;
                    audio[audio.length-1].load();
                });       
          }
        }        
      });      
}

export {createSimulationView}