//=========================================================================================
//
//             createNvisHirarch.js : visualize conversion steps from C to a simulation
//             executable
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
 import {processRawIniDirPath} from "./processRawIniDirPath.js";
 import {createXYScrollbars} from "./createXYScrollbars.js";

 export function createNvisHirarch(socket)
 {
    function list_to_tree_pg(list,id,prntid) {
        var map = {}, node, roots = [], i;
        
        for (i = 0; i < list.length; i += 1) {
          map[list[i][id]] = i; // initialize the map
          list[i].children = []; // initialize the children 
        }
        
        for (i = 0; i < list.length; i += 1) {
          node = list[i];
          //if (node[prntid] !== "0.") {
          if (node.hasOwnProperty(prntid) && node[id]!="0.") {
            if(node[prntid]=="")
            {
                node[prntid]="0.";
            }
            // if you have dangling branches check that map[node.parentId] exists
            list[map[node[prntid]]].children.push(node);
          } else {
            roots.push(node);
          }
        }
        return roots;
      }

    function createTree(flatData,index,parentid)
    {
        var treeData = list_to_tree_pg(flatData,index,parentid);                
        var root = d3.hierarchy(treeData[0]);
        return root;
    }


    function nodeClicked(data,nd)
    {
        var graphdata = data;
        var pID = d3.select(nd).data()[0].data.index;
        if(!d3.select(nd).data()[0].data.isFile.localeCompare("true"))
        {
            var fPth = d3.select(nd).data()[0].data.filepath;
            socket.emit('reqCFileAnalysis',fPth);
        }
        else
        {
            socket.emit('getCFiles',pID);
            socket.on('sendCFiles',function(data){                
            for(let i=0;i<data.length;i++)    
            {
                graphdata.push(data[i]); 
            }                
            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }                                                                  
            graphdata = graphdata.filter(onlyUnique);               
            d3.selectAll(".link").remove();
            d3.selectAll(".circgrp").remove();
            var root = createTree(graphdata,"index","parentID");
            createGraph(root,graphdata,"filepath",1400,800);
        });
        }
        socket.emit('getheaderFiles',1);
        socket.on('sendHeaderfiles',function(data){
            console.log(data);                
          });
    }

    function createGraph(root,postgresdata,property,x,y)
        {
            var clusterLayout = d3.cluster().size([x,y])
            clusterLayout(root);

            // Nodes
            d3.select('svg').select('.nodes').selectAll('g')
            .data(root.descendants())
            .enter()
            .append('g').attr('class','circgrp')
            .append('circle')
            .classed('node', true)
            .attr('cx', function(d) {return d.x;})
            .attr('cy', function(d) {return d.y;})
            .attr('r', 10).on('click',function()
            {
                var dat = d3.select("#graph").data()[0];
                nodeClicked(dat,this);
            });
    
            d3.select("#graph").data([postgresdata]);
    
            var slCrc = d3.selectAll('.circgrp');
            slCrc.append('text').text(function(d) 
            {                
                var words = d.data[property].split('/');
                var txt = words[words.length-1]
                if(d.data.hasOwnProperty('opcodestr'))
                {
                    txt +="\n" + d.data.opcodestr;
                }
                return txt;
            })//.attr('x',function(d) {return d.x;}).attr('y',function(d) {return d.y;});
            .attr('transform',function(d) 
                { 
                 return "rotate(90) translate("+(20+d.y)+","+(-d.x)+")";});
         
    
            // Links
            d3.select('svg g.links')
                .selectAll('line.link')
                .data(root.links())
                .enter()
                .append('line')
                .classed('link', true)
                .attr('x1', function(d) {return d.source.x;})
                .attr('y1', function(d) {return d.source.y;})
                .attr('x2', function(d) {return d.target.x;})
                .attr('y2', function(d) {return d.target.y;});                    
        }



    d3.select('#draw_headertree').on('click', function(){
        socket.emit('getHeadertree',1);    
    });

    var isInitialized = 0;
    socket.on('time',function()
    {        
        if(isInitialized === 0)
        {
            socket.emit('getSourceCodePath',1);    
            isInitialized=1;
        }        

        if(isInitialized === 2)
        {
            socket.emit('getDirectoryStruct',1);    
            isInitialized=1;
        }        
    });
    
    socket.on('sendSourceCodePath',function(postgresdata)
    {         
    

        if(isInitialized===1)
        {
            if(!postgresdata.toolsctrl.isprocessed.localeCompare("false"))
            {                
                processRawIniDirPath(postgresdata.inidir.pathnames,socket);      
                isInitialized=2;
            }         
            else {
                isInitialized=0; 
            }
        }         
    });
    socket.on('sendHeaderTree',function(headertree)
    {  
        var obj = {};
        obj.CFile = "root";
        obj.Header = "root";
        obj.id = "0.";        
        obj.sampleno = "0";
        headertree.push(obj);


        d3.select('#graph').select('.links').selectAll('.link').remove();        
        d3.select('#graph').select('.nodes').selectAll('.circgrp').remove();        
        var initial_root = createTree(headertree,"id","parentid");
        createGraph(initial_root,headertree,"Header",5000,2000);        
        createXYScrollbars('graph');
    });

    socket.on('sendDirectoryStruct',function(postgresdata)
    {                     
        var initial_root = createTree(postgresdata,"index","parentID");
        createGraph(initial_root,postgresdata,"filepath",1400,800);
        createXYScrollbars('graph');
    });


 }
 