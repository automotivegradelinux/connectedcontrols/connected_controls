## The Connected Controls Project

An increasing amount of current automotive vehicles is getting connected to the internet. This offers new perspectives for controls 
strategies which were not possible in previous vehicle controls approaches. By gathering statistical data from a fleet of vehicles it will become possible to quantify expectation values and tolerances.

In general there are three types of statistical behavior which are of special interest for this project : 
1. vehicle traffic e.g. a expected value of accelerations at a certain area in a city at a certain point of time 
2. sensors / observers of technical processes e.g. NoX sensors
3. the technical process / the mathematical model of the process. 

The goal of this project is to provide solutions for connected controls strategies by using information from the internet. The information gathered over the internet allows to describe uncertainties in the technical process much more precise than in the past. To understand the impact of statistical information we provide the following models :

1. algebraic single combustion engine
2. dynamic model of an average combustion engine
3. noise models for sensors and technical processes involved in engine controls
4. noise propagation analysis which describes the engine reaction on noise

Implementation details : 
- all solutions shall be provided by a web application written in Node.js
- algebraic models shall be provided by IPython notebooks
- dynamic models shall be implemented in Python but may be expanded into open modellica descriptions
- the refactoring of previous embedded controls software shall be based on CLANG/LLVM compiler tools

The project releases its deliveries in several steps. It starts out with a set of tools and web browser views  which converts from a given C source code with given XML based interface model into a simulation executable. The simulation executable uses postgres interfaces to receive stimulation values for each simulation time stamp and sends the result vectors for each time stamp back to postgres. The project uses calibration data given by HTML files which are stored as JSONB data in postgres. The calibration data is visualized in a special calibration view in the project's web browser. The same web browser view is reused to calibrate physical models. 

The C code conversion from C + XML into a simulation module starts out with a simple example and will be enhanced such that C code and function mockup interface descriptions created by open modellica code expanders may be simulated. 

A 2nd goal is to enhance the XML parser such that a limited set of Autosar or MSR interface description may be translated into conversion rules from physical representations into engine control unit representation and vice versa. This will then cover the necessity of co-simulating legacy combustion engine control modules with models created by the open modellica tool set. 

A use case of this project is to implement fleet wide combustion engine controls strategies. This use case makes it necessary to get a basic understanding of the combustion engine. A further aspect of this project is the discussion of combustion engines. For a basic understanding an algebraic engine model will be provided by a IPython notebook description. An Jupyter server will be added to this project once the compiler toolchain framework is up and running. 

After the algebraic engine model is available and validated, a dynamic combustion engine model will be derived as a open modellica description. The open modellica code will be simulated by the simulation environment and the legacy control modules as well as newly developed control modules in open modellica will be used to control the virtual engine. 

From a dynamic single engine simulation this project will extrapolate to a fleet of engine and a fleet of vehicles. By introducing the vehicle into the emissions discussion it is important to describe a typical vehicle behaviour in a realistic driving situation. Once this project stage will be reached, a link to a parallel project will be implemented. The parallel project provides models for typical traffic and driving situations. 

Once the control modules are available they will be translated into a linux system on a chip (SOC) configuration which allows the module usage in linux embedded hardware which may be used to control parts of the engine or gather sensor data or they may be used get a comparison between new controls approaches and the legacy controls from the vehicle fleet. 

### How to use this project

#### Clone this git to your working destination
Create a local clone on your working directory (git clone ...).

#### build server : the gitlab-runner
The project software assumes that all build artefacts are built by a gitlab-runner which runs as a docker container on a build server. 



As there is no central server available for this project yet, it is currently still necessary to install the gitlab runner on a docker host which is accessible by your gitlab environment. For single users it makes sense to install the docker image on the docker host of your local machine. 

[Install latest gitlab runner](https://hub.docker.com/r/gitlab/gitlab-runner)      

#### gitlab environment and remote git

The gitlab environment is also taken from dockerhub. Single users may also install this image on the same local engine. 

[Install gitlab](https://hub.docker.com/r/gitlab/gitlab-ce)      

Once the gitlab environment is installed one has to create a blank project. One may call it "connected_controls" or choose any other name. Follow the link to find out how a blank project is created in gitlab. 

[create a blank project](https://docs.gitlab.com/ee/user/project/working_with_projects.html)

Once the blank project is there, the blank project's automatically generated readme.md explains how to define the repository in your blank project as the remote repository. One may change the previous default remote repository "origin" by using the command : 

git remote set-url origin http://localhost/USERNAME/REPOSITORY.git

To fill the blank project with content one has to push it to the the remote repository. For example by executing : 

git push origin master

The remote repository is now filled with contents. The remote repository contains a .gitlab-ci.yml which contains the commands for the contineous integration and deployment pipeline. Follow the link to find the details about how to connect the gitlab runner to the repository such that the pipeline can be executed :

[Run and register gitlab runner for your project](https://docs.gitlab.com/runner/install/docker.html)      

By calling :

docker container inspect gitlab-runner

on your docker host, one finds the mounted volumes of the gitlab runner.    

Inside the report one finds : 

"HostConfig": {                                                                                                                                                                
            "Binds": [                                                                                                                                                                 
                "/home/your_user/srv/gitlab-runner/config:/etc/gitlab-runner",                                                                                                              
                "/var/run/docker.sock:/var/run/docker.sock"                                                                                                                            
            ], 
...

The directory /home/your_user/srv/gitlab-runner/config contains the file config.toml.

There you find the configuration details for your registered runner. There are several changes necessary : 

1. set privileged = true    
2. add a the url of your remote repository as the clone url (https://myname.dsmynas.org:30445 is the url name of the remote repository)
3. set bind mounts between the gitlab runner and the host : volumes = ["/certs/client", "/cache", "/usr/local:/usr/local/dbtools:rw", "/home/your_username/llvm/llvm-project:/    llvm-project:rw", "/your_username/maha/pipeline_storage:/pipeline_storage:rw"]        

The result may look like the following code snippet : 

\[\[runners\]\]    
  name = "Connected controls releases"    
  url = "https://myname.dsmynas.org:30445/"    
  token = "z7DjHvzzy55txEMtKt11"    
  executor = "docker"    
  clone_url = "https://myname.dsmynas.org:30445"    
  \[runners.custom_build_dir\]     
  \[runners.cache\]     
    \[runners.cache.s3\]     
    \[runners.cache.gcs\]       
    \[runners.cache.azure\]     
  \[runners.docker\]    
    tls_verify = false    
    image = "docker"    
    privileged = true    
    disable_entrypoint_overwrite = false    
    oom_kill_disable = false    
    disable_cache = false    
    volumes = ["/certs/client", "/cache", "/usr/local:/usr/local/dbtools:rw", "/home/your_user/llvm/llvm-project:/    llvm-project:rw", "/home/your_user/pipeline_storage:/pipeline_storage:rw"]        
    shm_size = 0    

After the config.toml file is edited and stored one has to restart the gitlab-runner by 

calling docker gitlab-runner restart. 

The gitlab runner should now be visible under the setting for CI/CD in gitlab. Should the tags not be spelled correctly there is a chance to correct the spelling. If all tags are declared and correctly spelled, the gitlab-ci.yml pipeline is executing and the results are stored in "/home/your_user/pipeline_storage".

Listing the directory content with : 
ls ./pipeline_storage/      yields :

docker-compose.yml  docs  gui.tar  uidoc

By calling 

docker load < gui.tar 

the created gui.tar is loaded to the list of docker images.

By calling 

docker-compose up 

the docker-compose.yml gets executed. Docker-compose loads a postgres data base environment, pgAdmin4 as a postgres administration user surface and the freshly created image into the docker runtime and starts the corresponding containers after postgres is indicating its availablity. The folders "docs" and "uidoc" contain the html documentations of the project. 

### Planned project steps (due dates)

#### June 3rd 2022 : 

Release the compiler tool chain documentation and the node.js environment including the framework for the web browser views. 

#### June 20st 2022 : 

1. install the clang / llvm compiler framework on the local build server
2. analyze directory trees and file dependencies
3. introduce a tree view for directories, files and file dependencies
4. introduce a tool control surface

#### July 1st 2022 : 

1. introduce a CMake build environment and create linux archives for selected files
2. replace raw lexer for the file dependency analysis by a CLANG preprocessor object

#### July 30st 2022 : 

1. introduce the CLANG AST node analysis and enhance the child parent views to visualize call trees andstatement trees 
2. merge a static and dynamic engine model as toy project for module simulations

#### September - December 2022 : 

Start physical modelling, co-simulation of physical models versus legacy software modules. 
Use modules in a SOC for closed loop controls in a vehicle. 