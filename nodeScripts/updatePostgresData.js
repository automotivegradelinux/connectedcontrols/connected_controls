//=========================================================================================
//
//             updatePostgresData.js : update a postgres table by deleting the previous
//             and inserting the new one
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Update method : update existing postgres data
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {object} data data content in JS object format
 * @module j_updatePostgresData_server
 */
module.exports = function(table,column,data)
{
     /**
      * Pool of postgres clients object
      * @typedef {Object} Pool
      * @property {string} user - postgres user
      * @property {string} host - posgres host
      * @property {string} database - database name
      * @property {string} password - password
      * @property {number} port - port
      * @property {method} connect - connection method
      */
    const {Pool} = require('pg');

    const pool = new Pool({
        user: 'ThermBaseAlgebraic',
        host: 'db',
        database: 'ThermoBaseDataStationary',
        password: 'thermo',
        port: 5432
    });
    /**
      * @method pool_connect_then
      * @description The postgres query consists of the following to arguments
      * <ol><li>table</li>
      * <li>qrstring = 'SELECT * FROM ' + 'public.' + '"'+table+'"' => SQL command</li></ol>
      * <br>
      * If the selected and read postgres table is empty, the setter API stores the data directly. 
      * If the read postgres data is not empty, the table is deleted before the setter API stores
      * the dataset. 
      */
    pool.connect().then(client => {
      var qrstr ='SELECT * FROM ' + 'public.' + '"'+table+'"';
        return client
          .query(qrstr)
          .then(res => {            
            console.log(res.rows.length) 
            const pgSet = require('./setPostgresData.js');
            if(res.rows.length==0)
            {              
              console.log('emtpy');
              pgSet(table,column,data);
              client.release()
            }
            else {              
                var qrstr ='DELETE FROM ' + 'public.' + '"'+table+'"';
                  client
                    .query(qrstr)
                    .then(res => {
                      pgSet(table,column,data);
                      console.log(res);
                      client.release()            
                    })
                    .catch(err => {
                      client.release()
                      console.log(err.stack)
                    })              
            }                       
          })
          .catch(err => {
            client.release()
            console.log(err.stack)
          })
      });
}

