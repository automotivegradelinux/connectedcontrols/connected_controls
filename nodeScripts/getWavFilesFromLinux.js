//=========================================================================================
//
//             getWavFilesFromLinux.js : get sound files from docker container file share
//             using linux system commands
//             
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//

/**
 * <h3>Load Stimulation Data from wav Files</h3>
 * <p> The node.js package is configured to run in a linux ubuntu based docker container.  
 * <br> By using the Node.js "child_process" package the available wav files in a 
 * testdata folder is checked an the paths list is sent to the simulation view 
 * web client of the project</p> 
 * <p>See {@tutorial staticEngineModel}</p>
 * <h3>Parent module</h3>
 * <p> Index.js See {@link module:socketCom}</p>
 * <h3>Child modules</h3>
 * <p>There are no children. This is the lowest call level.</p>
 
 * 
 * @module e_getModelStimulationData_server
 */
module.exports = function(socket)
{
    /**     
     * @function exec 
     * @description this is the constructor of the exec function
     * 
    */
    const { exec } = require("child_process");

    /**     
      * @type {Array.<string>}
      */
    var signal_paths = [];
    /**     
      * @function exec()
      * @param {string} cmd "rm &ensp; -f &ensp; testdata/*.txt"
      * @param {function} fnc (error, stdout, stderr)
      * @description remove all textfiles
      */
    exec("rm -f testdata/*.txt", (error, stdout, stderr) => {
      if (error) {
          console.log(`error: ${error.message}`);
          return;
      }
      if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
      }
      console.log(`stdout: ${stdout}`);
      /**     
        * @function exec()
        * @param {string} cmd "ls testdata/*.wav"
        * @param {function} fnc (error, stdout, stderr)
        * @description load all wav file paths to signal_paths string
        */
      exec("ls testdata/*.wav", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);        
            return;
        }    
        signal_paths = stdout.split('\n');   
        /**      
          * @event sendAudioFlPths
          * @type {object}
          * @property {function} emit emits the socket.io event
          * @param {string} sendAudioFlPths name of the socket.io event
          * @param {string} signal_paths string of wav file paths
          * @description send signal paths to web client (simulation environment)
          * <a href="n_create_simulation_view_client.n_1_create_simulation_view.html#.event:sendAudioFlPths"> link : client receive stimulation </a>
          */
        socket.emit('sendAudioFlPths',signal_paths);
    });
    });
}