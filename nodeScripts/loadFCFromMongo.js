//=========================================================================================
//
//             loadFCFromMongo.js : copy selected C + XML files to destination 
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
const copyFC = require('./copyFunctionComponent');
const createCmake = require('./createCmakeLists');

function isNewData(pathCollector,filepath)
{
    var result = true;
    for(var i=0;i<pathCollector.length;i++)
    {        
        if(!pathCollector[i].filepath.localeCompare(filepath))
        {
            result = false;
        }
    }
    return result;
}
function isNewHeaderData(Collector,dat)
{
    var result = true;
    for(var i=0;i<Collector.length;i++)
    {        
        if(!Collector[i].Header.localeCompare(dat.Header))
        {
            result = false;
        }
    }
    return result;
}
module.exports = function(clientdata)
{
    // Connect to socket.io
    return new Promise((resolve, reject) => 
    {
        clientdata.on('connection', function(socket)
        {
            socket.on('Cfile',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 
                    dbo.collection("CFiles",function(err,coll)
                    {
                        coll.find({},function(err,cursor)
                        {
                            var pathCollector = [];
                            if(cursor !== null)
                            {
                                var data_vec = [];
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {                                                       
                                        var reqFileStr_vec = reqData.path.split('/');
                                        var dataStr_vec  = data.filepath.split('/');
                                        var b_eqPath = true;
                                        for(var pthIter = 1;pthIter<reqFileStr_vec.length;pthIter++)
                                        {
                                            if(pthIter<dataStr_vec.length)
                                            {
                                                if(reqFileStr_vec[pthIter].localeCompare(dataStr_vec[pthIter]))
                                                {
                                                    b_eqPath = false;
                                                    break;
                                                }
                                            }
                                        }
                                        if(b_eqPath)
                                        {
                                            console.log(data);
                                            var headerData_vec = [];
                                            if(isNewData(data_vec,data.filepath))
                                            {
                                                data_vec.push(data);
                                                dbo.collection("HeaderIncludeList",function(err,coll)
                                                {
                                                    var pth = data.filepath;
                                                    coll.find({CFile:pth},function(err,cursor)
                                                    {
                                                        if(cursor !== null)
                                                        {
                                                            var test = "test";
                                                            cursor.each(function(err,hddata)
                                                            { 
                                                                if(hddata!==null)
                                                                {                                                                        
                                                                    if(isNewHeaderData(headerData_vec,hddata))
                                                                    {
                                                                        headerData_vec.push(hddata);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if(headerData_vec.length>0)
                                                                    {
                                                                        socket.emit('headerTreedone',headerData_vec);  
                                                                        dbo.collection("initialDir",function(err,coll)
                                                                        {                                                                            
                                                                            coll.find({},function(err,cursor) 
                                                                            {
                                                                                var SWDir;
                                                                                cursor.each(function(err,ReqData)
                                                                                {
                                                                                    if(ReqData!==null)
                                                                                    {
                                                                                        SWDir = ReqData.SWDir
                                                                                        if(typeof SWDir !== 'undefined')
                                                                                        {          
                                                                                            var FCcopy = new copyFC(headerData_vec,SWDir);  
                                                                                            var ret = FCcopy.then(
                                                                                                function(path)
                                                                                                {
                                                                                                    var test = path;
                                                                                                    MakeList = new createCmake(headerData_vec,SWDir);
                                                                                                    headerData_vec = [];                                                                                              
                                                                                                    if(!data.filetype.localeCompare(".c"))
                                                                                                    {
                                                                                                        if(!data.isFile.localeCompare("true"))
                                                                                                        {
                                                                                                            dbo.collection("RequestedProcesses",function(err,coll)
                                                                                                            {
                                                                                                                coll.insertOne( { path:{ srcfile : data.filepath , dstfile : path }});                 
                                                                                                            });
                                                                                                        }
                                                                                                    }                                                                                            
                                                                                                    return SWDir;
                                                                                                }
                                                                                            );
                                                                                        }
                                                                                    }
                                                                                });
                                                                                
                                                                            });
                                                                        });
                                                                    }     
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        }                                        
                                    }
                                });
                            }
                        });                        
                    });        
                });                
            //    Cfile
            });
            socket.on('reqTaskList',function(dataReq)
            {
                var reqData = dataReq;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 
                dbo.collection("TaskList",function(err,coll)
                {
                   coll.find({},function(err,cursor)
                   {
                        var taskdata_vec = [];
                        if(cursor !== null)
                        {
                            cursor.each(function(err,taskdata)
                            { 
                                if(taskdata!==null)
                                {                                          
                                    taskdata_vec.push(taskdata);
                                }
                                else
                                {
                                    socket.emit('TaskListDone',taskdata_vec);  
                                }
                            }); 
                        }
                        else
                        {

                            }
                        });                        
                    });
                });
            });
            
            var MongoClient = require('mongodb').MongoClient;         
            // Establish connection to db        
            var url = "mongodb://localhost:27017/";
            MongoClient.connect(url, function(err, db) 
            {        
                if (err) throw err;                        
                var dbo = db.db("test");            
                dbo.collection("Directories",function(err,coll)
                {
                    if(coll !== null)
                    {
                        coll.find({},function(err,cursor)
                        {
                            var pathCollector = [];
                            if(cursor !== null)
                            {
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {                                       
                                        if(typeof data.filepath !== 'undefined')
                                        {     
                                            var path_vec = data.filepath.split('/');  
                                            var b_inc = false;   

                                            for(var pthIt=0;pthIt<path_vec.length;pthIt++)                                           
                                            {   
                                                if(!path_vec[pthIt].localeCompare('_gen'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('.gen'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('_bin'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('.bin'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('_log'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('.log'))
                                                {
                                                    b_inc = true;
                                                }
                                                if(!path_vec[pthIt].localeCompare('Conf'))
                                                {
                                                    b_inc = true;
                                                }
                                            }
                                            if(!(b_inc))
                                            {
                                                if(isNewData(pathCollector,data.filepath))
                                                {
                                                    pathCollector.push(data);
                                                }
                                            }
                                        }      
                                    }
                                    else
                                    {       
                                        socket.emit('recDirs',pathCollector);                     
                                        resolve(pathCollector);
                                        db.close();
                                    }
                                });
                            }
                        });
                    }                  
                });
                
              
                
            });
            

            socket.on('requestedProcess',function(dataReq)
            {
                var reqData = dataReq;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");            
                    dbo.collection("RequestedProcesses",function(err,coll)
                    {
                        if(typeof dataReq[0].ContainerProc !== 'undefined')
                        {
                            coll.insertOne( {process : dataReq[0].ContainerProc}  );                  
                        }                        
                    });                    
                });
                
            });

        });
    });
}