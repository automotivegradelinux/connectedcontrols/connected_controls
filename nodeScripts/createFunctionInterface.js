//=========================================================================================
//
//             createFunctionInterface.js : create stimultation and data aquisition 
//             interface for a C module according to the CLANG AST node analysis
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
module.exports = function (client) {

    return new Promise((resolve, reject) => 
    {     
        function getDeclarationString(data)
        {
            var declVec = [];
            for(var i=0;i<data.length;i++)
            {
                if(typeof data[i].data.decl.declExpr !== 'undefined')
                {
                    if(typeof data[i].data.decl.declExpr.respTypDef !== 'undefined')
                    {
                        if(typeof data[i].data.decl.declExpr.respTypDef.PtrName !== 'undefined')
                        {
                            var declString = data[i].data.decl.declExpr.respTypDef.PtrName + '   ' + data[i].data.decl.declExpr.name + ';';      
                        }
                        else
                        {

                        }
                    }
                    if(typeof data[i].data.decl.declExpr.TypeName)
                    {
                        var declString = data[i].data.decl.declExpr.TypeName  + '   ' + data[i].data.decl.declExpr.name + ';';   
                    }            
                }                
                declVec.push(declString); 
            }
            return declVec;
        }

        const readSTIM = require('./readSTIMData.js');
        readSTIMfunc = new readSTIM(client);
        readSTIMfunc
        .then(function(data)
        { 
            var STIMDecl_vec = getDeclarationString(data);
            console.log('---------------------------This is STIM data------------------------ \n'+STIMDecl_vec);     
        })
        .catch(function(err){ console.log('Decl data read error status: ',err);})

        const readDAQ = require('./readDAQData.js');
        readDAQfunc = new readDAQ(client);
        readDAQfunc
        .then(function(data)
        {
            var DAQDecl_vec = getDeclarationString(data);
            console.log('---------------------------This is DAQ data ------------------------\n'+DAQDecl_vec);     
        });


            //const lineReader = require('line-reader');
            //const path = require('path');

            //fs = require('fs');
            //fs.writeFile('helloworld.txt', 'Hello World!', function (err) {
            //  if (err) return console.log(err);
            //  console.log('Hello World > helloworld.txt');
            //});


        
    });
}