//=========================================================================================
//
//             deletePosgresData.js : provides a set of JS functions
//             needed to delete postgres tables
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//

/**
 * Delete method : delete data from postgres data table and send it to the web client using socket.io
 * @param {string} table postgres table 
 * @module h_deletePostgresData_server
 */
module.exports = function(table)
{
      /**
      * Pool of postgres clients object
      * @typedef {Object} Pool
      * @property {string} user - postgres user
      * @property {string} host - posgres host
      * @property {string} database - database name
      * @property {string} password - password
      * @property {number} port - port
      * @property {method} connect - connection method
      */

    const {Pool} = require('pg');

    const pool = new Pool({
        user: 'ThermBaseAlgebraic',
        host: 'db',
        database: 'ThermoBaseDataStationary',
        password: 'thermo',
        port: 5432
    });

      /**
      * @method pool_connect_then
      * @description The delete method addresses a table and deletes it directly. 
      * <li>qrstr ='DELETE FROM ' + 'public.' + '"'+table+'"' => SQL command</li>
      * <br>
      * There is only one call argument which is 
      * <li>table</li></ol>
      * <br>      
      */

    pool.connect().then(client => {
      var qrstr ='DELETE FROM ' + 'public.' + '"'+table+'"';
        return client
          .query(qrstr)
          .then(res => {
            console.log(res);
            client.release()            
          })
          .catch(err => {
            client.release()
            console.log(err.stack)
          })
      });
}