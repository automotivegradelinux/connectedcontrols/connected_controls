
//=========================================================================================
//
//             getASAMMDXxml.js : reads all XML files of the project as JSON
//             and stores the JSON converted files to postgres in JSONB format
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
function insert(client,socket,table,column,obj,eventName,no)
{
  var jsonData = JSON.stringify(obj);
              
  var qrstring = "INSERT INTO " + "public." + table+ " (" +'"'+ column+'"' + ") "+" VALUES('"+ jsonData + "');";             
  client.query(qrstring).then(async res=>
  {
    await client.release(true);    
    socket.emit(eventName,no);        
  })
 .catch( err => {
   console.log('error',err);   
  }
   
 );
}

/**
 * Getter method : read data from postgres data table and send it to the web client using socket.io
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {string} request name of the socket event
 * @module f_getPostgresData_server
 */
 module.exports = function(socket)
 {

     
       /**
       * Pool of postgres clients object
       * @typedef {Object} Pool
       * @property {string} user - postgres user
       * @property {string} host - posgres host
       * @property {string} database - database name
       * @property {string} password - password
       * @property {number} port - port
       * @property {method} connect - connection method
       */
 
       /**
       * @method pool_connect_then
       * @description The getter method uses a socket.io event to send data to a web client which is
       * read from a postgres table using a query. The arguments are : 
       * <ol><li>request => socket.io event name of the data which is sent to the web client</li>
       * <li>qrstr ='SELECT * FROM ' + 'public.' + '"'+table+'"' => SQL command</li></ol>
       * <br>
       * The "Select" query reads a complete table. There are 2 different socket.io send events :
       * <ol><li>'sendTable'</li>
       * <li>'sendCurve'</li></ol>
       * <br>
       * The request argument has to be equal to one of the below strings to create a reaction. 
       * Curves are collected in a collector array before they are sent to the client. 
       * Maps are sent directly to the client without any further conversions. 
       */
        
     
      socket.on('getTableLength',function(cnt)
      {
          const { Pool } = require('pg');
          const pool = new Pool(
              {
                user: 'ThermBaseAlgebraic',
                host: 'db',
                database: 'ThermoBaseDataStationary',
                password: 'thermo',
                port: 5432
              });

          pool.connect().then(client => 
          {
            var qrstr = " SELECT id FROM public.pavastfiles";
            return client
                    .query(qrstr)
                    .then(res => 
                    {
                      let obj = {};
                      obj.cnt = 0;
                      obj.rows = res.rows;
                      client.release();
                      socket.emit('sendTableLength',obj)
                    });

          });
      });

      socket.on('getMdxDataSize',function()
      {
          const { Pool } = require('pg');
          const pool = new Pool({
                    user: 'ThermBaseAlgebraic',
                    host: 'db',
                    database: 'ThermoBaseDataStationary',
                    password: 'thermo',
                    port: 5432
                });          
          pool.connect().then(client => 
            {
              var qrstr = "SELECT count(*) AS exact_count FROM public.mdxreport";                                             
              return client.query(qrstr)
                      .then(async res => 
                      {
                        client.release()                                          
                        socket.emit('sendMdxDataSize',res.rows[0]);
                      });                              
              });
      });

      socket.on('deletePrevTable',function(cnt)
      {
        const deletePg = require("./deletePosgresData.js");
        deletePg("mdxreport");
      });
        
      socket.on('getNextASAMMdxFile',function(cnt)
      {                       
              const { Pool } = require('pg');
              const pool = new Pool({
                user: 'ThermBaseAlgebraic',
                host: 'db',
                database: 'ThermoBaseDataStationary',
                password: 'thermo',
                port: 5432
              }); 
                pool.connect().then(client => 
                {
                  var qrstr = "SELECT pvstsrcs->>'filepath' FROM public.pavastfiles WHERE id="+cnt+";"
                    return client
                      .query(qrstr)
                      .then(async res => {                        
                          var cur = res.rows[0]["?column?"];                                
                          console.log(__dirname);
                          var fs = require('fs');
                          if (fs.existsSync(cur)) 
                          {
                                //file exists                  
                                var data = fs.readFileSync(cur, 'utf8');
                                var xml2js = require('xml2js');
                                var parser = new xml2js.Parser();                                                  
                                parser.parseStringPromise(data).then(
                                function(result) 
                                { 
                                  console.log(result);                                            
                                  insert(client,socket,'mdxreport','report',result,'ASAMDataReady',cnt);                                        
                                });                                   
                          }                 
                        }).catch(err => {             
                          console.log(err.stack);
                          client.release(true);
                    });              
                  }).catch(err => {             
                      console.log(err.stack);                      
                });
       });
 }