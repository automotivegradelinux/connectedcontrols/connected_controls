//=========================================================================================
//
//             appendJSONBArray.js : adds further entries to an existing JSONB array 
//             in postgres. 
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Update method : update existing postgres data
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {object} data data content in JS object format
 * @module j_updatePostgresData_server
 */
 module.exports = function(table,column,element,data)
 {
      /**
       * Pool of postgres clients object
       * @typedef {Object} Pool
       * @property {string} user - postgres user
       * @property {string} host - posgres host
       * @property {string} database - database name
       * @property {string} password - password
       * @property {number} port - port
       * @property {method} connect - connection method
       */
       const {Pool} = require('pg');
 
       const pool = new Pool({
         user: 'ThermBaseAlgebraic',
         host: 'db',
         database: 'ThermoBaseDataStationary',
         password: 'thermo',
         port: 5432
     });
   
     
   
     pool.connect().then(client => {
         //var jsonData = JSON.stringify(selectedDir);
         
         // var column = "toolsctrl";
         //  element = "selecteddir";
         //  table = "initialdir";            
         var qrstring = "UPDATE public."+ table + " SET " + column +"=jsonb_set("+ column +"::jsonb,array['" + element+"'], (cfilestoparse->'"+element+"')::jsonb || '["+data+"]'::jsonb);";
   
         return client
           .query(qrstring)
           .then(res => {
             client.release();
             console.log(res);
           })
           .catch(err => {
             client.release();
             console.log(err.stack);
           })
       });
   
 }
 
 