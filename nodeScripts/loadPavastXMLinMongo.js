//=========================================================================================
//
//             loadPavastXMLinMongo.js : load MSR meta data from MongoDB
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
function getObjectRecursively(curObj,level)
{   
    if(Array.isArray(curObj))
    {
        console.log('Array',curObj,curObj.length);
    }
    var key_vec = Object.keys(curObj);
    var copyObj={};
    for(var i=0;i<key_vec.length;i++)
    {
        
                                    
        var nestedObj = curObj[key_vec[i]];
        
        if(typeof nestedObj === "object" )
        {
            level++;
            getObjectRecursively(nestedObj,level);
            level--; 
        }

        if(!key_vec[i].localeCompare("$"))
        {
            copyObj.identifier = curObj[key_vec[i]];
            console.log('$ Vec : ',key_vec[i]); 
        }
        if(!key_vec[i].localeCompare("_"))
        {
            console.log('_ Vec : ',key_vec[i]); 
        } 
        
    }
}

function storeToMongo(db,collectionName,curObj,no)
{       
        level = 0;
 
        db.collection(collectionName).insertOne(curObj, function(err, res) {
            if (err) {console.log('call no: ',no,'obj: ',curObj); throw err; }  
            
        });     

      //  async function getAThing() {
      //      let db = await mongodb.MongoClient.connect('mongodb://server/mydatabase');
      //      if (await db.authenticate("myuser", "mypassword")) {
      //          let thing = await db.collection("Things").findOne({ name: "bob" });
      //          await db.close();
      //          return thing;
      //      }
      //  }


}
function readScalarOrVectorZero(data)
{
    var result;
    if(typeof data !== 'undefined' && Array.isArray(data) && data.length===1)    result = data[0];
    else   result = data;

   
    return result;
}
function createHeader(result,filepath)
{  
        var collector = {};    
        collector.FILEPATH = filepath;
        var json = JSON.stringify(result);  
        jsonCor = json.replace(/\$/g,"ident");               
        var jsonObj = JSON.parse(jsonCor);     
        var MsrSWObj=jsonObj.MSRSW;    
        var modulename = readScalarOrVectorZero(MsrSWObj['SHORT-NAME']);
        if(typeof modulename !== 'undefined')
        {
            collector.MODULENAME = modulename;
        }                 
        var swsystems = readScalarOrVectorZero(MsrSWObj['SW-SYSTEMS']);  
        if(typeof swsystems !== 'undefined')
        {
            collector.SWSYSTEMS = {};
            var swsystem = readScalarOrVectorZero(swsystems['SW-SYSTEM']);
            if(typeof swsystem !== 'undefined')
            {
                    collector.SWSYSTEMS.SWSYSTEM = {};
                    longname =  readScalarOrVectorZero(swsystem['LONG-NAME']);
                    if(typeof longname !== 'undefined')
                    {
                        collector.SWSYSTEMS.SWSYSTEM.LONGNAME = longname;
                    }                        
                    shortname = readScalarOrVectorZero(swsystem['SHORT-NAME']);
                    if(typeof longname !== 'undefined')
                    {
                            collector.SWSYSTEMS.SWSYSTEM.SHORTNAME = shortname;
                    }                                                                        
                    var SwDataDict =  readScalarOrVectorZero(swsystem['SW-DATA-DICTIONARY-SPEC']);     
                    if(typeof SwDataDict !== 'undefined')
                    {
                        collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC = {};
                            //var SwDataDict0 = readScalarOrVectorZero(SwDataDict); 
                            var CalParms =  readScalarOrVectorZero(SwDataDict['SW-CALPRMS']);
                            //var CalParms0 = readScalarOrVectorZero(CalParms);
                            if(typeof CalParms !== 'undefined')
                            {
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS = CalParms;            
                            }
                            var SwVars =  readScalarOrVectorZero(SwDataDict['SW-VARIABLES']);
                            if(typeof SwVars !== 'undefined')
                            {    
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES = {}; 
                                var SwVar = readScalarOrVectorZero(SwVars['SW-VARIABLE']);                                
                                if(typeof SwVar !== 'undefined')
                                {
                                    collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE = SwVar;
                                }                                                                
                            }                            
                            var Sysconsts =  readScalarOrVectorZero(SwDataDict['SW-SYSTEMCONSTS']);
                            if(typeof Sysconsts !== 'undefined')
                            {
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS = Sysconsts;
                            }
                            var SWUnits =  readScalarOrVectorZero(SwDataDict['SW-UNITS']); 
                            if(typeof SWUnits !== 'undefined')
                            {                                
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS = SWUnits;
                            }                            
                            var SWCompuMethods =  readScalarOrVectorZero(SwDataDict['SW-COMPU-METHODS']); 
                            if(typeof SWCompuMethods !== 'undefined')
                            {                                
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS = SWCompuMethods;
                            }
                            var SWDataConstrs =  readScalarOrVectorZero(SwDataDict['SW-DATA-CONSTRS']); 
                            if(typeof SWDataConstrs !== 'undefined')
                            {
                                collector.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS = SWDataConstrs;
                            } 
                             

                          
                    }
                    var SwInstance =  readScalarOrVectorZero(swsystem['SW-INSTANCE-SPEC']); 
                    if(typeof SwInstance !== 'undefined')
                    {
                        collector.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC = SwInstance;
                    }   
                    var SwComponentSpec =  readScalarOrVectorZero(swsystem['SW-COMPONENT-SPEC']); 
                    if(typeof SwComponentSpec !== 'undefined')
                    {
                        collector.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC = {};
                        var SwComponents =  readScalarOrVectorZero(SwComponentSpec['SW-COMPONENTS']);
                        if(typeof SwComponents !== 'undefined')
                        {
                            collector.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS = {};
                            var SwFeature = readScalarOrVectorZero(SwComponents['SW-FEATURE']);
                            if(typeof SwFeature !== 'undefined')
                            {
                                collector.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS.SWFEATURE = SwFeature;
                            }
                            var test = "test";
                        }
                        
                    }   
                    var SwSched =  readScalarOrVectorZero(swsystem['SW-SCHEDULING-SPEC']); 
                    if(typeof SwSched !== 'undefined')
                    {
                        collector.SWSYSTEMS.SWSYSTEM.SWSCHEDULINGSPEC = SwSched;
                    }
                                                          
            }              
            return collector;
        }  
}
function storeVariableObject(parentObj,dbo)
{

    if(typeof parentObj !== 'undefined')
    {        
        var variables
        if(typeof parentObj.SWSYSTEMS != 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM != 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC != 'undefined')
                {
                    variables = parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES;
                }
            }
        }
        
        if(typeof variables !== 'undefined' )
        {
            variables = parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES;
            delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES;
            parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES = {};
            if(typeof variables.SWVARIABLE !== 'undefined')
            {
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE = [];
                lastChild = variables.SWVARIABLE;
                var SwVarlen = lastChild.length;
        
                for(var i=0;i<SwVarlen;i++)
                {
                    var collVar = {};
                    var varCur = lastChild[i];
                    if(typeof varCur !== 'undefined')
                    {
                        var category= readScalarOrVectorZero(varCur['CATEGORY']);
                        if(typeof category !== 'undefined')
                        {
                            collVar.CATEGORY = category;
                        }
                        var description= readScalarOrVectorZero(varCur['DESC']);
                        if(typeof description !== 'undefined')
                        {
                            collVar.DESC = description;
                        }
                        var shortname= readScalarOrVectorZero(varCur['SHORT-NAME']);
                        if(typeof shortname !== 'undefined')
                        {
                            collVar.SHORTNAME = shortname;
                        }
                        var swDataDefProps= readScalarOrVectorZero(varCur['SW-DATA-DEF-PROPS']);                
                        if(typeof swDataDefProps !== 'undefined')
                        {                                                             
                            var AddrMethRef=readScalarOrVectorZero(swDataDefProps['SW-ADDR-METHOD-REF']);  
                            if(typeof AddrMethRef !== 'undefined')
                            {
                                collVar.SWADDRMETHODREF = AddrMethRef;
                            }                        
                            var  BaseTypeRef=readScalarOrVectorZero(swDataDefProps['SW-BASE-TYPE-REF']);  
                            if(typeof BaseTypeRef !== 'undefined')
                            {
                                collVar.SWBASETYPEREF = BaseTypeRef;
                            }
                            var VarCodeSyntax=readScalarOrVectorZero(swDataDefProps['SW-CODE-SYNTAX-REF']);   
                            if(typeof VarCodeSyntax !== 'undefined')
                            {
                                collVar.SWCODESYNTAXREF = VarCodeSyntax;
                            }
                            var VarimplPolicy= readScalarOrVectorZero(swDataDefProps['SW-IMPL-POLICY']);
                            if(typeof VarimplPolicy !== 'undefined')
                            {
                                collVar.SWIMPLPOLICY = VarimplPolicy;
                            }
                            var CompuMethod = readScalarOrVectorZero(swDataDefProps['SW-COMPU-METHOD-REF']);
                            if(typeof CompuMethod !== 'undefined')
                            {
                                collVar.SWCOMPUMETHODREF = CompuMethod;
                            }
                            var AccessImplPolicy  = swDataDefProps['SW-VARIABLE-ACCESS-IMPL-POLICY'];
                            if(typeof AccessImplPolicy !== 'undefined')
                            {
                                collVar.SWVARIABLEACCESSIMPLPOLICY = AccessImplPolicy;
                            }                            
                        }
                                    
                    } 
                    parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE.push(collVar);               
                }
                //storeToMongo(dbo,"VariableSpecification",parentObj,0); 
            }
        }  
    }   
}
function storeCalParamsObject(parentObj,dbo)
{   
    var CalParms;
    if(typeof parentObj != 'undefined')
    {
        if(typeof parentObj.SWSYSTEMS != 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM != 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC != 'undefined')
                {
                    CalParms =  parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS;    
                }
            }
        }
    }    
    //var CalParms0 = CalParms[0];
    if(typeof CalParms !== 'undefined')
    {
        delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS;
        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS = {};

        var CalParm = CalParms['SW-CALPRM']; 
        if(typeof CalParm !== 'undefined')
        {
            parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS.SWCALPRM=[];
            var CalParlen = CalParm.length;
            for(var i=0;i<CalParlen;i++)
            {    
                innerObj = {};  
               
                var CalCur = CalParm[i];
                var CalPrmName = readScalarOrVectorZero(CalCur['SHORT-NAME']);
                if(typeof CalPrmName !== 'undefined')
                {
                    innerObj.SHORTNAME = CalPrmName;
                }                                                                                                                        
                var CalPrmDesc = readScalarOrVectorZero(CalCur['DESC']);
                if(typeof CalPrmDesc !== 'undefined')
                {
                    innerObj.DESC = CalPrmDesc;
                }                                                                                                                                       

                var CalPrmCategory = readScalarOrVectorZero(CalCur['CATEGORY']);
                if(typeof CalPrmCategory !== 'undefined')
                {
                    innerObj.CATEGORY = CalPrmCategory;
                }                                                                                                                                       
                   
                var CalPrmDataDef = readScalarOrVectorZero(CalCur['SW-DATA-DEF-PROPS']);            
                if(typeof CalPrmDataDef !== 'undefined')
                {
                    innerObj.SWDATADEFPROPS = {};
                    var addrMeth = readScalarOrVectorZero(CalPrmDataDef['SW-ADDR-METHOD-REF']);
                    if(typeof addrMeth !== 'undefined')
                    {                                        
                        innerObj.SWDATADEFPROPS.SWADDRMETHODREF = addrMeth;
                    }
                    var basetype= readScalarOrVectorZero(CalPrmDataDef['SW-BASE-TYPE-REF']);                
                    if(typeof basetype !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWBASETYPEREF = basetype;
                    }               
                    var SWcalAcc   = readScalarOrVectorZero(CalPrmDataDef['SW-CALIBRATION-ACCESS']);
                    if(typeof SWcalAcc !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWCALIBRATIONACCESS = SWcalAcc;
                    }            
                    var CodeSynRef = readScalarOrVectorZero(CalPrmDataDef['SW-CODE-SYNTAX-REF']);
                    if(typeof CodeSynRef !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWCODESYNTAXREF = CodeSynRef;
                    }            
                    var CompuMethRef  = readScalarOrVectorZero(CalPrmDataDef['SW-COMPU-METHOD-REF']);                        
                    if(typeof CompuMethRef !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWCOMPUMETHODREF = CompuMethRef;
                    }            
                    var swDataConstrRef       = readScalarOrVectorZero(CalPrmDataDef['SW-DATA-CONSTR-REF']);
                    if(typeof swDataConstrRef !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWDATACONSTRREF = swDataConstrRef;
                    }           
                    var swRecordLayoutRef     = readScalarOrVectorZero(CalPrmDataDef['SW-RECORD-LAYOUT-REF']);            
                    if(typeof swRecordLayoutRef !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWRECORDLAYOUTREF = swRecordLayoutRef;
                    }
                    

                    var AxisSet = readScalarOrVectorZero(CalPrmDataDef['SW-CALPRM-AXIS-SET']);              
                    if(typeof AxisSet!='undefined')
                    {    
                        innerObj.SWDATADEFPROPS.SWCALPRMAXISSET = {};                                           
                        var axisSetSg = AxisSet['SW-CALPRM-AXIS'];
                        if(typeof axisSetSg!='undefined')
                        {
                            innerObj.SWDATADEFPROPS.SWCALPRMAXISSET.SWCALPRMAXIS = [];
                            var axisSetlen = axisSetSg.length;                        
                            var collAr = [];
                            for(var j=0;j<axisSetlen;j++)
                            {                    
                                var collParamAxis = {};
                                
                                var CurAxisPar = axisSetSg[j];                                                
                                var  SWAXISINDEX = readScalarOrVectorZero(CurAxisPar['SW-AXIS-INDEX']);
                                if(typeof SWAXISINDEX !== 'undefined')
                                {
                                    collParamAxis.SWAXISINDEX = SWAXISINDEX;
                                }    

                                var ax =  readScalarOrVectorZero(CurAxisPar['SW-AXIS-INDIVIDUAL']);                    
                                
                                if(typeof ax !== 'undefined')  
                                {                  
                                    collParamAxis.SWAXISINDIVIDUAL = {};
                                    var refVars = readScalarOrVectorZero(ax['SW-VARIABLE-REFS']);
                                    if(typeof refVars!='undefined')
                                    {
                                        collParamAxis.SWAXISINDIVIDUAL.SWVARIABLEREFS = {};
                                        var refVar = refVars['SW-VARIABLE-REF'];
                                        var refs_vec = [];
                                        if(typeof refVar !== 'undefined')
                                        {
                                            collParamAxis.SWAXISINDIVIDUAL.SWVARIABLEREFS.SWVARIABLEREF=[];
                                            for(var vrefIt=0;vrefIt<refVar.length;vrefIt++)
                                            {
                                                var cur = readScalarOrVectorZero(refVar[vrefIt]);
                                                if(typeof cur !== 'undefined')
                                                {
                                                    collParamAxis.SWAXISINDIVIDUAL.SWVARIABLEREFS.SWVARIABLEREF.push(cur);
                                                }                                                
                                            }                                         
                                        }
                                    }
                                    var iniDats = readScalarOrVectorZero(ax['SW-MAX-AXIS-POINTS']);
                                    if(typeof iniDats !== 'undefined')
                                    {
                                        collParamAxis.SWAXISINDIVIDUAL.SWMAXAXISPOINTS = iniDats;
                                    }
                                    var constr = readScalarOrVectorZero(ax['SW-DATA-CONSTR-REF']);
                                    if(typeof constr !== 'undefined')
                                    {
                                        collParamAxis.SWAXISINDIVIDUAL.SWDATACONSTRREF = constr;
                                    }

                                }                                                                                                                                    
                                innerObj.SWDATADEFPROPS.SWCALPRMAXISSET.SWCALPRMAXIS.push(collParamAxis);
                            }                      
                        }                       
                    }
                }
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS.SWCALPRM.push(innerObj);            
                
            } 
            //storeToMongo(dbo,"ParameterCollection",parentObj,1); 
    }
    }

}
function storeCalInstanceObject(parentObj,dbo)
{    
    var SwInstSpec;
    if(typeof parentObj !== 'undefined')
    {
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
        if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
        {
            SwInstSpec = parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC;
            delete parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC;
            parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC = {};
        }
        }
    }
        
    if(typeof SwInstSpec !== 'undefined')
    {        
        var SwInsttree = readScalarOrVectorZero(SwInstSpec['SW-INSTANCE-TREE']);
        
        if(typeof SwInsttree !== 'undefined')
        {
            parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE = {}
            var treeCont = SwInsttree['SW-INSTANCE'];    
            if(typeof treeCont !== 'undefined')
            {
                parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE.SWINSTANCE = [];
                for(var i=0;i<treeCont.length;i++)
                {
                    var collSWSpecAr = {};        
                    var content = treeCont[i];
                    var shortname = readScalarOrVectorZero(content['SHORT-NAME']);
                    if(shortname !== 'undefined')
                    {                                        
                        collSWSpecAr.SHORTNAME = shortname;
                    }
                    var category = readScalarOrVectorZero(content['CATEGORY']);
                    if(category !== 'undefined')
                    {                                        
                        collSWSpecAr.CATEGORY = category;
                    }
                    var SWfeat = readScalarOrVectorZero(content['SW-FEATURE-REF']);
                    if(SWfeat !== 'undefined')
                    {                                        
                        collSWSpecAr.SWFEATUREREF = SWfeat;
                    }                
                    var variant = readScalarOrVectorZero(content['SW-INSTANCE-PROPS-VARIANTS']);
                    if(variant !== 'undefined')
                    {                                        
                        collSWSpecAr.SWINSTANCEPROPSVARIANTS = {};
                    }                                
                    if(typeof variant != 'undefined')
                    {
                        var varInst = readScalarOrVectorZero(variant['SW-INSTANCE-PROPS-VARIANT']);
                        if(typeof varInst !== 'undefined')
                        {                                  
                            collSWSpecAr.SWINSTANCEPROPSVARIANTS.SWINSTANCEPROPSVARIANT = {};                                     
                            var axConts = readScalarOrVectorZero(varInst['SW-AXIS-CONTS']); 
                            if(typeof axConts !== 'undefined')
                            {               
                                collSWSpecAr.SWINSTANCEPROPSVARIANTS.SWINSTANCEPROPSVARIANT.SWAXISCONTS = {};
                                            
                                var axCont = readScalarOrVectorZero(axConts['SW-AXIS-CONT']); 
                                if(typeof axCont !== 'undefined')
                                {
                                    collSWSpecAr.SWINSTANCEPROPSVARIANTS.SWINSTANCEPROPSVARIANT.SWAXISCONTS.SWAXISCONT = [];
                                    for(var axiter=0;axiter<axCont.length;axiter++)
                                    {
                                        var collAxisAr = {};
                                        var currentVal = axCont[axiter];
                                        var valPhys = readScalarOrVectorZero(currentVal['SW-VALUES-PHYS']);
                                        if(typeof valPhys !== 'undefined')  
                                        {
                                            collAxisAr.SWVALUESPHYS = {};
                                            var val = readScalarOrVectorZero(valPhys['V']);
                                            if(typeof val !== 'undefined')  
                                            {
                                                collAxisAr.SWVALUESPHYS.V = val;                                                
                                            }
                                            var valF = readScalarOrVectorZero(valPhys['VF']);
                                            if(typeof valF !== 'undefined')  
                                            {
                                                collAxisAr.SWVALUESPHYS.VF = valF;                                                
                                            }
                                            var valGrp = readScalarOrVectorZero(valPhys['VG']);
                                            if(typeof valGrp !== 'undefined')  
                                            {
                                                collAxisAr.SWVALUESPHYS.VG = [];
                                                for(var GrpIter=0;GrpIter< valGrp.length;GrpIter++)
                                                {
                                                    var innerObj = {};
                                                    var curGrpVal = valGrp[GrpIter];

                                                    innerObj.V = readScalarOrVectorZero(curGrpVal['V']);
                                                    innerObj.LABEL = readScalarOrVectorZero(curGrpVal['LABEL']);                                                    
                                                    collAxisAr.SWVALUESPHYS.VG.push(innerObj);
                                                }
                                            }
                                        }                      
                                        var unitRef = readScalarOrVectorZero(currentVal['SW-UNIT-REF']);
                                        if(typeof valPhys !== 'undefined')  
                                        {
                                            collAxisAr.SWUNITREF = unitRef;
                                        }                      
                                        collSWSpecAr.SWINSTANCEPROPSVARIANTS.SWINSTANCEPROPSVARIANT.SWAXISCONTS.SWAXISCONT.push(collAxisAr);                                                                                                                         
                                    }                
                                }
                            }
                        }            
                    
                    }  
                    parentObj.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE.SWINSTANCE.push(collSWSpecAr);                                            
                }
                //storeToMongo(dbo,"InstanceCollection",parentObj,2);
            }
        }
        }
          
    
        
    //storeToMongo(dbo,"InstanceCollection",collSWSpec);   
}
function storeSysConstObject(parentObj,dbo)
{
    var dataObj = {};
    var Sysconst;
    if(typeof parentObj !== 'undefined')
    {
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC !== 'undefined')
                {
                    if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS !== 'undefined')
                    {
                        Sysconst = parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS;
                        delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS;
                        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS = {};
                    }
                }
            }
        } 
    }       
    if(typeof Sysconst !== 'undefined')
    {
        var Sysconst_vec = readScalarOrVectorZero(Sysconst['SW-SYSTEMCONST']);                        
        if(typeof Sysconst_vec !== 'undefined')  
        {     
            
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST = [];
            for(var sysIter=0;sysIter<Sysconst_vec.length;sysIter++)
            {
                var innerObj = {};
                var Syscon = Sysconst_vec[sysIter];
                var SysLongName = readScalarOrVectorZero(Syscon['LONG-NAME']);
                if(typeof SysLongName !== 'undefined' )
                {
                    innerObj.LONGNAME = SysLongName;
                }
                var SysShortName = readScalarOrVectorZero(Syscon['SHORT-NAME']);                
                if(typeof SysShortName !== 'undefined')
                {
                    innerObj.SHORTNAME = SysShortName;
                }
                var category = readScalarOrVectorZero(Syscon['CATEGORY']);                
                if(typeof category !== 'undefined')
                {
                    innerObj.CATEGORY = category;
                }

                var PhysValue = readScalarOrVectorZero(Syscon['SW-VALUES-PHYS']);
                if(typeof PhysValue !== 'undefined')
                {      
                    
                    innerObj.SWVALUESPHYS = [];
                    var VF =readScalarOrVectorZero(PhysValue['VF']); 
                    if(typeof VF !== 'undefined')
                    {
                        innerObj.SWVALUESPHYS = VF;
                    }

                }

                var CodedValue = readScalarOrVectorZero(Syscon['SW-VALUES-CODED']);
                if(typeof CodedValue !== 'undefined')
                {      
                    
                    innerObj.SWVALUESCODED = [];
                    var VF =readScalarOrVectorZero(CodedValue['VF']); 
                    if(typeof VF !== 'undefined')
                    {
                        innerObj.SWVALUESCODED = VF;
                    }

                }
                
                var SWDataDefProps = Syscon['SW-DATA-DEF-PROPS'];
                if(typeof SWDataDefProps !== 'undefined')
                {
                    innerObj.SWDATADEFPROPS={};
                    var basetyperef = readScalarOrVectorZero(readScalarOrVectorZero(SWDataDefProps)['SW-BASE-TYPE-REF']);
                    if(typeof basetyperef !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWBASETYPEREF = basetyperef;
                    }
                    var compumethodref = readScalarOrVectorZero(readScalarOrVectorZero(SWDataDefProps)['SW-COMPU-METHOD-REF']);
                    if(typeof compumethodref !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWCOMPUMETHODREF = compumethodref;
                    }
                    var swcal = readScalarOrVectorZero(readScalarOrVectorZero(SWDataDefProps)['SW-CALIBRATION-ACCESS']);
                    if(typeof swcal !== 'undefined')
                    {
                        innerObj.SWDATADEFPROPS.SWCALIBRATIONACCESS = swcal;
                    }              
                } 
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST.push(innerObj);                               
            }
        //storeToMongo(dbo,"SysConstColl",dataObj,3);
        }
    }    
}

function storeSWUnitObject(parentObj,dbo)
{
    var SWUnits;
    if(typeof parentObj !== 'undefined')
    {
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC !== 'undefined')
                {
                    if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS !== 'undefined')
                    {
                        SWUnits = parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS;
                        delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS;
                        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS = {};
                    }
                }
            }
        }
    }

    if(typeof SWUnits !== 'undefined')
    {                
        var su = readScalarOrVectorZero(SWUnits['SW-UNIT']);                                
        if(typeof su !== 'undefined')
        {         
            parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT = []; 
            var len;
            if(typeof su.length === 'undefined') 
            {
                var dataObj = {};                                         
                var sn = readScalarOrVectorZero(su['SHORT-NAME']);
                if(typeof sn !== 'undefined')
                {                            
                    dataObj.SHORTNAME = sn;
                }
                var ud = readScalarOrVectorZero(su['SW-UNIT-DISPLAY']);
                if(typeof ud !== 'undefined')
                {
                    dataObj.SWUNITDISPLAY = ud;
                }                        
                var SiUnit = readScalarOrVectorZero(su['SI-UNIT']);
                if(typeof SiUnit !== 'undefined')
                {
                    dataObj.SIUNIT = SiUnit;                
                } 
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.push(dataObj);                                               
                
            }         
            else
            {
                for(var i=0;i<su.length;i++)
                {       
                    var dataObj = {};                                         
                    var sn = readScalarOrVectorZero(su[i]['SHORT-NAME']);
                    if(typeof sn !== 'undefined')
                    {                            
                        dataObj.SHORTNAME = sn;
                    }
                    var ud = readScalarOrVectorZero(su[i]['SW-UNIT-DISPLAY']);
                    if(typeof ud !== 'undefined')
                    {
                        dataObj.SWUNITDISPLAY = ud;
                    }                        
                    var SiUnit = readScalarOrVectorZero(su[i]['SI-UNIT']);
                    if(typeof SiUnit !== 'undefined')
                    {
                        dataObj.SIUNIT = SiUnit;                
                    } 
                    parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.push(dataObj);                                               
            }
            }
            //storeToMongo(dbo,"SWUnitColl",parentObj,4);            
        }
    }                   
}

function storeCompuMethodObject(parentObj,dbo)
{
    var dataObj = {};
    var scms;
    if(typeof parentObj !== 'undefined')
    {   
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC !== 'undefined')
                {
                    if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS !== 'undefined')
                    {
                        scms = parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS;
                        delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS;
                        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS = {};
                    }
                }
            }
        }
    }
    if(typeof  scms !== 'undefined')
    {
            var scm = readScalarOrVectorZero(scms['SW-COMPU-METHOD']);
                                
            if(typeof scm !== 'undefined')
            {                    
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD = [];
                for(var i=0;i<scm.length;i++)
                {       
                    
                    var dataObj = {};                 
                    var curEl = scm[i];
                    dataObj.CompMeth = 'SW-COMPU-METHOD';
                    var sn = readScalarOrVectorZero(curEl['SHORT-NAME']);                    
                    if(typeof sn !== 'undefined')
                    {                            
                        dataObj.SHORTNAME = sn;
                    }
                    var ctg = readScalarOrVectorZero(curEl['CATEGORY']);
                    if(typeof ctg !== 'undefined')
                    {
                        dataObj.CATEGORY = ctg;
                    }                        
                    var SUref = readScalarOrVectorZero(curEl['SW-UNIT-REF']);
                    if(typeof SUref !== 'undefined')
                    {
                        dataObj.SWUNITREF = SUref;                
                    }                        
                    var SWPhysToIntern = readScalarOrVectorZero(curEl['SW-COMPU-PHYS-TO-INTERNAL']);
                    if(typeof SWPhysToIntern !== 'undefined')
                    {                    
                        dataObj.SWCOMPUPHYSTOINTERNAL={};                            
                        var scales = readScalarOrVectorZero(SWPhysToIntern['SW-COMPU-SCALES']);                        
                        if(typeof scales !== 'undefined')
                        {
                            dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES={};                            
                            var scale = readScalarOrVectorZero(scales['SW-COMPU-SCALE']);
                            if(typeof scale !== 'undefined')
                            {
                                dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE={};                                                     
                                var RatCoeff = readScalarOrVectorZero(scale['SW-COMPU-RATIONAL-COEFFS']);
                                if(typeof RatCoeff !== 'undefined')
                                {
                                    dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS={};                        
                                    var Numerator = readScalarOrVectorZero(RatCoeff['SW-COMPU-NUMERATOR']);
                                    if(typeof Numerator !== 'undefined')
                                    {    
                                        dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUNUMERATOR={}; 
                                        dataObj.NumeratorType = 'VF';                                   
                                        var vf = Numerator['VF'];
                                        
                                        if(typeof vf !== 'undefined')
                                        {   
                                            dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUNUMERATOR.VF = vf;
                                        }  
                                        else
                                        {                                       
                                            var v = Numerator['V'];
                                            if(typeof v !== 'undefined')
                                            {
                                                dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUNUMERATOR.V = v;                                                
                                            }                                            
                                        }                                      
                                    }
                                    var DeNominator = readScalarOrVectorZero(RatCoeff['SW-COMPU-DENOMINATOR']);
                                    if(typeof DeNominator !== 'undefined')
                                    {
                                        dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUDENOMINATOR={};
                                        var vf = DeNominator['VF'];
                                        if(typeof vf !== 'undefined')
                                        {
                                            dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUDENOMINATOR.VF = vf;

                                        }
                                        else
                                        {
                                            var v = Numerator['V'];
                                            if(typeof v !== 'undefined')
                                            {
                                                dataObj.SWCOMPUPHYSTOINTERNAL.SWCOMPUSCALES.SWCOMPUSCALE.SWCOMPURATIONALCOEFFS.SWCOMPUDENOMINATOR.V = v;                                                
                                            }                                            
                                            
                                        }                                                                              
                                        
                                    }
                                    
                                }                            
                            }
                            
                        }

                    }  
                    else
                    {                        
                        var SWInternToPhys = readScalarOrVectorZero(scm[i]['SW-COMPU-INTERNAL-TO-PHYS']);
                        if(typeof SWInternToPhys !== 'undefined')
                        {
                            dataObj.SWCOMPUINTERNALTOPHYS={}; 
                            var CmpScls = readScalarOrVectorZero(SWInternToPhys['SW-COMPU-SCALES']);
                            var Default = readScalarOrVectorZero(SWInternToPhys['SW-COMPU-DEFAULT-VALUE']);
                            if(typeof CmpScls !== 'undefined')
                            {         
                                dataObj.SWCOMPUINTERNALTOPHYS.SWCOMPUSCALES={};
                                CmpScl = readScalarOrVectorZero(CmpScls['SW-COMPU-SCALE']);
                                if(typeof CmpScl !== 'undefined')
                                {
                                    dataObj.SWCOMPUINTERNALTOPHYS.SWCOMPUSCALE={};
                                }                                                                                                            
                            }
                        }                                               
                    }    
                    
                    parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.push(dataObj);
                }                
            // storeToMongo(dbo,"SWComputation",parentObj,5);
        }                       
    }                        
}
function storeDataConstraintsObject(parentObj,dbo)
{    
    if(typeof parentObj !== 'undefined')
    {                
        var SWDataConstrs;
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC !== 'undefined')
                {
                    if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS !== 'undefined')
                    {
                        SWDataConstrs =  parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS;  
                    }
                }
            }
        }
        
        if(typeof SWDataConstrs !== 'undefined')
        {     
        delete parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS;
        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS = {};
       
            var dataConstr = readScalarOrVectorZero(SWDataConstrs['SW-DATA-CONSTR']); 
            if(typeof dataConstr !== 'undefined')
            {
                parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS.SWDATACONSTR = [];
                                                   
                    for(var i=0;i<dataConstr.length;i++)
                    {
                        dataObj = {};
                        var current = dataConstr[i];
                        var datCnstr = readScalarOrVectorZero(current['SHORT-NAME']);                        
                        if(typeof datCnstr !== 'undefined')
                        {
                            dataObj.SHORTNAME = datCnstr;
                        }
                        var description = readScalarOrVectorZero(current['DESC']);                        
                        if(typeof description !== 'undefined')
                        {
                            dataObj.DESC = description;
                        }
                        var constRule = readScalarOrVectorZero(current['SW-DATA-CONSTR-RULE']);                        
                        if(typeof constRule !== 'undefined')
                        {

                            dataObj.SWDATACONSTRRULE = {};
                            
                            var innerRule = readScalarOrVectorZero(constRule['SW-INTERNAL-CONSTRS']);
                            if(typeof innerRule !== 'undefined')
                            {
                                dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS = {};
                                var lwrLimit = readScalarOrVectorZero(innerRule['LOWER-LIMIT']);                                
                                if(typeof lwrLimit !== 'undefined')
                                {                                    
                                    if(typeof lwrLimit.ident !== 'undefined')
                                    {
                                        var Itype = readScalarOrVectorZero(lwrLimit.ident['INTERVAL-TYPE']);
                                        if(typeof Itype != 'undefined')
                                        {
                                            dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.INTERVALTYPE = Itype;
                                            if(typeof lwrLimit._ !== 'undefined')
                                            {
                                                dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.LOWERLIMIT = lwrLimit._;
                                            }
                                            else
                                            {                                                    
                                                var constRef = readScalarOrVectorZero(lwrLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                if(typeof constRef !== 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.LOWERLIMIT = {};
                                                    dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.LOWERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;                                                        
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                    else
                                    {
                                        dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.LOWERLIMIT = lwrLimit;
                                    }
                                    
                                }
                                var uprLimit = readScalarOrVectorZero(innerRule['UPPER-LIMIT']);                                
                                if(typeof uprLimit !== 'undefined')
                                {
                                    if(typeof uprLimit.ident !== 'undefined')
                                    {
                                        var Itype = readScalarOrVectorZero(uprLimit.ident['INTERVAL-TYPE']);
                                        if(typeof Itype != 'undefined')
                                        {
                                            dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.INTERVALTYPE = Itype;
                                            if(typeof uprLimit._ !== 'undefined')
                                            {
                                                dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.UPPERLIMIT = uprLimit._;
                                            }
                                            else
                                            {                                                    
                                                var constRef = readScalarOrVectorZero(uprLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                if(typeof constRef !== 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.UPPERLIMIT = {};
                                                    dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.UPPERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;                                                        
                                                }
                                            }

                                            
                                        }
                                        
                                    }
                                    else
                                    {
                                        dataObj.SWDATACONSTRRULE.SWINTERNALCONSTRS.UPPERLIMIT = uprLimit;
                                    }
                                }
                            }
                            var phyconstr = readScalarOrVectorZero(constRule['SW-PHYS-CONSTRS']);
                            if(typeof phyconstr !== 'undefined')
                            {
                                dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS = {};
                                var scaleConstrs = readScalarOrVectorZero(phyconstr['SW-SCALE-CONSTRS']);     
                                if(typeof scaleConstrs !== 'undefined') 
                                {
                                    dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS = {};
                                    var scaleConstr = readScalarOrVectorZero(scaleConstrs['SW-SCALE-CONSTR']);     
                                    if(typeof scaleConstr !== 'undefined') 
                                    {
                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR = {};                                        
                                        var lwrLimit = readScalarOrVectorZero(scaleConstr['LOWER-LIMIT']);                                
                                        if(typeof lwrLimit !== 'undefined')
                                        {      
                                            if(typeof lwrLimit.ident !== 'undefined')
                                            {
                                                var Itype = readScalarOrVectorZero(lwrLimit.ident['INTERVAL-TYPE']);
                                                if(typeof Itype != 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.INTERVALTYPE = Itype;
                                                    if(typeof lwrLimit._ !== 'undefined')
                                                    {
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.LOWERLIMIT = lwrLimit._;
                                                    }
                                                    else
                                                    {                                                    
                                                        var constRef = readScalarOrVectorZero(lwrLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                        if(typeof constRef !== 'undefined')
                                                        {
                                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT = {};
                                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;                                                        
                                                        }
                                                    }
                                                    
                                                }
                                                
                                            }
                                            else
                                            {                              
                                                dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.LOWERLIMIT = lwrLimit;
                                            }
                                        }
                                        var uprLimit = readScalarOrVectorZero(scaleConstr['UPPER-LIMIT']);                                
                                        if(typeof uprLimit !== 'undefined')
                                        {
                                            if(typeof uprLimit.ident !== 'undefined')
                                            {
                                                var Itype = readScalarOrVectorZero(uprLimit.ident['INTERVAL-TYPE']);
                                                if(typeof Itype != 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.INTERVALTYPE = Itype;
                                                    if(typeof uprLimit._ !== 'undefined')
                                                    {
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.UPPERLIMIT = uprLimit._;
                                                    }
                                                    else
                                                    {                                                    
                                                        var constRef = readScalarOrVectorZero(uprLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                        if(typeof constRef !== 'undefined')
                                                        {
                                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.UPPERLIMIT = {};
                                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.UPPERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;
                                                        }
                                                    }   
                                                    
                                                }
                                                
                                            }
                                            else
                                            {
                                                dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.SWSCALECONSTRS.SWSCALECONSTR.UPPERLIMIT = uprLimit;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var lwrLimit = readScalarOrVectorZero(phyconstr['LOWER-LIMIT']);                                
                                    if(typeof lwrLimit !== 'undefined')
                                    {    
                                        if(typeof lwrLimit.ident !== 'undefined')
                                        {
                                            var Itype = readScalarOrVectorZero(lwrLimit.ident['INTERVAL-TYPE']);
                                            if(typeof Itype != 'undefined')
                                            {
                                                dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.INTERVALTYPE = Itype;
                                                if(typeof lwrLimit._ !== 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT = lwrLimit._;                                                
                                                }
                                                else
                                                {                                                    
                                                    var constRef = readScalarOrVectorZero(lwrLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                    if(typeof constRef !== 'undefined')
                                                    {
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT = {};
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;                                                        
                                                    }
                                                }                                                                                                                                                                                             
                                            }
                                            
                                        }
                                        else
                                        {                                
                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.LOWERLIMIT = lwrLimit;
                                        }
                                    }
                                    var uprLimit = readScalarOrVectorZero(phyconstr['UPPER-LIMIT']);                                
                                    if(typeof uprLimit !== 'undefined')
                                    {
                                        if(typeof uprLimit.ident !== 'undefined')
                                        {
                                            var Itype = readScalarOrVectorZero(uprLimit.ident['INTERVAL-TYPE']);
                                            if(typeof Itype != 'undefined')
                                            {
                                                dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.INTERVALTYPE = Itype;
                                                if(typeof uprLimit._ !== 'undefined')
                                                {
                                                    dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.UPPERLIMIT = uprLimit._;                                                    
                                                }
                                                else
                                                {                                                    
                                                    var constRef = readScalarOrVectorZero(uprLimit['SW-SYSTEMCONST-PHYS-REF']);
                                                    if(typeof constRef !== 'undefined')
                                                    {
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.UPPERLIMIT = {};
                                                        dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.UPPERLIMIT.SWSYSTEMCONSTPHYSREF = constRef;                                                        
                                                    }
                                                }                                                
                                                
                                            }
                                            
                                        }
                                        else
                                        {
                                            dataObj.SWDATACONSTRRULE.SWPHYSCONSTRS.UPPERLIMIT = uprLimit;
                                        }
                                    }

                                }

                            }
                        }                        
                     
                        parentObj.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS.SWDATACONSTR.push(dataObj);       
                    }
                   // storeToMongo(dbo,"SWDataConstr",parentObj,6);

            }
                
        }

 
    }               
}

function storeSWFeatObject(parentObj,dbo)
{    
    var swFeat;
    if(typeof parentObj !== 'undefined')
    {   
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC !== 'undefined')
                {
                    if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS !== 'undefined')
                    {
                        if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS.SWFEATURE !== 'undefined')
                        {
                            swFeat = parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS.SWFEATURE;
                            delete parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS.SWFEATURE;
                           
                        }
                    }
                }
            }
        }
    }
    if(typeof  swFeat !== 'undefined')
    {
        var dataObj = {};
        var shortname = readScalarOrVectorZero(swFeat['SHORT-NAME']);                        
        if(typeof shortname !== 'undefined')
        {
            dataObj.SHORTNAME = shortname;
        }
        var longname = readScalarOrVectorZero(swFeat['LONG-NAME']);                        
        if(typeof longname !== 'undefined')
        {
            dataObj.LONGNAME = longname;
        }
        var category = readScalarOrVectorZero(swFeat['CATEGORY']);                        
        if(typeof category !== 'undefined')
        {
            dataObj.SHORTNAME = category;
        }
        var swfeatureownedelements = readScalarOrVectorZero(swFeat['SW-FEATURE-OWNED-ELEMENTS']);
        if(typeof swfeatureownedelements !== 'undefined')
        {
            dataObj.SWFEATUREOWNEDELEMENTS = {};
            var featElements = readScalarOrVectorZero(swfeatureownedelements['SW-FEATURE-ELEMENTS']);
            if(typeof featElements !== 'undefined')
            {
                dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS = featElements;
                var SWcalprmrefs = readScalarOrVectorZero(featElements['SW-CALPRM-REFS']);
                if(typeof SWcalprmrefs !== 'undefined')
                {
                    dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWCALPRMREFS = SWcalprmrefs;
                }
                var SWclassinstanceref = readScalarOrVectorZero(featElements['SW-CLASS-INSTANCE-REFS']);
                if(typeof SWclassinstanceref !== 'undefined')
                {
                    dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWCLASSINSTANCEREFS =  {};
                    var swClassInstRefSysCond = readScalarOrVectorZero(SWclassinstanceref['SW-CLASS-INSTANCE-REF-SYSCOND']);
                    if(typeof swClassInstRefSysCond !== 'undefined')
                    {
                        console.log(swClassInstRefSysCond);
                        dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWCLASSINSTANCEREFS.SWCLASSINSTANCEREFSYSCOND = [];
                        if(typeof swClassInstRefSysCond.length !== 'undefined' )
                        {
                            for(var i=0;i<swClassInstRefSysCond.length;i++)
                            {
                                var sysCondCur = swClassInstRefSysCond[i];
                                if(typeof sysCondCur !== 'undefined' )
                                {
                                    var innerObj = {};
                                    var clInstRef = readScalarOrVectorZero(sysCondCur['SW-CLASS-INSTANCE-REF']);
                                    if(typeof clInstRef !== 'undefined')
                                    {
                                        innerObj.SWCLASSINSTANCEREF = clInstRef;
                                    }
                                    var clInstRefSys =readScalarOrVectorZero(sysCondCur['SW-SYSCOND']);
                                    if(typeof clInstRefSys !== 'undefined')
                                    {
                                        innerObj.SWSYSCOND = clInstRefSys;
                                    }
                                }
                                dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWCLASSINSTANCEREFS.SWCLASSINSTANCEREFSYSCOND.push(innerObj);
                            }
                        }
                    }
                }
                var SWdatacontrref = readScalarOrVectorZero(featElements['SW-DATA-CONSTR-REFS']);
                if(typeof SWdatacontrref !== 'undefined')
                {
                    dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWDATACONSTRREFS = {};
                    if(typeof SWdatacontrref !== 'undefined')
                    {
                        var dataconstrRef = SWdatacontrref['SW-DATA-CONSTR-REF'];
                        if(typeof dataconstrRef !== 'undefined')
                        {
                            dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWDATACONSTRREFS.SWDATACONSTRREF = dataconstrRef;
                        }

                    }
                    
                }
                var SWservicerefs = readScalarOrVectorZero(featElements['SW-SERVICE-REFS']);
                if(typeof SWservicerefs !== 'undefined')
                {                    
                    dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWSERVICEREFS = {};
                    var servRef = readScalarOrVectorZero(SWservicerefs['SW-SERVICE-REF']);
                    if(typeof servRef !== 'undefined')
                    {
                        dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWSERVICEREFS.SWSERVICEREF = servRef;
                    }
                }
                var SWvarrefs = readScalarOrVectorZero(featElements['SW-VARIABLE-REFS']);
                if(typeof SWvarrefs !== 'undefined')
                {                    
                    dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWVARIABLEREFS = {};
                    var varRefSysCond = readScalarOrVectorZero(SWvarrefs['SW-VARIABLE-REF-SYSCOND']);
                    if(typeof varRefSysCond !== 'undefined')
                    {
                        console.log(varRefSysCond);
                        dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWVARIABLEREFS.SWVARIABLEREFSYSCOND=[];
                        for(var i=0;i<varRefSysCond.length;i++)
                        {
                            innerObj = {};
                            var innerVarRef = readScalarOrVectorZero(varRefSysCond[i]);
                            if(typeof innerVarRef !== 'undefined')
                            {
                                var swvarref = readScalarOrVectorZero(innerVarRef['SW-VARIABLE-REF']);
                                if(typeof swvarref !== 'undefined')
                                {
                                    innerObj.SWVARIABLEREF = swvarref;
                                }
                                var swsyscond = readScalarOrVectorZero(innerVarRef['SW-SYSCOND']);
                                if(typeof swsyscond !== 'undefined')
                                {
                                    innerObj.SWSYSCOND = swsyscond;
                                }                                
                                dataObj.SWFEATUREOWNEDELEMENTS.SWFEATUREELEMENTS.SWVARIABLEREFS.SWVARIABLEREFSYSCOND.push(innerObj);
                            }
                        }
                    }
                }
            }
        }
        var swfeatureinterfaces = readScalarOrVectorZero(swFeat['SW-FEATURE-INTERFACES']);
        if(typeof swfeatureinterfaces !== 'undefined')
        {
            dataObj.SWFEATUREINTERFACES = {};
            var swfeatInterface = readScalarOrVectorZero(swfeatureinterfaces['SW-FEATURE-INTERFACE']);
            if(typeof swfeatInterface !== 'undefined')
            {                
                if(typeof swfeatInterface.length !== 'undefined')
                {
                    dataObj.SWFEATUREINTERFACES.SWFEATUREINTERFACE = [];
                    for(var i=0;i<swfeatInterface.length;i++)
                    {
                        var innerObj = {};
                        var curInt = swfeatInterface[i];
                        var shortname = readScalarOrVectorZero(curInt['SHORT-NAME']);
                        if(typeof shortname !== 'undefined')
                        {
                            innerObj.SHORTNAME = shortname;
                        }
                        var category = readScalarOrVectorZero(curInt['CATEGORY']);
                        if(typeof category !== 'undefined')
                        {
                            innerObj.CATEGORY = category;
                        }
                        var swinterfimp = readScalarOrVectorZero(curInt['SW-INTERFACE-IMPORTS']);
                        if(typeof swinterfimp !== 'undefined')
                        {
                            innerObj.SWINTERFACEIMPORTS = swinterfimp;
                        }
                        dataObj.SWFEATUREINTERFACES.SWFEATUREINTERFACE.push(innerObj);
                    }
                }
            }
        }
        parentObj.SWSYSTEMS.SWSYSTEM.SWCOMPONENTSPEC.SWCOMPONENTS.SWFEATURE = dataObj; 
    }
}
function storeSWSchedObject(parentObj,dbo)
{    
    var swSched;
    if(typeof parentObj !== 'undefined')
    {           
        if(typeof parentObj.SWSYSTEMS !== 'undefined')
        {
            if(typeof parentObj.SWSYSTEMS.SWSYSTEM !== 'undefined')
            {
                if(typeof parentObj.SWSYSTEMS.SWSYSTEM.SWSCHEDULINGSPEC !== 'undefined')
                {                                                            
                            swSched = parentObj.SWSYSTEMS.SWSYSTEM.SWSCHEDULINGSPEC;
                            delete parentObj.SWSYSTEMS.SWSYSTEM.SWSCHEDULINGSPEC;                                                                       
                }
            }
        }
    }
    if(typeof  swSched !== 'undefined')
    {
        var dataObj = {}; 
        var swTaskSpec = readScalarOrVectorZero(swSched['SW-TASK-SPEC']);
        if(typeof swTaskSpec !== 'undefined')
        {
            dataObj.SWTASKSPEC = {};
            var swTasks = readScalarOrVectorZero(swTaskSpec['SW-TASKS']);
            if(typeof swTasks !== 'undefined')
            {
                dataObj.SWTASKSPEC.SWTASKS = {};
                var swTask = readScalarOrVectorZero(swTasks['SW-TASK']);
                if(typeof swTask !== 'undefined')
                {
                    if(typeof swTask.length !== 'undefined')
                    {
                        dataObj.SWTASKSPEC.SWTASKS.SWTASK = [];
                        for(var i=0;i<swTask.length;i++)
                        {
                            var innerObj = {};
                            var curTsk = swTask[i];
                            var desc = readScalarOrVectorZero(curTsk['DESC']);
                            if(typeof desc !== 'undefined')
                            {
                                innerObj.DESC = desc;
                            }
                            var ident = readScalarOrVectorZero(curTsk['ident']);
                            if(typeof ident !== 'undefined')
                            {
                                innerObj.ident = ident;
                            }
                            var shrtname = readScalarOrVectorZero(curTsk['SHORT-NAME']);
                            if(typeof shrtname !== 'undefined')
                            {
                                innerObj.SHORTNAME = shrtname;
                            }
                            var swSrvRef = readScalarOrVectorZero(curTsk['SW-SERVICE-REFS']);
                            if(typeof swSrvRef !== 'undefined')
                            {
                                innerObj.SWSERVICEREFS = swSrvRef;
                            }
                            dataObj.SWTASKSPEC.SWTASKS.SWTASK.push(innerObj);                            
                        }
                        parentObj.SWSYSTEMS.SWSYSTEM.SWSCHEDULINGSPEC = dataObj;
                    }
                }
            }
        }                
    }
}


async function parsePavast(db,filepath)
{
    return new Promise((resolve, reject) => 
    {
    dbo = db.db("mydb");
    var xml2js = require('xml2js');
    var fs = require('fs');
    var data = fs.readFileSync(filepath, 'utf8');

    var parser = new xml2js.Parser();                        
    //parser.parseString(data,function(err,result)                                               
    parser.parseStringPromise(data)
    .then(function(result)                                               
    {                
                console.log('Process for mongodb');                
                var collector = createHeader(result,filepath);                             
                storeVariableObject(collector,dbo);
                storeCalParamsObject(collector,dbo);
                storeCalInstanceObject(collector,dbo);
                storeSysConstObject(collector,dbo);  
                storeSWUnitObject(collector,dbo);
                storeCompuMethodObject(collector,dbo);
                storeDataConstraintsObject(collector,dbo); 
                storeSWFeatObject(collector,dbo);
                storeSWSchedObject(collector,dbo);
                console.log(filepath);
                storeToMongo(dbo,"PavastSpecification",collector,10);
                resolve(true); 
    })
    .catch(function(err)
            { 
                reject(true);
                console.log('XML parser failed');
            });  
    });
}

function readPavastFilePath()
{
    return new Promise((resolve, reject) => 
    {
        var MongoClient = require('mongodb').MongoClient;
        const path = require('path');
        
        var url = "mongodb://localhost:27017/";
        console.log('Read pavst file path');
        MongoClient.connect(url, function(err, db) 
        {        
            if (err) throw err;
            var dbo = db.db("test");        
            var cursor = dbo.collection("PavastFiles",function(err,coll)
            {
                coll.find({},function(err,cursor)
                {
                    var pathCollector = [];
                    cursor.each(function(err,data)
                    { 
                        if(data!==null)
                        {                                       
                            if(typeof data.filepath !== 'undefined')
                            {                                                
                                pathCollector.push(data.filepath);                                                                               
                            }   
                        }
                        else
                        {                    
                            resolve(pathCollector);
                            db.close();
                        }
                    });
                });
            });    
        });
    });
}



module.exports = function () {
    
    return new Promise((resolve, reject) => 
    {
        
        var MongoClient = require('mongodb').MongoClient;
        const path = require('path');
        var url = "mongodb://localhost:27017/";
        MongoClient.connect(url, function(err, db) 
        {        
            if (err) reject('Mongo DB could not be connected');

            readPavastFilePath().then(async function(fileContent_vec)
            {        
                for(var i=0;i<fileContent_vec.length;i++)
                {
                    console.log('file no',i,'call parser');
                    if(i===692)
                    {
                        var test = "test";
                    }
              
                    await parsePavast(db,fileContent_vec[i]);                                               
                }
                
                console.log('Pavast Paths Read! ',fileContent_vec);
                resolve(true);
            });
        });        
  });
}