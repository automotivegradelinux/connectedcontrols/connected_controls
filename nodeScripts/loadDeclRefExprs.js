//=========================================================================================
//
//             loadDeclRefExprs.js : load declaration reference expression nodes from the CLANG
//             AST node stored in a MongoDB
//             
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
const { response } = require('express');

module.exports = function(clientdata)
{
    // Connect to socket.io
    return new Promise((resolve, reject) => 
    {
        clientdata.on('connection', function(socket)
        {
            socket.on('requestedFuncDefAnalyze',function()
            {                                
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 
                    dbo.collection("requestedFuncDefAnalyze",async function(err,coll)
                    {                      
                        var b_requestfound = false;  
                        
                        var resp = {};                                                               
                        coll.find({},async function(err,cursor)
                        {                            
                            if(cursor !== null)
                            {                                
                                cursor.each(async function(err,data)
                                { 
                                    if(data!==null)
                                    {   
                                        b_requestfound = true;                        
                                        resp.found = b_requestfound;
                                        resp.response = data;
                        
                                        socket.emit('respFuncDefAnalyze',resp);

                                    }
                                    else
                                    {
                                        if(b_requestfound)
                                        {
                                            resp.found = b_requestfound;
                                            resp.response = 0;
                                            socket.emit('respFuncDefAnalyzeDone',resp);                                            
                                            db.close();
                                        }

                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqRTOSContainerAnalyze',function(dataClient)
            { 
                //db.RTOSTaskDescriptors.find().pretty()
                //db.RTOSTaskActivations.find().pretty()
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    var data_vec = []; 
                    dbo.collection("RTOSProcessContainerLists",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        //datainbuf.functionDefs[0]                    
                        // db.RTOSProcessLists.find({"processes.process":"SyC_PreDrv_Proc_IniEnd"}).pretty()     
                        var resp = {};                                       
                        resp.request = datainbuf;   
                                                
                            coll.find({"processes.process":resp.request.pattern}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    
                                    var data_found = false;                                                                   
                                    cursor.each(async function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            if(typeof data.processListName !== 'undefined')
                                            {
                                                data_found = true;                                                            
                                                resp.response = data.processListName;                                             
                                                socket.emit('respRTOSContainerAnalyze',resp);                                                                                                                                                      
                                            }
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;                                              
                                            resolve(data_vec);                                               
                                            socket.emit('respRTOSContainerAnalyzeDone',resp);                                                                                        
                                            db.close();
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                        
                    });
                });
            });

            socket.on('reqRTOSAnalyze',function(dataClient)
            { 
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    var data_vec = []; 
                    dbo.collection("RTOSProcessLists",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        //datainbuf.functionDefs[0]                    
                        // db.RTOSProcessLists.find({"processes.process":"SyC_PreDrv_Proc_IniEnd"}).pretty()     
                        var resp = {};                                       
                        resp.request = datainbuf;   
                                                
                            coll.find({"processes.process":resp.request.pattern}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    
                                    var data_found = false;                                                                   
                                    cursor.each(async function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            if(typeof data.processListName !== 'undefined')
                                            {
                                                data_found = true;                                                            
                                                resp.response = data.processListName;                                             
                                                socket.emit('respRTOSAnalyze',resp);                                                                                                                                                      
                                            }
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;                                              
                                            resolve(data_vec);                                                                                           
                                            socket.emit('respRTOSAnalyzeDone',resp);                         
                                            db.close();                                                               
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                        
                    });
                });
            });
            socket.on('reqDeclRefExpr',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Stmts",function(err,coll)
                    {
                        
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = dataClient;

                        
                        
                        coll.find({$and:[{"Process":resp.request.pattern},{"Stmt":"DeclRefExpr"}]}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.response = data;
                                            socket.emit('respDeclRefExpr',resp);                                            
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            socket.emit('respDeclRefExprDone',data_vec);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });
            socket.on('reqCallExpr',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Stmts",function(err,coll)
                    {
                        
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = dataClient;

                        var key = resp.request.pattern;
                                                                        
                        coll.find({$and:[{"Process":key},{"Stmt":"CallExpr"}]}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.response = data;
                                            socket.emit('respCallExpr',resp);                                            
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            socket.emit('respCallExprDone',resp);  
                                            db.close();                                            
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });
            socket.on('reqfuncDecl',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("FunctionDecls",function(err,coll)
                    {
                        
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = dataClient;

                        var key = resp.request.pattern;
                        //Stmts.find({$and:[{"Process":"ASMod_VolEff_Proc"},{"Stmt":"CallExpr"}]}).pretty()                        
                        
                        coll.find({"PtrOnDecl":key}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.response = data;
                                            socket.emit('respfuncDecl',resp);                                            
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            socket.emit('respfuncDeclDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });
            socket.on('reqDecl',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Decls",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = datainbuf;

                        var key = resp.request.pattern;
                                                
                        coll.find({PtrOnDecl:key}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;
                                            resp.response = data;                                                                                            
                                            socket.emit('respDecl',resp);
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;                                            
                                            socket.emit('respDeclDone',resp);                                              
                                            db.close();
                                            resolve(data);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            }); 
            // db.Types.find({PtrOnTypeSrcInfo:"140737168036896"}).pretty()    
            socket.on('reqType',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Types",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = datainbuf;                        
                                                                        
                        coll.find({PtrOnTypeSrcInfo:resp.request.pattern}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                                                                                                                    
                                            resp.response = data;
                                            socket.emit('respType',resp);
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;                                            
                                            socket.emit('respTypeDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });  
            socket.on('reqTypeDefDecl',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("TypeDefs",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        //datainbuf.functionDefs[0]                    
                        //db.Stmts.find({Process:"ASMod_VolEff_Proc"}).pretty()
                        var resp = {};                                       
                        resp.request = datainbuf;
                                                                                                
                        coll.find({PtrOnDecl:resp.request.pattern}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            socket.emit('respTypeDefDecl',data);
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            resp.response = data_vec;
                                            socket.emit('respTypeDefDeclDone',resp);                                             
                                            db.close(); 
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });  
            socket.on('reqMDXSWVariable',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("PavastSpecification",function(err,coll)
                    {
                                                
                        //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE.SHORTNAME":"ASMod_tIntMnfUs"}
                        var resp = {};
                        resp.request = dataClient;                                       

                        var key = resp.request.pattern;
                                                                                                
                        coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE.SHORTNAME":key}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.found = true;                                                                                                                                               
                                            
                                            
                                            for(var varIt=0;varIt<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE.length;varIt++)
                                            {
                                                if(key.localeCompare(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE[varIt].SHORTNAME)==0)
                                                {                                                     
                                                    data_vec.push(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWVARIABLES.SWVARIABLE[varIt]);
                                                }    
                                            } 
                                            resp.response = data_vec;                                                                                       
                                            socket.emit('respMDXSWVariable',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            resp.response = data_vec;
                                            socket.emit('respMDXSWVariableDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });   
            //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS":{$exists:"true"}}).pretty()
            socket.on('reqCompuMethod',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("PavastSpecification",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        
                        //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":...}).
                        var resp = {};                                       
                        resp.request = datainbuf;
                        
                        var key = resp.request.pattern;
                        
                                                                        
                        coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":key}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.found = true;                                                                                                                                               
                                            
                                            
                                            for(var varIt=0;varIt<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.length;varIt++)
                                            {
                                                if(key.localeCompare(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD[varIt].SHORTNAME)==0)
                                                {                                                     
                                                    data_vec.push(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD[varIt]);
                                                }    
                                            } 
                                            resp.response = data_vec;                                                                                       
                                            socket.emit('respCompuMethod',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            resp.response = data_vec;
                                            socket.emit('respCompuMethodDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            }); 
            //reqStorageOfStmtTreeData
            socket.on('reqStorageOfStmtTreeData',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeFunctionTreeData",function(err,coll)
                    {                                                                                                                     
                        coll.insertOne( {data : dataClient}  );                                                                                               
                    });
                });
            });
            socket.on('reqUpdateInitialData',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    
                    dbo.collection("initialDir",function(err,coll)
                    {                                                                                                                     
                        coll.updateOne( {req1stLevelFuncAnalysis:{$exists:true}},{$set:{req1stLevelFuncAnalysis:dataClient}});                                                                                               
                    });
                });
            });   
            socket.on('reqControlData',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    dbo.collection("initialDir",function(err,coll)
                    {                                                                                                                     
                        coll.find({}, function(err,cursor)
                        {                                                                
                            if(cursor !== null)
                            {                                                 
                                var resp = {};                   
                                var data_found = false;                                                                   
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {     
                                        data_found = true;
                                        resp.response = data;
                                        socket.emit('respControlData',resp);
                                    }
                                    else
                                    {
                                        resp.found = data_found;
                                        socket.emit('respControlDataDone',resp);
                                    }
                                });
                            }
                        });
                    });
                });
            });   
            socket.on('reqLoadingOfStmtTreeData',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeFunctionTreeData",function(err,coll)
                    {                                                                                                                     
                        coll.find({}, function(err,cursor)
                        {                                                                
                            if(cursor !== null)
                            {                                                 
                                var resp = {};                   
                                var data_found = false;                                                                   
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {     
                                        data_found = true;
                                        resp.response = data;
                                        socket.emit('respLoadingOfStmtTreeData',resp);
                                    }
                                    else
                                    {
                                        resp.found = data_found;
                                        socket.emit('respLoadingOfStmtTreeDataDone',resp);
                                    }
                                });
                            }
                        });
                    });
                });
            });         
            socket.on('reqCallTree',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeCallTree",function(err,coll)
                    {                                                                                                                     
                        coll.find({}, function(err,cursor)
                        {                                                                
                            if(cursor !== null)
                            {                                                 
                                var resp = {};                   
                                var data_found = false;                                                                   
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {     
                                        data_found = true;
                                        resp.response = data;                                        
                                        socket.emit('respCallTree',resp);
                                    }
                                    else
                                    {
                                        resp.found = data_found;
                                        socket.emit('respCallTreeDone',resp);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqStoreCallTree',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeCallTree",function(err,coll)
                    {                                                                                                                     
                        coll.insertOne( {data : dataClient}  );                                                                                               
                    });
                });
            });
            socket.on('functionStim',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeStimInterface",function(err,coll)
                    {                                                                                                                     
                        coll.insertOne( {data : dataClient}  );                                                                                               
                    });
                });
            });
            socket.on('functionData',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("storeDaqInterface",function(err,coll)
                    {                                                                                                                     
                        coll.insertOne( {data : dataClient}  );                                                                                               
                    });
                });
            });

            socket.on('reqSWUnit',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("mydb");
                    
                    dbo.collection("PavastSpecification",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        
                        //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":...}).
                        var resp = {};                                       
                        resp.request = datainbuf;
                        
                        var key = resp.request.pattern;
                        
                                                                        
                        coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.SHORTNAME":key}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.found = true;                                                                                                                                               
                                            
                                            
                                            for(var varIt=0;varIt<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.length;varIt++)
                                            {
                                                if(key.localeCompare(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT[varIt].SHORTNAME)==0)
                                                {                                                     
                                                    data_vec.push(data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT[varIt]);
                                                }    
                                            } 
                                            resp.response = data_vec;                                                                                       
                                            socket.emit('respSWUnit',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            resp.response = data_vec;
                                            socket.emit('respSWUnitDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            }); 
            socket.on('reqLeftHandStmt',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Stmts",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        
                        //db.Stmts.find({PtrOnCurrentStmt:"140737180789248"}).pretty()
                        var reqObj = datainbuf;
                        var resp = {};                                       
                        resp.request = datainbuf;

                        

                                                                        
                        coll.find({PtrOnCurrentStmt:reqObj.pattern}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            
                                                                                                                                                            
                                            resp.response = data;                                                                                       
                                            socket.emit('respLeftHandStmt',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            resp.response = data_vec;
                                            socket.emit('respLeftHandStmtDone',resp); 
                                            db.close();                                             
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });  
            socket.on('reqAssignments',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Stmts",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        
                        //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":...}).
                        var resp = {};                                       
                        resp.request = datainbuf;
                        var key = resp.request.pattern;
                                                                                                                        
                        coll.find({$and:[{"Process":key},{"Stmt":"BinaryOperator"},{"opcodestr":"="}]}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.found = true;                                                                                                                                               
                                                                                                                
                                            
                                            resp.response = data;                                                                                       
                                            socket.emit('respAssignments',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            
                                            socket.emit('respAssignmentsDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });  
            socket.on('reqLoadingOfExtFuncDefinitions',function(dataClient)
            {
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");
                    
                    dbo.collection("Stmts",function(err,coll)
                    {
                        var datainbuf = dataClient;
                        
                        //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":...}).
                        var resp = {};                                       
                        resp.request = datainbuf;
                        var key = resp.request.pattern;
                                                         
                        
                        coll.find({$and:[{"Process":key},{"Stmt" : "DeclRefExpr" }]}, function(err,cursor)
                            {                                                                
                                if(cursor !== null)
                                {                                    
                                    var data_vec = []; 
                                    var data_found = false;                                                                   
                                    cursor.each(function(err,data)
                                    { 
                                        if(data!==null)
                                        {     
                                            data_found = true;                                                            
                                            resp.found = true;                                                                                                                                               
                                                                                                                
                                            
                                            resp.response = data;                                                                                       
                                            socket.emit('respLoadingOfExtFuncDefinitions',resp);                                                                                                                                        
                                        }                                                            
                                        else
                                        {                    
                                            resp.found = data_found;
                                            
                                            socket.emit('respLoadingOfExtFuncDefinitionsDone',resp);                                              
                                            db.close();
                                            resolve(data_vec);                                                                                           
                                        }                                                            
                                    });                                                        
                                }                                                                                                                                   
                            });
                    });
                });

            });                                   
                     
        });
    });
}