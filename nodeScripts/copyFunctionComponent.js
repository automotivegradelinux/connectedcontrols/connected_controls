//==========================================================================================
//
//             copyFunctionComponent.js : this module is creating a requested 
//             target directory under a node.js public folder and copies
//             selected source code files into that directory. 
//              
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
const fs = require('fs');
const path = require('path');

function mkDirByPathSync(targetDir, { isRelativeToScript = false } = {}) {
    // Path segment separator like '/'
    const sep = path.sep;
    const initDir = path.isAbsolute(targetDir) ? sep : '';
    const baseDir = isRelativeToScript ? __dirname : '.';
  
    return targetDir.split(sep).reduce((parentDir, childDir) => {
      const curDir = path.resolve(baseDir, parentDir, childDir);
      try {
        fs.mkdirSync(curDir);
      } catch (err) {
        if (err.code === 'EEXIST') { // curDir already exists!
          return curDir;
        }
  
        // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
        if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
          throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
        }
  
        const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
        if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
          throw err; // Throw if it's just the last created dir.
        }
      }
  
      return curDir;
    }, initDir);
  }


module.exports = function (filepath_vec,baseDir) {
  
  return new Promise((resolve, reject) => 
    {
        var Cdir = path.dirname(filepath_vec[0].CFile);
        var CsplitDir = Cdir.split(baseDir);
        var CpureDir = CsplitDir[CsplitDir.length-1];

        var CpureDir_vec = CpureDir.split('/');
        var CconcatStr="/source";
        CpureDir = "/source" + CpureDir;    
        var absDir;
        for(var pr=0;pr<CpureDir_vec.length;pr++)
        {
            if(CpureDir_vec[pr])
            {
                CconcatStr=CconcatStr+'/'+CpureDir_vec[pr];
                var Cpth = path.join(__dirname+CconcatStr);                    
                console.log(Cpth);
                absDir = Cpth;
                mkDirByPathSync(Cpth);                                                               
            }
        }

        var src = filepath_vec[0].CFile;
        var dest = '.'+CpureDir+'/'+path.basename(filepath_vec[0].CFile);
        absDir += '/'+path.basename(filepath_vec[0].CFile);
        fs.copyFileSync(src,dest);

        var relDir_vec = [];
        for(var i=0;i<filepath_vec.length;i++) 
        {
          var dir = path.dirname(filepath_vec[i].Header);
          var splitDir = dir.split(baseDir);
          var pureDir = splitDir[splitDir.length-1];
          var Dirfound = false;
          for(var j=0;j<relDir_vec.length;j++)
          {
            if(!relDir_vec[j].localeCompare(pureDir))
            {
                Dirfound = true;
                break;
            }            
          }

          if(!Dirfound)
          {            
            var pureDir_vec = pureDir.split('/');
            var concatStr="/source";
    
            for(var pr=0;pr<pureDir_vec.length;pr++)
                {
                    if(pureDir_vec[pr])
                    {
                        concatStr=concatStr+'/'+pureDir_vec[pr];
                        var pth = path.join(__dirname+concatStr);                    
                        
                        mkDirByPathSync(pth);                                                               
                    }
                }
            relDir_vec.push(pureDir);
          }        
          pureDir = "/source" + pureDir;
          var hdrSrc = filepath_vec[i].Header;
          var hdrDSt = '.'+pureDir+'/'+path.basename(filepath_vec[i].Header);
          fs.copyFileSync(hdrSrc,hdrDSt);
        }
       resolve(absDir);
      });
}