//==========================================================================================
//
//             checkCreateNInitializePgTbl.js : this module provides a service
//             which checks the existence of a postgres table and creates
//             it if it does not exist. 
//              
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
function createTable(client,table,column,no)
{
    var qrstring = "CREATE TABLE public."+table+ "( id serial NOT NULL PRIMARY KEY, " + column + " jsonb NOT NULL);";
        client.query(qrstring)
              .then( res=>
                {
                    console.log(res);                                   
                })
              .catch();        
}
function checkTables(client,matchcond)
{
    var table_vec = [];
    qrstr = "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';";
    client.query(qrstr).then(async res => 
        {                 
            var found = false;
            for (let i in res.rows)
            {
                var current = res.rows[i].tablename;                
                if(!current.localeCompare(matchcond))
                {                    
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                createTable(client,matchcond,'report','number');
            }

            
        })
        .catch();
}
module.exports = function(table,column,element,data)
 {
      /**
       * Pool of postgres clients object
       * @typedef {Object} Pool
       * @property {string} user - postgres user
       * @property {string} host - posgres host
       * @property {string} database - database name
       * @property {string} password - password
       * @property {number} port - port
       * @property {method} connect - connection method
       */
       const {Pool} = require('pg');
 
       const pool = new Pool({
         user: 'ThermBaseAlgebraic',
         host: 'db',
         database: 'ThermoBaseDataStationary',
         password: 'thermo',
         port: 5432
     });
   
     
   
     pool.connect().then(client => 
     {
        checkTables(client,'mdxreport');
        return;
     });
 }