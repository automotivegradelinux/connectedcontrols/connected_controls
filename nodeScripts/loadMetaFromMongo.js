//=========================================================================================
//
//             loadMetaFromMongo.js : load MSR meta data from MongoDB
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//

function getDataFromInstance(dbo,socket)
{
       var coll = dbo.collection("PavastSpecification");
       //db.PavastSpecification.find({"SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE.SWINSTANCE.SHORTNAME":

       socket.on('DataInstanceReq', function(sninstance)
       {
            var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE.SWINSTANCE.SHORTNAME":sninstance.Request});                     
            cursor.each(function(err,data){ 
            if(data!==null)
            {                     
                var SWInstance = data.SWSYSTEMS.SWSYSTEM.SWINSTANCESPEC.SWINSTANCETREE.SWINSTANCE;
                for(var i=0;i<SWInstance.length;i++)
                {                    
                    var cmpStr = SWInstance[i].SHORTNAME;
                    if(!sninstance.Request.localeCompare(cmpStr))
                    {
                        var instObj = {};
                        instObj.Request = sninstance.Request;
                        instObj.Source = sninstance.Source;
                        instObj.Response = SWInstance[i];
                        instObj.Status = sninstance.Status;
                        instObj.Property = sninstance.Property;
                        instObj.FILEPATH      = data.FILEPATH;
                        socket.emit('requestedDataInstance', instObj);        
                    }
                }
            }
            else
            {
                socket.emit('requestedDataInstanceDone', true);
            }
        });
    }); 
}


function getFilePaths(dbo,socket)
{
    socket.on('reqMetaData', function(request){
    var req = request;
    var coll = dbo.collection("PavastSpecification");
    if(!req.localeCompare("FILEPATH"))
    {
        var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS.SWCALPRM":{$exists:true}},function(err,cursor)
        {
            cursor.each(function(err,data)
            { 
                if(data!==null)
                {                           
                    socket.emit('MetaData',data.FILEPATH);                    
                }
                else
                {            
                    socket.emit('MetaDataDone','Done');                    
                }
        });
        });
    }
});
}

function getParameterData(dbo,socket)
{
    socket.on('reqParmData', function(request){    
    var coll = dbo.collection("PavastSpecification",function(err,coll)
    {
        var cursor = coll.find({FILEPATH:request.Request},function(err,cursor)
        {
            var b_datafound = false;
            cursor.each(function(err,data)
            {    
                if(data!==null)
                {                         
                    if(typeof data.SWSYSTEMS !== 'undefined')
                    if(typeof data.SWSYSTEMS.SWSYSTEM !== 'undefined')
                    if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC !== 'undefined')
                    if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS !== 'undefined')
                    if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS.SWCALPRM !== 'undefined')
                    {
                        b_datafound = true;
                        outObj = {};                        
                        outObj.Request = request.Request;
                        outObj.Response = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCALPRMS.SWCALPRM;
                        outObj.Source = request.Source;
                        outObj.Status = request.Status;
                        socket.emit('RequestedParm', outObj);                    
                    }
                }
                else
                {
                    socket.emit('RequestedParmDone',b_datafound);
                }
            });
        });    
    });
});
}


module.exports = function(clientdata)
{
    // Connect to socket.io
    

    clientdata.on('connection', function(socket)
    {
        console.log('Now I am creating the mongo client');
        var MongoClient = require('mongodb').MongoClient;         
        // Establish connection to db        
        var url = "mongodb://localhost:27017/";
        MongoClient.connect(url, function(err, db) 
        {        
            if (err) throw err;
                        
            var dbo = db.db("mydb");
            getFilePaths(dbo,socket);
            getParameterData(dbo,socket);
            getDataFromInstance(dbo,socket);                                            
            console.log('Parameter loaded');
            socket.on('AxisDataConstrRef',function(reqObj)
            {
                var dataClient = reqObj.Request;
                console.log('AxisDataConstrRef : ',dataClient);
                var resp = {};
                resp.Request    = dataClient;
                resp.success     = false;                
                var coll = dbo.collection("PavastSpecification",function(err,coll)
                {                                
                    var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS.SWDATACONSTR.SHORTNAME":dataClient},function(err,cursor)
                    {
                        cursor.each(function(err,data)
                        { 
                            if(data!==null)
                            {                                                                                           
                                var dataConstr = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWDATACONSTRS.SWDATACONSTR;
                                for(var datCstrIter=0;datCstrIter<dataConstr.length;datCstrIter++)
                                {
                                    var curConstr = dataConstr[datCstrIter];
                                    if(!curConstr.SHORTNAME.localeCompare(dataClient))
                                    {                                                   
                                        resp.success = true;
                                        var respObj = {};
                                        respObj.Request     = dataClient;
                                        respObj.Response    = curConstr;
                                        respObj.Source      = reqObj.Source;
                                        respObj.FILEPATH      = data.FILEPATH;
                                        socket.emit('RequestedDataConstr', respObj); 
                                    }
                                }
                            } 
                            else
                            {
                                socket.emit('RequestedDataConstrDone', resp); 
                            }                      
                        });
                    });
                });
            });
            socket.on('SysCondAxis', function(reqDat)
            {         
              var dataClient = reqDat.Request;
              console.log('Sysconst query : ',dataClient);
              var coll = dbo.collection("PavastSpecification",function(err,coll) 
              {                       
                    console.log(dataClient);
                    var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST.SHORTNAME":dataClient},function(err,cursor)
                    {
                    cursor.each(function(err,data)
                    { 
                        if(data!==null)
                        {             
                            console.log('Sysconst query : ',data);
                            for(var sysConIter=0;sysConIter<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST.length;sysConIter++)
                            {                        
                                    if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST !== 'undefined')
                                    {
                                        var sn = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST[sysConIter].SHORTNAME;
                                        if(!sn.localeCompare(dataClient))
                                        {                                         
                                            var pairObj = {};
                                            pairObj.Request = dataClient;
                                            pairObj.Response = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWSYSTEMCONSTS.SWSYSTEMCONST[sysConIter];
                                            pairObj.Source = reqDat.Source;
                                            pairObj.FILEPATH = data.FILEPATH;
                                            socket.emit('RequestedMaxAxSysConds', pairObj);
                                        }
                                    }                        
                            }
                        }
                        else
                        {
                            socket.emit('RequestedMaxAxSysCondsDone', true);
                        }
                    });
                    });
                }
                ); 
            }
            );

            socket.on('CompuMethReq', function(reqObj)
            {
                var dataClient = reqObj.Request;
                console.log('Compu method : ',dataClient);
                var coll = dbo.collection("PavastSpecification", function(err,coll)
                {    
                    var key = dataClient;          
                    var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.SHORTNAME":key});
                    var processedData = [];
                    cursor.each(function(err,data)
                    { 
                    
                        if(data!==null)
                        {             
                            console.log('Compu method found data: ',data);
                            for(var sysConIter=0;sysConIter<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD.length;sysConIter++)
                            {                                                
                                if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD !== 'undefined')
                                {
                                    var sn = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD[sysConIter].SHORTNAME;
                                    var b_found = false;
                                    if(processedData.length > 0)
                                    {
                                        for(var prcIter=0;prcIter<processedData.length;prcIter++)
                                        {
                                            if(!dataClient.localeCompare(processedData[prcIter]))
                                            {
                                                b_found = true;
                                            }
                                        }
                                    }
                                    if(!sn.localeCompare(dataClient) && !b_found)
                                    {              
                                        console.log('found compu method',dataClient);                           
                                        var pairObj = {};
                                        pairObj.Request = dataClient;
                                        pairObj.Response = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWCOMPUMETHODS.SWCOMPUMETHOD[sysConIter];
                                        pairObj.Source = reqObj.Source;   
                                        pairObj.FILEPATH      = data.FILEPATH;                                     
                                        socket.emit('RequestedCompuMeth', pairObj);
                                        processedData.push(dataClient);
                                        break;
                                    }
                                }                        
                            }
                        }
                        else
                        {
                            socket.emit('RequestedCompuMethDone', true);
                        }
                    });
                });                
            });

            socket.on('RequestSWUnit', function(dataClient)
            {                
                var coll = dbo.collection("PavastSpecification",function(err,coll)
                {     
                    var key = dataClient.Request;                         
                    var cursor = coll.find({"SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.SHORTNAME":key},function(err,cursor)
                    {                        
                        var b_data = false;
                        cursor.each(function(err,data)
                        { 
                            if(data!==null)
                            {             
                                b_data = true;
                                for(var sysConIter=0;sysConIter<data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT.length;sysConIter++)
                                {                                                
                                    if(typeof data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT !== 'undefined')
                                    {
                                        var sn = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT[sysConIter].SHORTNAME;                                      
                                        if(!sn.localeCompare(dataClient.Request))
                                        {                                         
                                            var pairObj = {};
                                            pairObj.Request = dataClient.Request;
                                            pairObj.Source = dataClient.Source;
                                            pairObj.Response = data.SWSYSTEMS.SWSYSTEM.SWDATADICTIONARYSPEC.SWUNITS.SWUNIT[sysConIter];
                                            pairObj.FILEPATH = data.FILEPATH;
                                            socket.emit('RequestedSWUnit', pairObj);
                                            
                                            break;
                                        }
                                    }                        
                                }
                            }
                            else
                            {
                                var datObj = {};
                                datObj.Request = dataClient.Request;
                                datObj.success = b_data;
                                socket.emit('RequestedSWUnitDone',datObj);   
                            }
                        });  
                    });
                });            
            });

            socket.on('RequestKennfeld', function(dataClient)
            {
                var coll = dbo.collection("DCMData",function(err,coll)
                {
                    var key = dataClient.Request;                         
                    var cursor = coll.find({KFNAME:key},function(err,cursor)
                    {
                        var b_data = false;
                        cursor.each(function(err,data)
                        { 
                            if(data!==null)
                            {      
                                b_data = true;
                                var pairObj = {};
                                pairObj.Request = dataClient.Request;
                                pairObj.Source = dataClient.Source;
                                pairObj.Response = data;                            
                                socket.emit('RequestedKennfeld', pairObj);                              
                            }
                            else
                            {
                                var datObj = {};
                                datObj.Request = dataClient.Request;
                                datObj.success = b_data;
                                socket.emit('RequestedKennfeldDone',datObj);   
                            }
                        });
    
                    });
                });
            });                        
        });

     
                              
        socket.on('SWVariableRef',function(dataClient)
        {
            console.log('SW variable ref : ',dataClient);
        }
        );
            
    });
    console.log('Client handler for cal interface created');
}