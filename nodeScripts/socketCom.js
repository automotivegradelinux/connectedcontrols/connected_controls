
//==========================================================================================
//
//             socketCom.js : this module processes socket.io events which are
//             triggered either by the web client or by a time periodic event on the server. 
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//

/**
 * <h3>Communication via socket.io</h3>
 * <table>
 *   <tr><th>socket event emit</th><th>socket event on (receive request)</th><th>description</th></tr>
 *   <tr><td>name:connection<br>call:socket.connect();<br><br>@see [client: function selector view (dispatcher.html)]{@link HTML/dispatcher.html}</p></td><td>name:connection<br>call:io.on('connection',...<br><br>@see [server : connect server to socket]{@link c_socketCom_server.c_1_socketfunction.event:connection}</td><td>function selection web client starts the communication</td></tr>
 * </table>
 * <p> The socketCom module consists of only one function which is described in the following namespace: </p>
 * <p>@see {@link c_socketCom_server.c_1_socketfunction}</p>
 * <p>Details are described in the tutorial</p> 
 * <p>See {@tutorial staticEngineModel}</p>
 * <h3>Parent module</h3>
 * <p> Index.js See {@link module:index}</p>
 * <h3>Child modules</h3>
 * <p>See {@link module:f_getPostgresData_server}</p>
 * <p>See {@link module:g_setPostgresData_server}</p>
 * <p>See {@link module:j_updatePostgresData_server}</p>
 * <p>See {@link module:h_deletePostgresData_server}</p>
 * <p>See {@link module:k_select-stimulation_client}</p>
 * <p>@see {@link c_socketCom_server.c_1_socketfunction.event:getTable}</p>
 * 
 * @module c_socketCom_server
 */

/** @function 
 * @description exported function : needed to select and visualize audio data
 * <p> - the internals of this function are described in the linked namespace
 * <p>{@link c_socketCom_server.c_1_socketfunction}
*/
module.exports = function(server)
{   
 /**
     * Foo object (@see: {@link module:socketCom~function})
     * @name c_socketCom_server
     * @inner
     * @private
     * @memberof module:socketCom
     * @property {Object} c_socketCom_server - The foo object
     * @property {Object} c_socketCom_server.c_1_socketfunction - The bar object     
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected - The bar object     
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata - The bar object     
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_3_curvedata - The bar object  
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_4_getTable - The bar object
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_5_getCurve - The bar object
     * @property {Object} c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_6_getStimData - The bar object
     */
 
   /**
     * This namespace describes the function body of the server sided socket.io
     * interface
     * @memberof c_socketCom_server
     * @type {object}
     * @namespace c_socketCom_server.c_1_socketfunction
     * 
     */

   /**
    * socket io object created upon the existing http server 
    * @memberof c_socketCom_server.c_1_socketfunction
    * @type {object}
    * @see {@link socket.io} 
    */
    const io = require('socket.io')(server);
    /**
     * Getter function of postgres data
    * @memberof c_socketCom_server.c_1_socketfunction
    * @type {function}
    * @see {@link module:f_getPostgresData_server}
    */
    const pg = require('./getPostgresData.js');
    /**
    * @memberof c_socketCom_server.c_1_socketfunction
    * @param {object}
    */
    const pgSet = require('./setPostgresData.js');
    const pgUpdate = require('./updatePostgresData.js');
    /**
    * @memberof c_socketCom_server.c_1_socketfunction
    * @param {object}
    */
    const pgDel = require('./deletePosgresData.js');

    const pgArApnd = require('./appendJSONBArray.js');

    const pgCreate = require('./createPostgresTable.js');
    const pgStim = require('./updateStimValuesInPg.js');
    /**
    * @memberof c_socketCom_server.c_1_socketfunction
    * @param {object}
    */
    const getWav = require('./getWavFilesFromLinux.js');
    const checkNcreate = require("./checkCreateNInitializePgTbl.js");
    checkNcreate();
    const getASAMMdx = require("./getASAMMDXxml.js")
    

    
    
    /**     
     * @memberof c_socketCom_server.c_1_socketfunction    
     * @event connection
     * @type {object}
     * @property {object} connection - the connection callback opens up a chain of socket events.     
     * <br><br>@see {@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected}
     */

    io.on('connection', (socket) => {

    /**
     * This namespace describes the function body of the server sided socket.io
     * callback function on the connection event. Each sub-namespace represents a callback function
     * on a socket event which occurs afters the socket is connected. Follow the links with the short 
     * description or the namespace names to get the details (follow the link to find the details about the server strategy).
     * 
     * The following steps are executed by the server on a socket connection : 
     * <li>[emit 1000ms periodic socket.io event named 'time']{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:time}</li>
     * <li>[send postgres table "ThermoBaseDataStationary" to the client by emitting the socket.io event 'thermdata']{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.pg}</li>
     * 
     * 
     * 
     * @memberof c_socketCom_server.c_1_socketfunction
     * @type {object}
     * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
     * 
     * @see [server: start processing new set of formatted curve data]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData}
     * <br>[server: store formatted 3d table data to postgres]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata}
     * <br>[server: store formatted 2d curve data to postgres]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_3_curvedata}
     * <br>[server: read raw table data from postgres and send it to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_4_getTable}
     * <br>[server: read raw curve data from postgres and send it to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_5_getCurve}
     * <br>[server: read stimulation data from file/postgres and send it to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_6_getStimData}
     * 
     */
     
        console.log('Client connected');
        console.log(socket.handshake.headers.host);        
        getASAMMdx(socket);

        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @function setInterval
          * @param {function} socket.emit
          * @param {number} 1000
          * @description creates a periodic time interval which emits the 'time'
          * event on the socket.io interface. The 'time' event triggers the 
          * simulation view of the client and starts a new calculation in the 
          * client perspective.
          * @see [time event]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.event:time}
          */
        
        setInterval(() => socket.emit('time', 
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event time
          * @type {object}
          * @property {method} toTimeString - convert current system time to string          
          * @description [client : simulation view time event]{@link n_create_simulation_view_client.n_1_create_simulation_view.event:periodic_time}          
          */

          new Date().toTimeString()),
         1000);
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event disconnect
          * @type {object}   
          * @description ouput of the disconnected status on the standard console  
          */
        socket.on('disconnect', () => console.log('Client disconnected'));
           /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @function pg
          * @param {object} socket socket object
          * @param {string} ThermoBaseDataStationary name of the database
          * @param {string} thermdata name of the emitted event
          * @description reads the ThermoBaseDataStationary data table 
          * to the "function selection" view of the client. 
          * <br><br> Find the client's read event callback by following the link
          * <br>[client : process thermodynamic engine data]{@link b_create_caldata_buttons_client.b_1_dataviz.b_1_4_thermdata_client}
          * <br><br> Find the precise description of the postgres interface by following the link. 
          * <br>[server : postgres interface - get data]{@link module:f_getPostgresData_server}          
          */
        pg(socket,"ThermoBaseDataStationary",'thermdata');
        pg(socket,"initialdir",'sourceCodePaths');

        socket.on('SWDirSelect', function(selectedDir){
                                     
            const pgUpdJSONB = require('./updatePostgresJSONB.js')
        
            pgUpdJSONB("initialdir","toolsctrl","selecteddir",selectedDir);
            pgUpdJSONB("initialdir","toolsctrl","reqdirstruct","true");        
            pgUpdJSONB("initialdir","toolsctrl","reqcfilestruct","true");        
            pgUpdJSONB("initialdir","toolsctrl","reqheaderstruct","true"); 
            pgUpdJSONB("initialdir","toolsctrl","reqostaskstruct","true"); 
            pgUpdJSONB("initialdir","toolsctrl","reqmetamodelstruct","true"); 
        });        
//------------------------------------------------------------------------------------------
// Start of initNewData event
//------------------------------------------------------------------------------------------        
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event initNewData
          * @type {function}
          * @property {function} () Initializes postgres for a new set of formatted
          * calibration curves
          * @description by clicking on curve typed engine cal. data in the function selection view (dispatcher.html) 
          * the click event handler emits the 'initNewData' event. It starts the processing of a set of curves
          * like the pressure ratio across a compressor wheel. The serves reacts on this event by initializing 
          * new data structures and setting the "ActiveView" parameter to "curve". <br>
          * <br> Find the details in the link. 
          * <br>[server: start processing new set of formatted curve data]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData}
          */
        socket.on('initNewData', function(){  
          /**
          * This namespace describes the function body of the server sided 'initNewData'
          * event. It initializes the calibration data structure by setting the
          * 'ActivateView' object and postgres table to 'curve' and deleting the
          * postgres data table 'ThermoCalDataCurves'. 
          * There are 2 events which fill content into the curve data structures : 
          * <li>'initNewData'</li>
          * <li>'curvedata'</li>
          * <br>
          * The 'curvedata' event processes all curves which belong to a data element,
          * e.g. the curves of the pressure across the compressor wheel. 
          * <br><br>Find the details about the callback function which triggers those events in 
          * the following link. <br>
          * [client : process data for turbine and compressor]{@link data.d_1_handler.forLoopTurbineCompressor}
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
          * 
          */ 

          /**
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
          * @typedef {object} type
          * @property {string} view status string : 'contant', 'curve' or 'map'
          * 
          */         
         
          /**
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
          * @type {type}
          * @description initialized by 'curve'
          */         
          var type = {}

          type.view = 'curve';

          /**          
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
          * @function pgUpdate
          * @param {string} ActiveView postgres table
          * @param {string} View postgres column in table
          * @param {type} type type object - indicates "curve" view in given case
          * @description Set active view in postgres to "curve".
          *  @see {@link module:j_updatePostgresData_server}
          */    
          pgUpdate('ActiveView','View',type);                           

          /**          
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_1_initNewData
          * @function pgDel
          * @param {string} ThermoCalDataCurves Database
          * @param {string} ThermoCalDataCurves Table          
          * @description delete previous calibration curves because this is the first step of the definition of a new set of curves. 
          * @see {@link module:h_deletePostgresData_server}
          */    
          pgDel("ThermoCalDataCurves","ThermoCalDataCurves");
        });
//------------------------------------------------------------------------------------------
// End of initNewData event
//------------------------------------------------------------------------------------------
socket.on('getSourceCodePath',function(data){
  pg(socket,"initialdir",'sendSourceCodePath');
});

socket.on('getDirectoryStruct',function(data){
  pg(socket,"directories",'sendDirectoryStruct');
});

socket.on('getCFiles',function(data){  
  pg(socket,"cfiles",'sendCFiles','csrcs','parentID',data);
});

socket.on('reqCFileAnalysis',function(data){ 
   
  var obj = {};
  obj.pathname = data;
  var jsondat = JSON.stringify(obj);
  pgArApnd('initialdir','cfilestoparse','pathnames',jsondat);
  
});

socket.on('getheaderFiles',function(data){
  pg(socket,"headerfiles",'sendHeaderfiles');
});
socket.on('getHeadertree',function(data){
  pg(socket,"headertree",'sendHeaderTree');
});

socket.on('getIOSignals', function(data){
  pg(socket,"simulationio",'sendIOSignals');
});

socket.on('prepareStim', function(data){  
  var collector = [];
  var unique_names = [];
  for(var iter=0;iter<data.length;iter++)
  {
    var obj = {};    
    var wavConverter = require('wav-converter');
    var fs = require('fs');
    var path = require('path');
    var name = data[iter].name;
    var filename = data[iter].audiofilename;
    var Arraysize = data[iter].Arraysize;
    var flatName = data[iter].nameflatelement;
    var words = flatName.split("_ar_");
    var id = words[words.length-1];
    
    var num = parseInt(Arraysize) || 0;
 
    var wavData = fs.readFileSync(path.resolve("./testdata", filename));
    var pcmData = wavConverter.decodeWav(wavData);
    var innercollect = [];
    for(var i=0;i<100; i++)
    {
      var loc = pcmData[i];
      innercollect.push(loc);
    }

    //let col = elementname.toLowerCase();    
    var jsondat = JSON.stringify(innercollect);    
          // var column = "simio";
             //  element = "values";
             //  table = "simulationio";            
             // data = [0.2]
             // subdocu = {"name":"ASMod_tIntMnfUs"}         
             
    var subdocu = "{\"name\":\""+ name + "\"}";
  
    if(num===0)
    {       
      pgStim('simulationio','simio','values',jsondat,subdocu);               
    }
    else
    {
      pgStim('simulationio','simio','values'+id,jsondat,subdocu);                
    }
  }
  const pgUpdJSONB = require('./updatePostgresJSONB.js')
  pgUpdJSONB("initialdir","toolsctrl","stim_state","stim_requested"); 
 
});
socket.on('getStatusInfo', function(data){  
  pg(socket,"initialdir",'sendStatusInfo');

  });
socket.on('prepareDaq', function(data){  
    const pgGenQuery = require('./genericPGquery.js');
    pgGenQuery("SELECT simio FROM public.simulationio WHERE simio @> '{\"isLHS\": \"true\"}'").
    then(function(data)
    {
      console.log(data);      
      for(var i = 0; i<data.rows.length;i++)
      {
        var row_data = data.rows[i];
        var name = row_data.simio.name;
        var values = row_data.simio.values;
        const buffer = Buffer.from(values);
        var wavConverter = require('wav-converter');
        var fs = require('fs');
        var path = require('path');      
        var wavData = wavConverter.encodeWav(buffer, {
            numChannels: 1,
            sampleRate: 16000,
            byteRate: 16
        });
        fs.writeFileSync(path.resolve('./simulationout', name+'16k.wav'), wavData);
      }
      const pgUpdJSONB = require('./updatePostgresJSONB.js')
      pgUpdJSONB("initialdir","toolsctrl","stim_state","stim_finished");
    });
});



socket.on('registerDAQ', function(data){
  //pg(socket,"simulationio",'sendIOSignals');
});

socket.on('createDAQSTIMTable', function(data){
  const pgCreateDaqStim = require('./createDaqStimInPG.js');    
  pgCreateDaqStim("daqstimtable",data);
});


//------------------------------------------------------------------------------------------
// Start of tabledata event
//------------------------------------------------------------------------------------------
        
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event tabledata
          * @type {object}
          * @property {function} () Changes the following postgres data :
          * <li>Indicate map view as active view</li> 
          * <li>delete previous map</li>
          * <li>add new map</li>  
          * @see [server: store formatted 3d table data to postgres]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata}   
          * @description tabledata event triggers a callback function which stores formatted 3d table data to postgres
          */
        socket.on('tabledata', function(data){
          
        /**
          * This namespace describes the function body of the server sided socket.io
          * interface
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata
          * 
          */

          /**
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata
          * @type {type}
          * @description if data length is greater than 2, then the 'ActiveType' equals 'map' otherwise
          * is is 'const'. The type 'curve' is set in a separate event callback.
          */       

          var type = {}
          if(data.length>2)
          {
            type.view = 'map';  
          }
          else 
          {
            type.view = 'const';
          }
          
          pgUpdate('ActiveView','View',type);                         
           /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata    
          * @function pgDel
          * @param {string} database name of the database
          * @param {string} event name of the emitted event
          * @description triggers a query in the postgres database which eventually
          * eventually loads the found data
          */
          pgDel("ThermoCalDataMaps");

          /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_2_tabledata        
          * @function pgSet
          * @param {string} database name of the database
          * @param {string} event name of the emitted event
          * @description triggers a query in the postgres database which eventually
          * eventually loads the found data
          */
          pgSet("ThermoCalDataMaps","Maps",data);
        }); 
//------------------------------------------------------------------------------------------
// End of tabledata event
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// Start of curvedata event
//------------------------------------------------------------------------------------------

        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event curvedata
          * @type {object}
          * @property {function} function(data) store formatted curve data to postgres.  
          * @see [server: store formatted 2d curve data to postgres]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_3_curvedata}   
          * @description set of curves is received from the client by this socket.io event and stored to postgres. 
          * <br>follow the link to find the details about callback function
          */ 
        socket.on('curvedata', function(data){   
          
          /**
          * This namespace describes the body of the callback function which is called
          * on the 'curvedata' event. The server sided callback receives formatted
          * curve data from the engine model and stores it in a corresponding postgres
          * table "ThermoCalDataCurves".
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_3_curvedata
          * 
          */    

         /**     
           * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_3_curvedata    
           * @function pgSet
           * @param {string} ThermoCalDataCurves name of the postgres table
           * @param {string} Curves name of the column in the postgres table
           * @description set "Curves" column in postgres table "ThermoCalDataCurves".
           * <br>Follow the link to find the details about the postgres setter function
           * @see {@link module:g_setPostgresData_server}
           */
          pgSet("ThermoCalDataCurves","Curves",data);
          
        });
//------------------------------------------------------------------------------------------
// End of curvedata event
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// Start of getTable event
//------------------------------------------------------------------------------------------
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event getTable
          * @type {object}
          * @property {function} getPostGresData Calls postgres getter function with the arguements 'sendTable'. 'sendTable' 
          * is the name of the emitted event which conveys the postgres data table ThermoCalDataMaps to the web client. 
          * @description request event coming from the function selection view. It is triggered by the request button 
          * for a specific 3d table editor and requests the raw data of a selected map to send it to the 3d view.
          * @see [server: read raw table data from postgres and send it to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_4_getTable}
          */ 
        socket.on('getTable',function(){
          /**
          * This namespace describes the function body of the server sided socket.io
          * interface
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_4_getTable
          * 
          */ 
         
          
        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_4_getTable    
          * @function pg
          * @param {string} ThermoCalDataMaps postgres table
          * @param {string} sendTable name of the socket event
          * @description triggers a query in the postgres database which eventually
          * eventually loads the found data via the 'sendTable' event of socket.io.  
          */
          pg(socket,"ThermoCalDataMaps",'sendTable');          
        });
//------------------------------------------------------------------------------------------
// End of getTable event
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
// Start of getCurve event
//------------------------------------------------------------------------------------------

        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event getCurve
          * @type {object}
          * @property {function} () get currently selected set of curves from postgres
          * @description selected curve of function selector view is requested by the client.
          * This corrsponding event on the server returns the curve data inside its callback
          * routine.
          * @see [server: read raw curve data from postgres and send it to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_5_getCurve}
          */ 
        socket.on('getCurve',function(){
          /**
          * This namespace describes the function body of the server sided socket.io
          * interface
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_5_getCurve
          * 
          */
          pg(socket,"ThermoCalDataCurves",'sendCurve');          
        });
//------------------------------------------------------------------------------------------
// Start of getAudioFlPths event
//------------------------------------------------------------------------------------------

        /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected    
          * @event getAudioFlPths
          * @type {object}
          * @property {function} getStimData gets stimulation data for the simulation view from postgres    
          * @description get stimulation data as wav files and send it to the simulation view
          * <br>Follow the link to find the details about the callback function.<br><br>
          * [send stimulation data to client]{@link c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_6_getStimData}  
          */
        socket.on('getAudioFlPths',function(){
          /**
          * This namespace describes the function body of the server sided socket.io
          * interface
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected
          * @type {object}
          * @namespace c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_6_getStimData
          * 
          */

          /**     
          * @memberof c_socketCom_server.c_1_socketfunction.c_1_1_socketConnected.c_1_1_6_getStimData    
          * @function getWav
          * @param {string} socket socket object
          * @description triggers a query in the postgres database which eventually
          * eventually loads the found data <br>
          * <a href="module-e_getModelStimulationData_server.html#~event:sendAudioFlPths">link </a>
          * 
          * @see {@link module:e_getModelStimulationData_server}
          */
        new getWav(socket);
        });
//------------------------------------------------------------------------------------------
// End of getAudioFlPths event
//------------------------------------------------------------------------------------------        
        });        
   
}