//=========================================================================================
//
//             selectSWtoAnalyze.js : load root directories from JSON file
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
const loadInitialDirs = require('./loadInitialDirs');

router.get('/selectSW',function(req,res){
    var DirsReader = new loadInitialDirs('/initialPaths/initialDirs.json');
    
    DirsReader.then(function(result)
        {
          var iniPaths = result.intialPaths;
          var iniPth=[];
          for(var i=0;i<iniPaths.length;i++)
          {          
            iniPth.push(iniPaths[i].SWDir);
          }
  
    
          
          res.render('index',{ iniPaths: iniPth });
        });
    //__dirname : It will resolve to your project folder.
  });