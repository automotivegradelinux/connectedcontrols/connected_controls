//==========================================================================================
//
//             createDaqStimInPG.js : creates a table in postgres that is used for 
//             this exchange of simulation data (daq : data aquisition, stim : stimulation). 
//              
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Setter method : write data to postgres data table
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {object} data data content in JS object format
 * @module g_createPostgresTable_server
 */
 module.exports = function(table,column)
 {

    const {Pool} = require('pg');

    /**
       * Pool of postgres clients object
       * @typedef {Object} Pool
       * @property {string} user - postgres user
       * @property {string} host - posgres host
       * @property {string} database - database name
       * @property {string} password - password
       * @property {number} port - port
       * @property {method} connect - connection method
       */
     
     const pool = new Pool({
        user: 'ThermBaseAlgebraic',
        host: 'db',
        database: 'ThermoBaseDataStationary',
        password: 'thermo',
        port: 5432
    });

    function create(client,table,column)
    {     
        if(Array.isArray(column))
        {
            var cmdstr="CREATE TABLE public."+table+"( id serial NOT NULL PRIMARY KEY, ";
            for(var i=0;i<column.length;i++)
            {
                if(i==column.length-1)
                {
                    cmdstr += column[i] + " jsonb NOT NULL)";
                }
                else
                {
                    cmdstr += column[i] + " jsonb NOT NULL,";
                }               
            }
            client.query(cmdstr)
            .then( res=>{
              console.log(res);              
              })
            .catch();        
        }
        else
        {       
            var qrstring = "CREATE TABLE public."+table+ "( id serial NOT NULL PRIMARY KEY, " + column + " jsonb NOT NULL);";
            client.query(qrstring)
                  .then( res=>{
                    console.log(res);                    
                    })
                  .catch();        
        }

    }
    // UPDATE public.initialdirectories SET dirs.initialpaths =jsonb_set(dirs,array['3'], '"newvalue"');

 
     /**
       * @method pool_connect_then
       * @description The postgres query consists of the following to arguments
       * <ol><li>jsonData = JSON.stringify(data) => JSON data created out of JS data</li>
       * <li>qrstring = "INSERT INTO " + "public." +'"'+table+'"'+ "(" +'"'+ column+'"' + ") " + "VALUES ($1)" => SQL command</li></ol>
       * <br>
       * The INSERT INTO command stores the corresponding data into :
       * <ol><li>table</li>
       * <li>column</li></ol>
       * <br>
       * Table and columns are the call parameters of the setter function.
       * <br>
       * The pool connect method creates a postgres client which starts the query. After both a successful and erroneous
       * query the client has to be released. 
       */
     pool.connect().then(client => {         
         var qrstring = "SELECT EXISTS ( SELECT FROM information_schema.tables WHERE  table_schema = 'public'  AND    table_name   = '"+table+"');";
                                      
         return client
           .query(qrstring)
           .then(res => {
            if(!res.rows[0].exists)
            {
                create(client,table,column);                                         
            }
            else
            {
                var qrstr ='DELETE FROM ' + 'public.' + '"'+table+'"';
                client.query(qrstr)
                .then(res => { console.log(res);  })
                .catch(err => { client.release(); console.log(err.stack);})
                console.log('map exists');
            }
                        
           })
           .catch(err => {
             client.release();
             console.log(err.stack);
           })
       });    
 }