//=========================================================================================
//
//             loadDCMFile.js : load DCM files (converted from audio files)
//             
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
function splitWhiteSpace(input)
{
    var result = input.split(/[ ,]+/);
    return result;
}

function processKFHeader(input)
{
    var result = {};
    var headerArray = splitWhiteSpace(input);
    result.TYPE = headerArray[0];
    result.KFNAME =  headerArray[1];
    result.SPTX =  headerArray[2];
    result.SPTY =  headerArray[3];

    return result;    
}

function processKLHeader(input)
{
    var result = {};
    var headerArray = splitWhiteSpace(input);
    result.TYPE = headerArray[0];
    result.KLNAME =  headerArray[1];
    result.SPTX =  headerArray[2];    

    return result;    
}

function processFWHeader(input)
{
    var result = {};
    var headerArray = splitWhiteSpace(input);
    result.TYPE = headerArray[0];
    result.FWNAME =  headerArray[1];
    
    return result;    
}

//KENNFELD AFS_dmMinThresBi_MAP 12 10
//   LANGNAME "Minimum AirMassFlow threshold in Bi Turbo mode"
//   FUNKTION AFS_VD 
//   EINHEIT_X "rpm"
//   EINHEIT_Y "Nm"
//   EINHEIT_W "Kg/h"
//   ST/X   0.00   500.00   900.00   1400.00   1800.00   2300.00   
//   ST/X   2700.00   3200.00   3600.00   4100.00   4500.00   5000.00   
//   ST/Y   0.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   50.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   100.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   200.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   250.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   300.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   400.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   600.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   700.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   ST/Y   800.000
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//   WERT   200.000   200.000   200.000   200.000   200.000   200.000   
//END

function processFUNKTION(input)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;
    var lngname='';
    var key;
    for(var i=1;i<len;i++)
    {
        if(!LangnameArray[i].localeCompare('FUNKTION'))
        { 
            key = LangnameArray[i];
        }
        else
        {
            lngname +=LangnameArray[i]+' ';
        }        
    }
    result[key] = lngname;
    return result;    
}

function processEINHEIT(input,keyString)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;
    var lngname='';
    var key;
    for(var i=1;i<len;i++)
    {
        if(!LangnameArray[i].localeCompare(keyString))
        { 
            key = LangnameArray[i];
        }
        else
        {
            lngname +=LangnameArray[i]+' ';
        }        
    }
    result[key] = lngname;
    return result;    
}

function processSTX(input,keyString,value)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;    
   
    for(var i=1;i<len;i++)
    {
        if(!LangnameArray[i].localeCompare(keyString))
        { 
            key = LangnameArray[i];
        }
        else
        {
            if(LangnameArray[i].length)
            {
                value.push(LangnameArray[i]);
            }
        }        
    }        
    result[key] = value;
    return result;    
}

function processSTY(input,keyString)
{
    
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;    
   
    for(var i=1;i<len;i++)
    {
        if(!LangnameArray[i].localeCompare(keyString))
        { 
            key = LangnameArray[i];
        }
        else
        {
            if(LangnameArray[i].length)
            {
                value=(LangnameArray[i]);
                return value;
            }
        }        
    }       
}

function processSTW(input,keyString,value,Yvalue,b_new)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;    

    var b_found = false;
    
    if(typeof value !== 'undefined')
    {
        if(Array.isArray(value))
        {
            for(var j=0;j<value.length;j++)
            {
                if(typeof value[j].StY !== 'undefined')
                {
                    if(!value[j].StY.localeCompare(Yvalue) && !b_new)
                    {
                        for(var i=1;i<len;i++)
                        {                            
                            
                            if(!LangnameArray[i].localeCompare(keyString))
                            { 
                                key = LangnameArray[i];
                                b_found = true;
                            }
                            else
                            {
                                if(LangnameArray[i].length)
                                {                
                                    value[j].StW.push(LangnameArray[i]);             
                                }
                            }        
                        } 
                    }
                }
            }
        }
    }
    if(!b_found)
    {
        var obj = {};
        obj.StY = Yvalue;
        obj.StW = [];
        for(var i=1;i<len;i++)
        {
            
            if(!LangnameArray[i].localeCompare(keyString))
            {   
                key = LangnameArray[i];
            }
            else
            {
                if(LangnameArray[i].length)
                {                
                    obj.StW.push(LangnameArray[i]);             
                }
            }        
        }
        value.push(obj);
    }                    
}

function processYW(input,keyString,value,Xvalue)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;    
    
    var obj = {};
    obj.StX = Xvalue;
    obj.StW = [];
    for(var i=1;i<len;i++)
    {
        
        if(!LangnameArray[i].localeCompare(keyString))
        { 
            key = LangnameArray[i];
        }
        else
        {
            if(LangnameArray[i].length)
            {                
                obj.StW.push(LangnameArray[i]);             
            }
        }        
    }                
    value.push(obj);
}


function processLANGNAME(input)
{
    var result = {};
    var LangnameArray = splitWhiteSpace(input);
    var len = LangnameArray.length;
    var lngname='';
    for(var i=1;i<len;i++)
    {
        if(!LangnameArray[i].localeCompare('LANGNAME'))
        { 
            key = LangnameArray[i];
        }
        else
        {
            lngname +=LangnameArray[i]+' ';
        }        
    }
    result[key]=lngname;
    return result;
    //console.log(lngname);
}
function clearArray(array) {
    while (array.length) {
      array.pop();
    }
  }

  function storeToMongo(db,collectionName,curObj)
  {             
          db.collection(collectionName).insertOne(curObj, function(err, res) {
              if (err) throw err;
          });     
  }

module.exports = function () {

    return new Promise((resolve, reject) => 
    {       
        var MongoClient = require('mongodb').MongoClient;
        var url = "mongodb://localhost:27017/";
        MongoClient.connect(url, function(err, db) 
        {        
            if (err) throw err;
            var dbo = db.db("mydb");
        
            const lineReader = require('line-reader');
            const path = require('path');

            var b_KF = false;
            var b_GKF = false;

            var b_KL = false;
            var b_GKL = false;

            var b_FW = false;

            var stX = [];
   
            var stW = [];
            var Yvalue; 
            var Y_vec = []; 
    
        
            // or read line by line:
            var KFHeader,KLHeader,FWHeader;
            var b_new = false;

            lineReader.eachLine(path.join(__dirname,'/P1988PantherFIE_300_20200212_DataDynoFIEv01_AMUData_ZFASU3_patsDis_HFM_HPT_1_Endstand_200220.DCM'), function(line, last, cb) 
            {                       
                if (line.includes('KENNFELD'))
                {                        
                    KFHeader = processKFHeader(line); 
                    if(typeof KFHeader.KFNAME !== 'undefined' && typeof KFHeader.SPTX !== 'undefined' && typeof KFHeader.SPTY !== 'undefined' && typeof KFHeader.TYPE !== 'undefined')
                    {                    
                        if(!KFHeader.TYPE.localeCompare('GRUPPENKENNFELD'))
                        {                     
                            b_GKF = true;
                        }
                        if(!KFHeader.TYPE.localeCompare('KENNFELD'))
                        {                        
                            b_KF = true;
                        }
                    }                            
                }
                if(b_KF || b_GKF)
                { 
                    //console.log(line);           
                    if (line.includes('LANGNAME'))      
                    { 
                        var longname = processLANGNAME(line);
                        if(typeof longname.LANGNAME !== 'undefined') KFHeader.LONGNAME = longname.LANGNAME;
                    }
                    if(line.includes('FUNKTION'))       
                    { 
                        var functionname = processFUNKTION(line);                     
                        if(typeof functionname.FUNKTION !== 'undefined') KFHeader.FUNKTION = functionname.FUNKTION;
                    }            
                    if(line.includes('EINHEIT_X'))      
                    { 
                        var unitX = processEINHEIT(line,'EINHEIT_X');                     
                        if(typeof unitX.EINHEIT_X !== 'undefined') KFHeader.UNIT_X = unitX.EINHEIT_X;
                    }
                    if(line.includes('EINHEIT_Y'))      
                    { 
                        var unitY = processEINHEIT(line,'EINHEIT_Y'); 
                        if(typeof unitY.EINHEIT_Y !== 'undefined') KFHeader.UNIT_Y = unitY.EINHEIT_Y;
                    }
                    if(line.includes('EINHEIT_W'))      
                    { 
                        var unitW = processEINHEIT(line,'EINHEIT_W'); 
                        if(typeof unitW.EINHEIT_W !== 'undefined') KFHeader.UNIT_W = unitW.EINHEIT_W;
                    }
                    if(line.includes('ST/X'))           
                    { 
                        processSTX(line,'ST/X',stX); 
                    }
                    
                    if(line.includes('ST/Y'))           
                    { 
                        Yvalue = processSTY(line,'ST/Y'); 
                        b_new = true;
                    }
                    if(line.includes('WERT'))           
                    { 
                        processSTW(line,'WERT',stW,Yvalue,b_new); 
                        b_new = false;
                    }
                    if (line.includes('END')) 
                    {
                        //if(b_GKF) console.log('End of Gruppenkennfeld');
                        KFHeader.SPTX   =  stX;
                        KFHeader.VALUES =  stW;
                        storeToMongo(dbo,"DCMData",KFHeader); 

                        b_KF = false;        
                        b_GKF = false;
                        //console.log('X Axis:',stX);                 
                        //console.log('W Axis:',stW);
                        clearArray(stX);                 
                        clearArray(stW);
                        //console.log('END of TAG');                                
                    }
                }             
                if (line.includes('KENNLINIE'))
                {                        
                    KLHeader = processKLHeader(line); 
                    if(typeof KLHeader.KLNAME !== 'undefined' && typeof KLHeader.SPTX !== 'undefined'  && typeof KLHeader.TYPE !== 'undefined')
                    {         
                        if(!KLHeader.TYPE.localeCompare('GRUPPENKENNLINIE'))
                        {                     
                            b_GKL = true;
                        }
                        if(!KLHeader.TYPE.localeCompare('KENNLINIE'))
                        {                        
                            b_KL = true;
                        }                   
                    }                          
                }
                if(b_KL || b_GKL)
                { 
                    if (line.includes('LANGNAME'))      
                    { 
                        var longname = processLANGNAME(line);
                        if(typeof longname.LANGNAME !== 'undefined') KLHeader.LONGNAME = longname.LANGNAME;
                    }
                    if(line.includes('FUNKTION'))       
                    { 
                        var functionname = processFUNKTION(line);                     
                        if(typeof functionname.FUNKTION !== 'undefined') KLHeader.FUNKTION = functionname.FUNKTION;
                    }            
                    if(line.includes('EINHEIT_X'))      
                    { 
                        var unitX = processEINHEIT(line,'EINHEIT_X');                     
                        if(typeof unitX.EINHEIT_X !== 'undefined') KLHeader.UNIT_X = unitX.EINHEIT_X;
                    }
                    if(line.includes('EINHEIT_Y'))      
                    { 
                        var unitY = processEINHEIT(line,'EINHEIT_Y'); 
                        if(typeof unitY.EINHEIT_Y !== 'undefined') KLHeader.UNIT_Y = unitY.EINHEIT_Y;
                    }
                    if(line.includes('EINHEIT_W'))      
                    { 
                        var unitW = processEINHEIT(line,'EINHEIT_W'); 
                        if(typeof unitW.EINHEIT_W !== 'undefined') KLHeader.UNIT_W = unitW.EINHEIT_W;
                    }
                    if(line.includes('ST/X'))           
                    { 
                        processSTX(line,'ST/X',stX); 
                    }                
                    if(line.includes('WERT'))           
                    { 
                        processSTX(line,'WERT',Y_vec);                    
                    }                                
                    if (line.includes('END')) 
                    {
                        KLHeader.SPTX =  stX;
                        KLHeader.VALUES =  Y_vec;
                        storeToMongo(dbo,"DCMData",KLHeader); 
                        b_KL = false;        
                        b_GKL = false;
                        clearArray(stX);                 
                        clearArray(Y_vec);

                    }
                }                             
                if (line.includes('FESTWERT'))
                {
                    FWHeader = processFWHeader(line);
                    b_FW = true;
                }
                if(b_FW)
                {
                    if (line.includes('LANGNAME'))      
                    { 
                        var longname = processLANGNAME(line);
                        if(typeof longname.LANGNAME !== 'undefined') FWHeader.LONGNAME = longname.LANGNAME;
                    }
                    if(line.includes('FUNKTION'))       
                    { 
                        var functionname = processFUNKTION(line);                     
                        if(typeof functionname.FUNKTION !== 'undefined') FWHeader.FUNKTION = functionname.FUNKTION;
                    }            
                 
                    if(line.includes('EINHEIT_W'))      
                    { 
                        var unitW = processEINHEIT(line,'EINHEIT_W'); 
                        if(typeof unitW.EINHEIT_W !== 'undefined') FWHeader.UNIT_W = unitW.EINHEIT_W;
                    }
                             
                    if(line.includes('WERT'))           
                    { 
                        var lineArray = splitWhiteSpace(line);
                        for(var lintIt=0;lintIt<lineArray.length;lintIt++)
                        {
                            if(!lineArray[lintIt].localeCompare('WERT'))
                            {
                                var wert = processSTX(line,'WERT',Y_vec);
                                FWHeader.VALUE = wert.WERT[0];
                            }
                        }
                    }

                    if (line.includes('END')) 
                    {   
                        b_FW = false;  
                        storeToMongo(dbo,"DCMData",FWHeader);                          
                    }
                }
                if(last)
                {
                    cb(false);    
                }
                cb();
            });
            resolve(true);  
        });              
    });
}