//=========================================================================================
//
//             getPostgresWOsocket.js : get data from postgres without any
//             socket.io intervention
//             
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Main module - See {@tutorial calculator-tutorial}
 * @module i_getPostgresWOsocket_server
 */
 module.exports = function(table) { return new Promise((resolve, reject) =>
 
 {
   
     const {Pool} = require('pg');
     
     const pool = new Pool({
         user: 'ThermBaseAlgebraic',
         host: 'db',
         database: 'ThermoBaseDataStationary',
         password: 'thermo',
         port: 5432
     });
 
     pool.connect().then(client => {
       var qrstr ='SELECT * FROM ' + 'public.' + '"'+table+'"';
         return client
           .query(qrstr)
           .then(res => {
             client.release();             
             console.log(res);
             resolve(res);
           })
           .catch(err => {
             client.release()
             console.log(err.stack)
             reject(err.stack);
           })
       });
 })};