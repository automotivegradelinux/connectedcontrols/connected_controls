//==========================================================================================
//
//             createCmakeLists.js : this module creates CMake configuration files 
//             which are used to compile C code modules from the source folder
//             into a linux archive file
//              
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
///////////////////////////////////////////////////////////////////////////////////////
//
// cmake_minimum_required(VERSION 3.3)
//  
// project(example_lib)
// 
// set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
// 
//    SET (CMAKE_C_COMPILER             "clang")
//    SET (CMAKE_C_FLAGS                "-Wno-unknown-attributes")
//    SET (CMAKE_C_FLAGS_DEBUG          "-g")
//    SET (CMAKE_C_FLAGS_MINSIZEREL     "-Os -DNDEBUG")
//    SET (CMAKE_C_FLAGS_RELEASE        "-O4 -DNDEBUG")
//    SET (CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g")
//
//    SET (CMAKE_CXX_COMPILER             "clang++")
//    SET (CMAKE_CXX_FLAGS                "-Wall")
//    SET (CMAKE_CXX_FLAGS_DEBUG          "-g")
//    SET (CMAKE_CXX_FLAGS_MINSIZEREL     "-Os -DNDEBUG")
//    SET (CMAKE_CXX_FLAGS_RELEASE        "-O4 -DNDEBUG")
//    SET (CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")
//
//    SET (CMAKE_AR      "llvm-ar")
//    SET (CMAKE_LINKER  "llvm-ld")
//    SET (CMAKE_NM      "llvm-nm")
//    SET (CMAKE_OBJDUMP "llvm-objdump")
//    SET (CMAKE_RANLIB  "llvm-ranlib")
// 
// 
// # define the library
// set(header_srcs
// ${CMAKE_CURRENT_SOURCE_DIR}/../../_gen/swb/filegroup/includes/project
// ${CMAKE_CURRENT_SOURCE_DIR}/../../_gen/swb/filegroup/includes/corecfg
// ${CMAKE_CURRENT_SOURCE_DIR}/../../_gen/swb/filegroup/includes/data
// ${CMAKE_CURRENT_SOURCE_DIR}/../../_gen/swb/filegroup/includes/mcop
// )
// 
// include_directories(${header_srcs})
// message ("Headers : ${header_srcs}")
// 
// set(library_srcs
//    airmod_if.c
// )
//
///////////////////////////////////////////////////////////////////////////////////////

module.exports = function (Header_vec,swDir) {

    const lineReader = require('line-reader');
    const path = require('path');
    const fs = require('fs');

  

    
    var CStr = path.basename(Header_vec[0].CFile);


    var CDir_vec = Header_vec[0].CFile.split(swDir);
    var Cdir = path.dirname(CDir_vec[CDir_vec.length-1]);


    var pathStr = path.join(__dirname,'source',Cdir,'/CMakeLists.txt');
    try {
      if (fs.existsSync(pathStr)) {
        //file exists
        var strFile = fs.readFileSync(pathStr, 'utf8');
        var str_vec = strFile.split('set(library_srcs');
        if(str_vec.length>1)
        {
          var resString = str_vec[1];
          var str2_vec = resString.split(')');          
          var prevString_vec = str2_vec[0].split(' ');
          var newDatStr = CStr.trim();
          var b_isAlreadyUsed = false;
          var prevStr_result = '';
          for(var iPr=0;iPr<prevString_vec.length;iPr++)
          {
            let curStr = prevString_vec[iPr].trim();
            if(!curStr.localeCompare(newDatStr))
            {
              b_isAlreadyUsed = true;
              return;
            }
            prevStr_result+= ' ' + curStr;
          }     
          if(!b_isAlreadyUsed) 
          {    
            CStr = prevStr_result + ' ' + newDatStr;  
            //CStr = str2_vec[0] + ' ' + newDatStr;  
          }         
        }
      }
      else
      {
        console.log('no file found');  
      }
    } catch(err) {
      console.log('no file found');
    }

    var headerpth=[];
    for(var i=0;i<Header_vec.length;i++) 
    {
      var HsplitDir = Header_vec[i].Header.split(swDir);
      var tempPath = path.dirname(HsplitDir[HsplitDir.length-1]);
      var b_found = false;
      for(var k=0;k<headerpth.length;k++)
      {
        if(!headerpth[k].localeCompare(tempPath))
        {
          b_found = true;
          break;
        }
        
      }      
      if(!b_found)
      {
        headerpth.push(tempPath);      
      }
    }

    // Read CMake boilerplate
    
    var headerstr = "\n";
    var baseStringHeaders = "${CMAKE_CURRENT_SOURCE_DIR}/../"
    for(var k=0;k<headerpth.length;k++)
    {
      headerstr += baseStringHeaders + ".." + headerpth[k] + "\n";
    }
    var str = fs.readFileSync(path.join(__dirname+'/CmakeBoilerPlate.txt'), 'utf8')
    
    
    var resH = str.replace("ADD_HEADER_PATH", headerstr);
    var resC = resH.replace("ADD_C_FILE", CStr);
 

    fs.writeFileSync(path.join(__dirname,'source',Cdir,'/CMakeLists.txt'), resC);
    
    console.log(resC);

   
     

}