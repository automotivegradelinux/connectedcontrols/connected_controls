//=========================================================================================
//
//             loadInitialDirs.js : load root directories from file
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
module.exports = function (filename) {

    return new Promise((resolve, reject) => {
        
        console.log('I am trying to read the initial Dirs');
        const path = require('path');
        const fs = require('fs');
    
        fs.readFile(path.join(__dirname+filename), (err, data) => {
            if (err) reject(new Error("Could not load image with URI " + uri));
            let student = JSON.parse(data);
            console.log(student);                       
            const pgCreate = require('./createPostgresTable.js');
            const pgSet = require('./setPostgresData.js');
            pgCreate("initialdirectories","dirs",student);
            //pgSet("ThermoCalDataMaps","Maps",student);            
            resolve(student);            
          });        
      });
}