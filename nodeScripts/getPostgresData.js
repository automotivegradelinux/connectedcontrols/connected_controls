//=========================================================================================
//
//             getPostgresData.js : get data from postgres on a socket.io request
//             sent by the web client and send the postgres data back to the requester
//             using a reponse event in socket.io
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Getter method : read data from postgres data table and send it to the web client using socket.io
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {string} request name of the socket event
 * @module f_getPostgresData_server
 */
module.exports = function(socket,table,request,column=null,property=null,cmpval=null)
{
      /**
      * Pool of postgres clients object
      * @typedef {Object} Pool
      * @property {string} user - postgres user
      * @property {string} host - posgres host
      * @property {string} database - database name
      * @property {string} password - password
      * @property {number} port - port
      * @property {method} connect - connection method
      */

    const {Pool} = require('pg');
    
    const pool = new Pool({
        user: 'ThermBaseAlgebraic',
        host: 'db',
        database: 'ThermoBaseDataStationary',
        password: 'thermo',
        port: 5432
    });

      /**
      * @method pool_connect_then
      * @description The getter method uses a socket.io event to send data to a web client which is
      * read from a postgres table using a query. The arguments are : 
      * <ol><li>request => socket.io event name of the data which is sent to the web client</li>
      * <li>qrstr ='SELECT * FROM ' + 'public.' + '"'+table+'"' => SQL command</li></ol>
      * <br>
      * The "Select" query reads a complete table. There are 2 different socket.io send events :
      * <ol><li>'sendTable'</li>
      * <li>'sendCurve'</li></ol>
      * <br>
      * The request argument has to be equal to one of the below strings to create a reaction. 
      * Curves are collected in a collector array before they are sent to the client. 
      * Maps are sent directly to the client without any further conversions. 
      */

    pool.connect().then(client => {
      if(column!=null && property!=null && cmpval!=null)
      {
        var qrstr ='SELECT ' + column + ' FROM ' + 'public.' +table + ' WHERE '+ column + '->>\'' + property + '\'=\'' + cmpval + '\';';  
      }
      else 
      {
        var qrstr ='SELECT * FROM ' + 'public.' + '"'+table+'"';  
      }
      
        return client
          .query(qrstr)
          .then(res => {
            client.release()
            if(request=='sendTable')
            {
              socket.emit(request,res.rows[0].Maps);              
            }
            else if(request=='sendCurve') {        
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].Curves)
              }   
              socket.emit(request,collect);  
            }
            else if(request=='sendDirectoryStruct'){        
              
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].dirs)
              }   
              socket.emit(request,collect);  
            }            
            else if(request=='sendCFiles'){        
              
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].csrcs)
              }   
              socket.emit(request,collect);  
            }
            else if(request=='headerfiles'){                      
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].hdrfls)
              }   
              socket.emit(request,collect);  
            }
            else if(request=='sendHeaderTree'){                      
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].hdrtree)
              }   
              socket.emit(request,collect);                
            }
            else if(request=='sendIOSignals'){                      
              var collect = [];
              for(var i=0;i<res.rows.length;i++)
              {
                collect.push(res.rows[i].simio)
              }   
              socket.emit(request,collect);  
            }
            else{                          
              socket.emit(request,res.rows[0]);  
            }            
          })
          .catch(err => {
            client.release()
            console.log(err.stack)
          })
      });
}