//=========================================================================================
//
//             loadDeclarationsFromMongo.js : load declaration nodes from the CLANG
//             AST node stored in a MongoDB
//             
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              Copyright (C) 2022 Ford Motor Company. All Rights Reserved.
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
module.exports = function(clientDecl)
{
    // Connect to socket.io
    return new Promise((resolve, reject) => 
    {

        clientDecl.on('connection', function(socket)
        {
            socket.on('reqFunctionAnalysis',function(dataClient)
            {
                var test = "test";
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("requestedFuncDefAnalyze",function(err,coll)
                    {
                        coll.insertOne( {functionDefs : dataClient});
                    });
                });
            });

            socket.on('reqRTOSTimeTables',function(dataClient)
            {
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSTimeTables",function(err,coll)
                    {
                        
                        // db.RTOSTimeTables.find({"table.OSActivationFunction":reqdata}).pretty()
                        coll.find({"table.OSActivationFunction":reqData},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                var dataObj = {};
                                dataObj.reqData = reqData;
                                dataObj.hasVarData = false;
                                
                                cursor.each(function(err,data)
                                { 
                                    if(data!==null)
                                    {                 
                                        dataObj.hasVarData = true;  
                                        dataObj.respData = data;
                                        socket.emit('respRTOSTimeTables',dataObj);                                        
                                    }
                                    else
                                    {                                        
                                        socket.emit('respRTOSTimeTablesDone',dataObj);
                                    }
                                });
                            }
                        });
                    });
                });
            });

            socket.on('reqRTOSTaskActivations',function(dataClient)
            {
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSTaskActivations",function(err,coll)
                    {
                        //{pointerToProcessList:reqData}
                        coll.find({"TaskCall.activatedTask":reqData},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                var dataObj = {};
                                dataObj.reqData = reqData;
                                
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        socket.emit('respRTOSTaskActivations',data);
                                        
                                        if(typeof data.TaskCall !== 'undefined')
                                        {
                                            if(typeof data.TaskCall.nameOfActivationFunction !== 'undefined')
                                            {
                                                dataObj.nameOfActivationFunction = data.TaskCall.nameOfActivationFunction;
                                                dataObj.respData = data;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        socket.emit('respRTOSTaskActivationsDone',dataObj);
                                        //db.RTOSTaskDescriptors.find({pointerToProcessList:"os_taskTabOS_Trans_2ms_Task"}).pretty()
                                        // "descriptionName" : "os_OS_Trans_2ms_TaskDesc"
                                        //db.RTOSTaskTypes.find({ptrOnTaskDescription:"os_OS_Trans_2ms_TaskDesc"}).pretty()
                                    }
                                });
                            }
                        });
                    });
                });
            });

            socket.on('reqRTOSTaskTypes',function(dataClient)
            {
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSTaskTypes",function(err,coll)
                    {
                        //{pointerToProcessList:reqData}
                        coll.find({ptrOnTaskDescription:reqData},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                var dataObj = {};
                                dataObj.reqData = reqData;
                                
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        
                                        if(typeof data.nameOfTaskType !== 'undefined')
                                        {
                                            dataObj.nameOfTaskType = data.nameOfTaskType;
                                            dataObj.respData = data;
                                        }
                                        socket.emit('respRTOSTaskTypes',dataObj);
                                    }
                                    else
                                    {
                                        socket.emit('respRTOSTaskTypesDone',dataObj);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqTaskDescription',function(dataClient)
            {
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSTaskDescriptors",function(err,coll)
                    {
                        //{pointerToProcessList:reqData}
                        coll.find({pointerToProcessList:reqData},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                var dataObj = {};
                                dataObj.reqData = reqData;
                                
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        socket.emit('respRTOSTaskDescriptors',data);
                                        if(typeof data.descriptionName !== 'undefined')
                                        {
                                            dataObj.desriptorname = data.descriptionName;
                                            dataObj.respData = data;
                                        }
                                    }
                                    else
                                    {
                                        socket.emit('respRTOSTaskDescriptorsDone',dataObj);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqAppModeDesc',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSAppModeDesc",function(err,coll)
                    {
 
                        coll.find({},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        socket.emit('respAppModeDesc',data);
                                    }
                                    else
                                    {
                                        socket.emit('respAppModeDescDone',hasVarData);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqRTOSConfig',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSConfigDesc",function(err,coll)
                    {
                        coll.find({},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        socket.emit('respRTOSConfigDesc',data);
                                    }
                                    else
                                    {
                                        socket.emit('respRTOSConfigDescDone',data);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            socket.on('reqProcessContainerLists',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {
                    if (err) throw err;                        
                    var dbo = db.db("test"); 

                    dbo.collection("RTOSProcessContainerLists",function(err,coll)
                    {
                        var buffer = reqData;
                        for(var i=0;i<buffer.length;i++)
                        {
                            var line = buffer[i];
                            if(typeof line.functionname !== 'undefined')
                            {                        
                                var name = line.functionname;
                                coll.find({"processes.process":name},function(err,cursor)
                                {
                                    if(cursor !== null)
                                    {                                                                                
                                        var resObj = {};
                                        resObj.found = false;
                                        resObj.response = [];
                                        cursor.each(function(err,data)
                                        { 
                                            if(data!==null)
                                            {                 
                                                resObj.found = true;   
                                                resObj.data = data;
                                                socket.emit('respRTOSProcessContainerLists',resObj);
                                            }
                                            else
                                            {
                                                socket.emit('respRTOSProcessContainerListsDone',resObj);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                });                       
            }); 
            socket.on('reqProcessLists',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {                               
                    if (err) throw err;                        
                    var dbo = db.db("test"); 
                    dbo.collection("RTOSProcessLists",function(err,coll)
                    {
                        var buffer = reqData;
                        for(var i=0;i<buffer.length;i++)
                        {
                            var line = buffer[i];
                            if(typeof line.functionname !== 'undefined')
                            {                        
                                var name = line.functionname;
                                
                                coll.find({"processes.process":name},function(err,cursor)
                                {
                                    if(cursor !== null)
                                    {
                                        var dataObj = {};
                                        dataObj.hasVarData = false;
                                        dataObj.requestedName = name;
                                       
                                        cursor.each(function(err,data)
                                        {                                             
                                            if(data!==null)
                                            {                                                                   
                                                dataObj.hasVarData = true;
                                                if(typeof data.processListName !== 'undefined')
                                                {
                                                    dataObj.processListName = data.processListName;
                                                }
                                                dataObj.data = data;
                                                socket.emit('respProcessLists',dataObj);
                                            }
                                            else
                                            {
                                                socket.emit('respProcessListsDone',dataObj);
                                            }                                            
                                        });
                                    }
                                });
                            }
                        }                        
                    });                                                                       
                });
            });
            socket.on('reqSourceCodeDeclData',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");            
                    dbo.collection("Decls",function(err,coll)
                    {
                        coll.find({},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                var hasVarData = false;
                                cursor.each(function(err,data)
                                { 

                                    if(data!==null)
                                    {                 
                                        hasVarData = true;   
                                        socket.emit('respSourceCodeDeclData',data);
                                    }
                                    else
                                    {
                                        socket.emit('respSourceCodeDeclDataDone',data);
                                        db.close();
                                    }
                                });
                            }
                        });
                    });
                  
                });
            });
            socket.on('reqFuncDeclData',function(dataClient)
            {                
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");            
                dbo.collection("FunctionDecls",function(err,collFunc)
                {
                    collFunc.find({},function(err,cursorFunc)
                    {
                        if(cursorFunc !== null)
                        {
                            var hasData = false;
                            cursorFunc.each(function(err,funcdata)
                            { 
                                if(funcdata!==null)
                                {                    
                                    hasData = true;
                                    socket.emit('respSourceCodeFuncDeclData',funcdata);
                                    
                                }
                                else
                                {                                                                                
                                    socket.emit('respSourceCodeFuncDeclDone',hasData);                                        
                                }
                            });
                        }
                    });
                    });
                    var outerData = [];
                    dbo.collection("FunctionDefs",function(err,collFunc)
                    {
                        collFunc.find({},function(err,cursorFunc)
                        {
                            if(cursorFunc !== null)
                            {
                                var hasData = false;
                                cursorFunc.each(function(err,funcdata)
                                { 
                                    if(funcdata!==null)
                                    {                    
                                        hasData = true;                                        
                                        outerData.push(funcdata);                                                                                                                                                                
                                    }
                                    else
                                    {                                                                                
                                           
                                        dbo.collection("TaskList",function(err,collTask)
                                        {
                                            collTask.find({},function(err,cursorTask)
                                            {
                                                cursorTask.each(function(err,taskdata)
                                                {
                                                    if(taskdata!==null)
                                                    {
                                                        var test = taskdata;
                                                        for(var i=0;i<outerData.length;i++)
                                                        {
                                                            
                                                            var def = outerData[i].filepath;
                                                            var proc = taskdata.Proc;
                                                            if(typeof proc !== 'undefined')
                                                            {                                                                
                                                                if(!def.localeCompare(proc))
                                                                {
                                                                    var test = "test";
                                                                    socket.emit('respFunctionDefsDone',def); 
                                                                }
                                                            }
                                                            var contProc = taskdata.ContainerProc;
                                                            if(typeof contProc !== 'undefined')
                                                            {                                                                
                                                                if(!def.localeCompare(contProc))
                                                                {
                                                                    socket.emit('respFunctionDefsDone',def); 
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                });

                                            });
                                        });                                    
                                    }
                                });
                            }
                        });
                        });
                });
            });
            socket.on('reqNewFuncAnalysis',function(dataClient)
            {
                var reqData = dataClient;
                var MongoClient = require('mongodb').MongoClient;         
                // Establish connection to db        
                var url = "mongodb://localhost:27017/";
                MongoClient.connect(url, function(err, db) 
                {      
                    if (err) throw err;                        
                    var dbo = db.db("test");            
                    dbo.collection("RequestedProcesses",function(err,coll)
                    {
                        coll.find({},function(err,cursor)
                        {
                            if(cursor !== null)
                            {
                                coll.insertOne( {process : dataClient});
                            }
                        });
                    });
                });

            });
        });
        resolve = true;
    });
}