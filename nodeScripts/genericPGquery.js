//=========================================================================================
//
//             genericPGquery.js : provides a set of JS functions
//             which allow to request a generic SQL query by calling the function
//             with the SQL search string.
//
//             This project and source code is free for usage, open source and
//             licensed according to Mozilla Public License 2.0. The License may 
//             be found here:  https://www.mozilla.org/en-US/MPL/2.0/
//             Thus, the source code comes with no warranties or liabilities of any kind. 
//             Use it at your own risk
//
//              authors : 
//              Martin Haussmann (martin.haussmann@de.bosch.com)
//              Carles Sole Mendi (Carles.SoleMendi@de.bosch.com))
//              Christian Vigild (cvigild@ford.com)
//
//===-----------------------------------------------------------------------------------===//
/**
 * Getter method : read data from postgres data table and send it to the web client using socket.io
 * @param {string} table postgres table
 * @param {string} columns column in postgres table
 * @param {string} request name of the socket event
 * @module f_getPostgresData_server
 */
 module.exports = function(request)
 {
    return new Promise((resolve, reject) => 
    {
       /**
       * Pool of postgres clients object
       * @typedef {Object} Pool
       * @property {string} user - postgres user
       * @property {string} host - posgres host
       * @property {string} database - database name
       * @property {string} password - password
       * @property {number} port - port
       * @property {method} connect - connection method
       */
 
     const {Pool} = require('pg');
     
     const pool = new Pool({
         user: 'ThermBaseAlgebraic',
         host: 'db',
         database: 'ThermoBaseDataStationary',
         password: 'thermo',
         port: 5432
     });
       
     pool.connect().then(client => {
             
         return client
           .query(request)
           .then(res => 
            {
                resolve(res);
            });
             
       });
    });
 }